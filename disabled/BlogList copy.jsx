/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

import { BlogListItem } from 'components/Blog'

const animation = {
    transition: 'opacity 0.3s ease-in, color 0.3s ease-in',
}

const BlogListA = ({ posts }) => {
    if (posts.length === 0) return null
    return (
        <Styled.ol
            sx={{
                m: 0,
                display: 'flex',
                flexFlow: 'column nowrap',
                justifyContent: 'space-between',
            }}>
            {posts.map((post, index) => (
                <BlogListItem key={index} post={post.node} />
            ))}
        </Styled.ol>
    )
}

const BlogList = () => (
    <StaticQuery
        query={graphql`
            query {
                allMarkdownRemark(
                    filter: {
                        fields: { sourceName: { eq: "blog" } }
                        fileAbsolutePath: { glob: "**/blog/*/*/index.md" }
                        frontmatter: { published: { ne: false } }
                    }
                    sort: { order: DESC, fields: [frontmatter___date] }
                    limit: 1000
                ) {
                    edges {
                        node {
                            id
                            excerpt(pruneLength: 200)
                            fields {
                                slug
                                sourceName
                            }
                            frontmatter {
                                title
                                categories
                                tags
                                date(formatString: "MM.DD.YYYY")
                                is_featured
                                published
                                cover {
                                    childImageSharp {
                                        fluid(maxWidth: 1920, maxHeight: 360, quality: 90) {
                                            ...GatsbyImageSharpFluid
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `}
        render={data => (
            <Styled.ol
                sx={{
                    m: 0,
                    display: 'flex',
                    flexFlow: 'column nowrap',
                    justifyContent: 'space-between',
                }}>
                {data.allMarkdownRemark.edges.map((post, index) => (
                    <BlogListItem key={index} post={post.node} />
                ))}
            </Styled.ol>
        )}
    />
)

export default BlogList
