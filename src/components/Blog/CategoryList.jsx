/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import React, { useState, useEffect } from 'react'

import { Link, navigate } from 'gatsby'
import kebabCase from 'lodash/kebabCase'

const animation = {
    transition: 'opacity 0.3s ease-in, color 0.3s ease-in',
}

const CategoryList = ({ categories }) => {
    return (
        <React.Fragment>
            <ul
                sx={{
                    listStyleType: 'none',
                    textIndent: '0',
                    m: 0,
                    p: 0,
                    display: 'inline',
                    'li::after': {
                        content: '", "',
                    },
                    'li:last-child::after': {
                        content: '"',
                    },
                }}>
                {categories.map(category => (
                    <li
                        sx={{
                            display: 'inline-block',
                            '&:after': {
                                content: '", "',
                            },
                            '&:last-child:after': {
                                content: '""',
                            },
                        }}>
                        <Link
                            sx={{
                                variant: 'styles.a',
                                color: 'secondary',
                                transition: animation.transition,
                            }}
                            key={category}
                            to={`/blog/categories/${kebabCase(category)}`}
                            onClick={() => navigate(`/blog/categories/${kebabCase(category)}`)}>
                            {category}
                        </Link>
                    </li>
                ))}
            </ul>
        </React.Fragment>
    )
}

export default CategoryList
