/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

import { BlogListItem } from 'components/Blog'

const BlogList = ({ data }) => {
    // console.log('BlogList', data)
    return (
        <Box sx={{ variant: 'styles.spacer.list' }}>
            <Styled.ol
                sx={{
                    m: 0,
                    display: 'flex',
                    flexFlow: 'column nowrap',
                    justifyContent: 'space-between',
                }}>
                {data.allMarkdownRemark.edges.map((post, index) => (
                    <BlogListItem key={index} post={post.node} />
                ))}
            </Styled.ol>
        </Box>
    )
}

export default BlogList
