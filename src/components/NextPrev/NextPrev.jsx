/** @jsx jsx */
import { jsx, Styled, Flex } from 'theme-ui'

import React from 'react'
import PropTypes from 'prop-types'

import { Link } from 'gatsby'

import { get } from '@styled-system/css'
import { transparentize, darken } from 'polished'
import theme from 'theme'

// get(theme, `colors.background`)

const FancyButton = {
    height: '3rem',
    px: '2rem',
    backgroundColor: 'transparent',
    borderWidth: '3px',
    borderStyle: 'solid',
    borderColor: 'buttonColor',
    borderRadius: '2rem',

    position: 'relative',
    fontSize: '1rem',
    display: 'flex',
    flexFlow: 'column wrap',
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    '&:hover': {
        backgroundColor: 'buttonColorHover',
        borderWidth: '3px',
        borderStyle: 'solid',
        borderColor: 'buttonColorHover',
        color: 'buttonColorHover',
    },
    color: 'buttonColor',
    fontWeight: 'extrabold',

    cursor: 'pointer',
    '&:active': {
        boxShadow: 'none',
        border: 'none',
        top: '1px',
    },
}
const NextButton = {
    ...FancyButton,
    textAlign: 'left',
    pl: '1rem',
}
const PrevButton = {
    ...FancyButton,
    textAlign: 'right',
    pr: '1rem',
    boxSizing: 'border-box',
}
const SiteButton = {
    ...FancyButton,
    width: '40px',
    textAlign: 'center',
    textIndent: 0,
    '&:after': {
        display: 'none',
    },
}

const NextPrev = props => {
    // console.log('NextPrev', props)
    const { next, prev, nextText, prevText } = props

    return (
        <React.Fragment>
            <Flex
                sx={{
                    my: 5,
                    p: 3,
                    flexFlow: 'row nowrap',
                    justifyContent: `${next ? (prev ? 'space-between' : 'flex-start') : 'flex-end'}`,
                }}>
                {next && (
                    <Styled.a
                        as={Link}
                        to={next}
                        sx={{
                            variant: 'buttons.site',
                        }}>
                        <span sx={{ fontWeight: 'extrabold', whiteSpace: 'nowrap' }}>{nextText}</span>
                    </Styled.a>
                )}
                {prev && (
                    <Styled.a as={Link} to={prev} sx={{ variant: 'buttons.site' }}>
                        <span sx={{ fontWeight: 'extrabold', whiteSpace: 'nowrap' }}>{prevText}</span>
                    </Styled.a>
                )}
            </Flex>
        </React.Fragment>
    )
}

NextPrev.propTypes = {
    next: PropTypes.string,
    prev: PropTypes.string,
    nextText: PropTypes.string,
    prevText: PropTypes.string,
}

NextPrev.defaultProps = {
    next: null,
    prev: null,
    nextText: '🡠 Next',
    prevText: 'Previous 🡢',
}

export default NextPrev
