/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import { darken } from '@theme-ui/color'
import { Link } from 'gatsby'
import React from 'react'
import { animation } from 'theme'
import theme from 'theme'
// import { darken } from 'polished'

const HeaderLogo = props => {
    return (
        <React.Fragment>
            <Box
                sx={{
                    ...theme.layout.logo,
                    display: 'flex',
                    flexFlow: 'column nowrap',
                    justifyContent: 'flex-end',
                    alignItems: 'flex-end',
                    zIndex: 'header',
                }}
                {...props}>
                <Link
                    key='home'
                    to='/'
                    sx={{
                        variant: 'styles.logo',
                        m: 0,
                        p: 0,
                        color: `primary`,
                        textDecoration: 'none',
                        transition: `${animation.transition}, font-size 0.2s`,
                        '&:focus': {
                            outline: `none`,
                            boxShadow: `none`,
                        },
                        '&:before, &:after': {
                            content: 'none',
                        },
                        '&:hover': {
                            cursor: 'pointer',
                            textDecoration: 'none',
                            color: 'accent',
                        },
                    }}>
                    <span>Robert</span>
                    <br />
                    <span>Moggach</span>
                </Link>
            </Box>
        </React.Fragment>
    )
}

export default HeaderLogo
