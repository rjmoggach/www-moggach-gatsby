/** @jsx jsx */
import { jsx } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'
import { ContextConsumer } from 'components/Context'
import { MenuChanger, MenuSetterBlog, MenuSetterWork, MenuSetterPlay } from 'components/Header/MenuSetter'

const MenuSetter = props => {
    const { source, open } = props
    // separate components because of <StaticQuery/> limit
    switch (source) {
        case 'work': {
            return <MenuSetterWork source={source} open={open} />
            break
        }
        case 'blog': {
            return <MenuSetterBlog source={source} open={open} />
            break
        }
        case 'play': {
            return <MenuSetterPlay source={source} open={open} />
            break
        }
        default: {
            return (
                <ContextConsumer>
                    {({ data, set }) => (
                        <React.Fragment>
                            <MenuChanger data={data} set={set} source={source} open={open} />
                        </React.Fragment>
                    )}
                </ContextConsumer>
            )
            break
        }
    }
}

MenuSetter.propTypes = {
    source: PropTypes.string,
    open: PropTypes.bool,
}

MenuSetter.defaultProps = {
    source: '',
    open: false,
}

export default MenuSetter
