/** @jsx jsx */
import { jsx } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'
import { ContextConsumer } from 'components/Context'
import { MenuChanger } from 'components/Header/MenuSetter'

const MenuSetterWork = props => {
    const { source, open } = props
    return (
        <StaticQuery
            query={graphql`
                query {
                    source: allMarkdownRemark(
                        filter: { frontmatter: { published: { ne: false } }, fields: { sourceName: { eq: "work" } } }
                    ) {
                        categories: group(field: frontmatter___categories) {
                            label: fieldValue
                        }
                    }
                }
            `}
            render={queryData => (
                <ContextConsumer>
                    {({ data, set }) => {
                        return (
                            <MenuChanger
                                data={data}
                                set={set}
                                source={source}
                                open={open}
                                items={queryData.source.categories}
                            />
                        )
                    }}
                </ContextConsumer>
            )}
        />
    )
}

MenuSetterWork.propTypes = {
    source: PropTypes.string,
    open: PropTypes.bool,
}

MenuSetterWork.defaultProps = {
    source: 'work',
    open: false,
}

export default MenuSetterWork
