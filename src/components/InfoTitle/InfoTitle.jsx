/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'

import Moment from 'react-moment'
import { CategoryList } from 'components/InfoTitle'

const InfoTitle = props => {
    // console.log('InfoTitle', props)
    const { categories, date, source, center } = props

    return (
        <Styled.h5
            sx={{
                opacity: 0.6,
                fontSize: '75%',
                m: 0,
                my: 2,
                color: `muted`,
                textDecoration: 'none',
                letterSpacing: 'wide',
                textDecoration: 'none',
                '&:hover': {
                    opacity: 1,
                },
                transition: 'opacity 0.15s ease-in, color 0.15s ease-in',
                textAlign: center ? 'center' : 'left',
            }}>
            {categories.length ? (
                <React.Fragment>
                    <CategoryList categories={categories} source={source ? source : null} />
                    &nbsp;/&nbsp;
                </React.Fragment>
            ) : null}

            <Moment
                format='MMM D, YYYY'
                date={date}
                sx={{
                    color: 'muted',
                    textDecoration: 'none',
                }}
            />
        </Styled.h5>
    )
}

InfoTitle.propTypes = {
    categories: PropTypes.arrayOf(PropTypes.string),
    // date: PropTypes.instanceOf(Date),
    date: PropTypes.string,
    source: PropTypes.string,
    center: PropTypes.bool,
}

InfoTitle.defaultProps = {
    categories: [],
    date: null,
    source: null,
    center: false,
}

export default InfoTitle
