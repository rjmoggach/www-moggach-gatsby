/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import { Link, navigate } from 'gatsby'
import kebabCase from 'lodash/kebabCase'

const animation = {
    transition: 'opacity 0.3s ease-in, color 0.3s ease-in',
}

const CategoryList = props => {
    const { categories, source } = props
    return (
        <React.Fragment>
            <ul
                sx={{
                    listStyleType: 'none',
                    textIndent: '0',
                    m: 0,
                    p: 0,
                    display: 'inline',
                    'li::after': {
                        content: '", "',
                    },
                    'li:last-child::after': {
                        content: '""',
                    },
                }}>
                {categories.map((category, index) => (
                    <li
                        sx={{
                            display: 'inline-block',
                            '&:after': {
                                content: '", "',
                            },
                            '&:last-child:after': {
                                content: '""',
                            },
                            m: 0,
                            p: 0,
                        }}
                        key={index}>
                        <Link
                            sx={{
                                variant: 'styles.a',
                                color: 'secondary',

                                transition: animation.transition,
                            }}
                            key={category}
                            to={(source ? '/' + source + '/' : '/') + 'categories/' + kebabCase(category)}
                            // onClick={() =>
                            //     navigate((source ? '/' + source + '/' : '/') + 'categories/' + kebabCase(category))
                            // }
                        >
                            {category}
                        </Link>
                    </li>
                ))}
            </ul>
        </React.Fragment>
    )
}

CategoryList.propTypes = {
    categories: PropTypes.arrayOf(PropTypes.string),
    source: PropTypes.string,
}

CategoryList.defaultProps = {
    categories: [],
    source: null,
}

export default CategoryList
