/** @jsx jsx */
import { jsx, Box, Flex } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'
import config from 'config'

// import Slider from 'react-slick'
// import 'slick-carousel/slick/slick.css'
// import 'slick-carousel/slick/slick-theme.css'

import { Fade } from 'react-slideshow-image'
import { AlbumFader } from 'components/ImageCarousel'
// https://github.com/femioladeji/react-slideshow#readme

// import ImageGallery from 'react-image-gallery'
// import 'react-image-gallery/styles/css/image-gallery.css'

// import AliceCarousel from 'react-alice-carousel'
// import 'react-alice-carousel/lib/alice-carousel.css'
// https://github.com/maxmarinich/react-alice-carousel

const style = {
    workListItemSlide: {
        height: '100%',
        div: {
            width: '100%',
            height: '100%',
        },
        img: {
            height: '100%',
        },
    },
}

const AlbumSlideshow = props => {
    const { albums, speed, list } = props
    const images = []
    // console.log('AlbumSlideshow', albums)
    if (!albums || albums.length === 0) return null

    albums.forEach(album => {
        const path = config.aws_cdn_prefix + '/' + album.path
        album.images.forEach(image => {
            images.push({
                original: `${path}/${image.filename}`,
                thumbnail: `${path}/${image.filename}`,
            })
        })
    })

    if (images.length === 0) return null

    const properties = {
        duration: speed,
        transitionDuration: 500,
        infinite: true,
        indicators: false,
        arrows: false,
        autoplay: true,
        infinite: true,
    }
    return !list ? (
        <Fade {...properties}>
            {images.map((image, i) => (
                <img src={image.original} key={i} sx={{ minWidth: '100%', width: '100%', height: '100%' }} />
            ))}
        </Fade>
    ) : (
        <Fade {...properties} sx={{ ...style.workListItemSlide }}>
            {images.map((image, i) => (
                <Box
                    key={i}
                    sx={{
                        backgroundImage: `url(${image.original})`,
                        backgroundSize: 'cover',
                        backgroundPosition: 'center center !important',
                    }}
                />
            ))}
        </Fade>
    )
}

AlbumSlideshow.propTypes = {
    albums: PropTypes.arrayOf(PropTypes.object),
    list: PropTypes.bool,
    speed: PropTypes.number,
}

AlbumSlideshow.defaultProps = {
    albums: [],
    list: false,
    speed: 2000,
}

export default AlbumSlideshow

// albums:
//     - images:
//           - caption: ''
//             filename: got-milk-thirsty.01.jpg
//           - caption: ''
//             filename: got-milk-thirsty.02.jpg
//       path: /work/2004/got-milk-thirsty/images/stills
//       cover:
//       slug: stills
//       title: Stills
