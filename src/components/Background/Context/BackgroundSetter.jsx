/** @jsx jsx */
import { jsx } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'
import { ContextConsumer } from 'components/Context'
import { BackgroundChanger } from 'components/Background/Context'

const BackgroundSetter = props => {
    const { children } = props
    // separate components because of <StaticQuery/> limit
    return (
        <ContextConsumer>
            {({ data, set }) => (
                <React.Fragment>
                    <BackgroundChanger data={data} set={set} background={children} />
                </React.Fragment>
            )}
        </ContextConsumer>
    )
}

BackgroundSetter.propTypes = {
    source: PropTypes.string,
    open: PropTypes.bool,
}

BackgroundSetter.defaultProps = {
    source: '',
    open: false,
}

export default BackgroundSetter
