/** @jsx jsx */
import { jsx } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'
import capitalize from 'lodash/capitalize'
import kebabCase from 'lodash/kebabCase'

class BackgroundChanger extends React.Component {
    componentDidMount() {
        // const data = this.props.data
        const { data, set, background } = this.props
        set({
            // ...data,
            background: background,
        })
    }

    render() {
        return <React.Fragment></React.Fragment>
    }
}

export default BackgroundChanger
