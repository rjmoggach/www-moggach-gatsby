import { BackgroundImageComponent } from './BackgroundImage'
import BackgroundContainer from './BackgroundContainer'
// import BackgroundSlideshow from './BackgroundSlideshow'

export { BackgroundImageComponent as BackgroundImage, BackgroundContainer }
