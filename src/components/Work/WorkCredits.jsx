/** @jsx jsx */
import { jsx, Styled } from 'theme-ui'

import React from 'react'
import { Link } from 'gatsby'

const WorkCredits = props => {
    // console.log('WorkCredits', props.credits)
    const { credits } = props
    const roleSlugs = [
        'client',
        'agency',
        'network',
        'studio',
        'production',
        'design-animation',
        'edit',
        'color',
        'vfx',
        'sound',
        'music',
    ]

    if (!credits || credits.length === 0) return null
    return (
        <React.Fragment>
            <h3 sx={{ textAlign: 'center', mt: 5 }}>Credits</h3>
            <dl
                sx={{
                    display: 'flex',
                    flexFlow: 'column',
                    listStyleType: 'none',
                    m: 0,
                    p: 0,
                    fontSize: 2,
                    lineHeight: 2,
                    textIndent: 'none',
                    div: {
                        display: 'flex',
                        flexFlow: 'row nowrap',
                        width: ['90%', '80%', '70%'],
                        minWidth: '414px',
                        margin: 'auto',
                        '&.heading': {
                            mt: 3,
                            dt: {
                                color: 'text',
                            },
                            dd: {
                                color: 'primary',
                            },
                        },
                    },
                    dt: {
                        display: 'contents',
                        color: 'muted',
                        '&:after': {
                            borderBottom: '1px dotted #ffffff',
                            borderColor: 'muted',
                            opacity: 0.25,
                            content: '""',
                            m: '0.5rem',
                            flex: '1 1 auto',
                        },
                    },

                    dd: {
                        color: 'secondary',

                        flex: '0 1 auto',
                        m: 0,
                        p: 0,
                        textIndent: 0,
                    },
                    'dt.heading + dd': {
                        color: 'primary',
                    },
                }}>
                {credits.map((credit, i) => {
                    // console.log('WorkCredits2', credit)
                    return roleSlugs.indexOf(credit.roleSlug) > -1 ? (
                        <div className='heading' key={i}>
                            <dt>{credit.roleTitle}</dt>
                            <dd>{credit.companyName || credit.personName}</dd>
                        </div>
                    ) : (
                        <div key={i}>
                            <dt>{credit.roleTitle}</dt>
                            <dd>{credit.personName || credit.companyName}</dd>
                        </div>
                    )
                })}
            </dl>
            <Styled.p sx={{ textAlign: 'center', color: 'muted', my: 4, fontStyle: 'italic' }}>
                <span sx={{ fontSize: '85%' }}>
                    This list is incomplete!{' '}
                    <Styled.a as={Link} to='/contact'>
                        Contact me
                    </Styled.a>{' '}
                    to update it.
                </span>
            </Styled.p>
        </React.Fragment>
    )
}

export default WorkCredits
