/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'

import React from 'react'
import Moment from 'react-moment'
import { CategoryList } from 'components/Work'

const WorkDetailCatDate = props => {
    const { categories, date } = props
    return (
        <Styled.h5
            sx={{
                opacity: 0.5,
                fontSize: '75%',
                m: 0,
                my: 3,
                mb: 5,
                color: `muted`,
                textDecoration: 'none',
                letterSpacing: 'wide',
                textDecoration: 'none',
                '&:hover': {
                    opacity: 0.8,
                },
                transition: 'opacity 0.3s ease-in, color 0.3s ease-in',
                textAlign: 'center',
            }}>
            {categories.length ? (
                <React.Fragment>
                    <CategoryList categories={categories} />
                    &nbsp;/&nbsp;
                </React.Fragment>
            ) : null}

            <Moment
                format='MMM D, YYYY'
                date={date}
                sx={{
                    color: 'muted',
                    textDecoration: 'none',
                }}
            />
        </Styled.h5>
    )
}

export default WorkDetailCatDate
