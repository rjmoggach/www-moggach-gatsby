/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import React from 'react'
import { StaticQuery, graphql } from 'gatsby'
import { MenuSetter } from 'components/Header'
import { TagList } from 'components/Collections'

const WorkTags = props => {
    const { tags } = props
    return (
        <Box sx={{ position: 'relative', mt: ['27vmin', '30vmin', '31vmin', '31vmin', '30vmin'] }}>
            <MenuSetter source='work' />
            <Styled.h1 sx={{ textAlign: ['center', 'center', 'left'], mb: [2, 2, 4] }}>Tags</Styled.h1>
            <TagList sourceName='work' tags={tags} />
        </Box>
    )
}

export default WorkTags
