import WorkHome from './WorkHome'
import WorkDetail from './WorkDetail'
import CategoryList from './CategoryList'
import WorkCredits from './WorkCredits'
import WorkDetailCatDate from './WorkDetailCatDate'
import WorkTags from './WorkTags'

export { WorkHome, WorkDetail, CategoryList, WorkCredits, WorkDetailCatDate, WorkTags }
