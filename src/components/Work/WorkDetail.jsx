/** @jsx jsx */
import { jsx, Styled, Box } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'
import { Video } from 'components/Video'
import { AlbumSlideshow } from 'components/ImageCarousel'
import { WorkCredits, WorkDetailCatDate } from 'components/Work'
import { TagList } from 'components/Collections'
import { NextPrev } from 'components/NextPrev'
import { InfoTitle } from 'components/InfoTitle'
import { SEO } from 'components/SEO'

class WorkDetail extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { post, context } = this.props
        const {
            html,
            frontmatter: { categories, date, tags, title: titleRaw, headline, videos, albums, credits },
        } = post
        const [title, subtitle] = titleRaw.split('|')
        const { next, prev } = context
        // console.log('WorkDetail', post)
        const helmetTitle = 'Work: ' + title + subtitle
        return (
            <Box sx={{ variant: 'styles.spacer.detail' }}>
                <SEO title={helmetTitle} sourceName='work' />
                <Box sx={{ variant: 'styles.roundedBox' }}>
                    <AlbumSlideshow albums={albums} speed={1000 + Math.round(Math.random() * 1000)} />
                </Box>
                <Styled.h1
                    sx={{ fontWeight: 'extrabold', textAlign: 'center', mt: 5, textShadow: 'lg', lineHeight: 'tight' }}>
                    {title}
                </Styled.h1>
                {subtitle && (
                    <Styled.h2
                        sx={{
                            textAlign: 'center',
                            lineHeight: 'tight',
                        }}>
                        {subtitle}
                    </Styled.h2>
                )}
                <InfoTitle categories={categories} date={date} source='work' center />
                <article sx={{ mt: 5 }} dangerouslySetInnerHTML={{ __html: html }} />

                {videos && videos.map((v, i) => <Video key={i} video={v} caption={v.caption} />)}
                {credits && <WorkCredits credits={credits} />}

                {tags.length != 0 && <h3 sx={{ textAlign: 'center', mt: 5 }}>Tags</h3>}

                {tags.length != 0 && <TagList tags={tags} source='work' />}

                <NextPrev
                    prev={next ? next.fields.slug : ''}
                    next={prev ? prev.fields.slug : ''}
                    nextText='&laquo; Newer'
                    prevText='Older &raquo;'
                />
            </Box>
        )
    }
}

WorkDetail.propTypes = {
    post: PropTypes.object,
}

export default WorkDetail
