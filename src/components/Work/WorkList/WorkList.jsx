/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

import { WorkListItem } from 'components/Work/WorkList'
import { NextPrev } from 'components/NextPrev'

const WorkList = props => {
    const { work, pageContext } = props
    // console.log('WorkList', work)
    return (
        <Box>
            <Flex
                sx={{
                    flexFlow: ['column nowrap', 'row wrap'],
                }}>
                {work.map((project, index) => (
                    <WorkListItem project={project} key={index} />
                ))}
            </Flex>
            <NextPrev
                prev={pageContext.nextPagePath || ''}
                next={pageContext.previousPagePath || ''}
                nextText='Newer'
                prevText='Older'
            />
        </Box>
    )
}

export default WorkList

// <WorkListItem project={project} key={index} />
