/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import { darken } from '@theme-ui/color'
import React from 'react'
import PropTypes from 'prop-types'

import { Link, navigate } from 'gatsby'
import Moment from 'react-moment'
import Picture from 'gatsby-image'
import { AlbumSlideshow } from 'components/ImageCarousel'

import { CategoryList } from 'components/Work'
import { animation } from 'theme'
import { WorkListTitle } from 'components/Work/WorkList'

const WorkGridInnerBox = props => {
    const { children } = props
    return (
        <Flex
            sx={{
                position: 'absolute',
                backgroundColor: 'transparent',
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
                m: 2,
                boxShadow: 'xl',
                flexFlow: 'column nowrap',
                borderRadius: '1vmax',
                overflow: 'hidden',
            }}
            children={children}
        />
    )
}

class WorkListItem extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            hover: false,
        }
    }

    toggleIn = () => {
        this.setState(state => {
            return {
                hover: true,
            }
        })
    }

    toggleOut = () => {
        this.setState(state => {
            return {
                hover: false,
            }
        })
    }
    render() {
        // console.log('WorkListItem', this.props)
        const {
            project: {
                node: {
                    fields: { slug },
                    frontmatter: { categories, date, title: titleRaw, albums },
                },
            },
        } = this.props

        const [title, subtitle] = titleRaw.split('|')
        const aspect = 1.777
        const columns = [1, 1, 1, 1, 2, 3, 4]
        const columnWidth = columns.map(columns => (100 / columns).toFixed(2).toString() + '%')
        const pb = columns.map(columns => (100 / columns / aspect).toFixed(2).toString() + '%')

        return (
            <Box
                sx={{
                    width: columnWidth,
                    height: 0,
                    paddingBottom: pb,
                    position: 'relative',
                    '&:hover': {
                        cursor: 'pointer',
                    },
                }}
                onMouseEnter={this.toggleIn}
                onMouseLeave={this.toggleOut}>
                <WorkGridInnerBox>
                    <Box
                        sx={{
                            position: 'absolute',
                            left: 0,
                            right: 0,
                            top: 0,
                            bottom: 0,
                            flexFlow: 'column wrap',
                            alignItems: 'center',
                            justifyContent: 'center',
                            alignContent: 'center',
                            opacity: `${this.state.hover ? 0.2 : 1}`,
                            transition: animation.transition,
                            transform: `scale(${this.state.hover ? 1.2 : 1})`,
                            transition: 'all 0.4s',
                        }}>
                        <AlbumSlideshow albums={albums} speed={5000} list />
                    </Box>
                    <Box
                        sx={{
                            opacity: `${this.state.hover ? 1 : 0}`,
                            transition: 'all 0.5s 0.1s',
                            letterSpacing: `${this.state.hover ? 0 : -1}px`,
                            transition: animation.transition + ',letter-spacing 0.6s ease',
                        }}>
                        <WorkListTitle
                            title={title}
                            subtitle={subtitle}
                            slug={slug}
                            categories={categories}
                            date={date}
                            hover={this.state.hover}
                        />
                    </Box>
                </WorkGridInnerBox>
            </Box>
        )
    }
}

WorkListItem.propTypes = {
    post: PropTypes.object,
}

export default WorkListItem
// <AlbumSlideshow albums={albums} speed={5000} />
