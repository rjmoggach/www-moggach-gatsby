/** @jsx jsx */
import { jsx, Styled, Box } from 'theme-ui'

import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import { Link } from 'gatsby'

export const HeroHeaderText = props => {
    const { showidx, hookidx } = props

    return (
        <Styled.h2
            sx={{
                variant: 'text.hero.header',
                color: 'accent',
                mb: 3,
            }}
            {...props}
        />
    )
}

export const HeroBodyText = props => {
    const { showidx, hookidx } = props

    return (
        <Styled.p
            sx={{
                variant: 'text.hero.body',
                color: 'text',
                mb: 3,
            }}
            {...props}
        />
    )
}

export const HeroInlineHeaderText = props => {
    const { showidx, hookidx } = props

    return (
        <Styled.h2
            sx={{
                variant: 'text.hero.header',
                color: 'text',
                pr: 3,
                display: 'inline-block',
                visibility: `${showidx > hookidx ? 'visible' : 'hidden'}`,
                opacity: `${hookidx ? (showidx > hookidx ? '1' : '0') : '1'}`,
            }}
            {...props}
        />
    )
}

export const HeroInlineBodyText = props => {
    const { showidx, hookidx } = props

    return (
        <Styled.p
            sx={{
                variant: 'text.hero.body',
                opacity: `${hookidx ? (showidx > hookidx ? '1' : '0') : '1'}`,
                display: 'inline-block',
                color: 'text',
                pr: 3,
            }}>
            {props.children}
        </Styled.p>
    )
}

export const HeroInlineLink = props => {
    const { showidx, hookidx } = props
    const animation = {
        duration: '0.15s',
        easing: 'cubic-bezier(0.175, 0.885, 0.320, 1.275)',
    }

    return (
        <Styled.h2
            as={Link}
            sx={{
                color: 'primary',
                position: 'relative',
                textDecoration: 'none',
                '&:before': {
                    content: '',
                },
                '&:after': {
                    content: '""',
                    position: 'absolute',
                    transform: 'translateY(0.2rem)',
                    height: '0.2rem',
                    bottom: 0,
                    width: '90%',
                    left: '5%',
                    opacity: 0.7,
                    backgroundColor: 'secondary',
                    transition: '0.3s ease all .1s',
                },
                '&:hover': {
                    color: 'accent',
                    '&:after': {
                        transform: 'translateY(0.3rem)',
                        width: '100%',
                        left: '0%',
                        opacity: 1,
                        backgroundColor: 'primary',
                    },
                },
                visibility: `${showidx > hookidx ? 'visible' : 'hidden'}`,
                opacity: `${hookidx ? (showidx > hookidx ? '1' : '0') : '1'}`,
                display: 'inline-block',
                transition: 'color 0.3s ease, opacity 1s',
            }}>
            {props.children}
        </Styled.h2>
    )
}

export const HeroWrapper = props => {
    const { showidx, hookidx } = props
    return (
        <div
            sx={{
                width: '50%',
                display: 'inline-block',
                verticalAlign: 'top',
                visibility: `${showidx > hookidx ? 'visible' : 'hidden'}`,
                opacity: `${showidx > hookidx ? '1' : '0'}`,
            }}
            {...props}
        />
    )
}

export default {
    HeroHeaderText,
    HeroBodyText,
    HeroInlineHeaderText,
    HeroInlineLink,
    HeroInlineBodyText,
    HeroWrapper,
}
