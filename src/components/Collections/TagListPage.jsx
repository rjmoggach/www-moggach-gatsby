/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'
import { MenuSetter } from 'components/Header'
import { TagList } from 'components/Collections'

const TagListPage = props => {
    const { tags, source } = props
    return (
        <Box sx={{ position: 'relative', mt: ['27vmin', '30vmin', '31vmin', '31vmin', '30vmin'] }}>
            {source != 'all' ? (
                <React.Fragment>
                    <MenuSetter source={source} />
                    <Styled.h1 sx={{ textAlign: ['center', 'center', 'left'], mb: [2, 2, 4] }}>Tags</Styled.h1>
                    <TagList source={source} tags={tags} />
                </React.Fragment>
            ) : (
                <React.Fragment>
                    <MenuSetter source='tags' />
                    <TagList tags={tags} />
                </React.Fragment>
            )}
        </Box>
    )
}

TagListPage.propTypes = {
    tags: PropTypes.arrayOf(PropTypes.string),
    source: PropTypes.string,
}

TagListPage.defaultProps = {
    tags: [],
    source: '',
}
export default TagListPage
