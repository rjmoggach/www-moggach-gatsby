/** @jsx jsx */
import { jsx } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'
import { ContextConsumer } from 'components/Context'

const Pusher = props => {
    const { children, margintop } = props
    return (
        <React.Fragment>
            <ContextConsumer>
                {({ data, set }) => (
                    <div
                        sx={{
                            mt: [
                                `${data.menuOpen ? margintop[0] + 3 : margintop[0]}rem`,
                                `${data.menuOpen ? margintop[1] + 3 : margintop[1]}rem`,
                                `${margintop[2]}rem`,
                                `${margintop[3]}rem`,
                                `${margintop[4]}rem`,
                            ],
                            transition: 'margin 0.15s ease-in',
                        }}>
                        {children}
                    </div>
                )}
            </ContextConsumer>
        </React.Fragment>
    )
}
Pusher.propTypes = {
    margintop: PropTypes.array,
}

Pusher.defaultProps = {
    margintop: [0],
}

export default Pusher
