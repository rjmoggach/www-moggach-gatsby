/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'

import React, { useState, useEffect } from 'react'

import { StaticQuery, graphql, Link } from 'gatsby'
import PropTypes from 'prop-types'

import { animation } from 'theme'
import { Revealer } from 'components/Revealer'
import { BackgroundImage } from 'components/Background'

import { BackgroundSetter } from 'components/Background/Context'

const About = () => (
    <StaticQuery
        query={graphql`
            query {
                index: markdownRemark(fileAbsolutePath: { glob: "**/about/index.md" }) {
                    html
                    fields {
                        slug
                        sourceName
                    }
                    frontmatter {
                        date
                        slug
                        title
                        headline
                    }
                    id
                }
                bio: markdownRemark(fileAbsolutePath: { glob: "**/about/bio.md" }) {
                    html
                }
                feature: markdownRemark(fileAbsolutePath: { glob: "**/about/feature.md" }) {
                    html
                }
                commercial: markdownRemark(fileAbsolutePath: { glob: "**/about/commercial.md" }) {
                    html
                }
                desktopImage: imageSharp(fluid: { originalName: { regex: "/rob-hand-shadow.jpg/" } }) {
                    fluid(quality: 90, maxWidth: 1000, cropFocus: CENTER) {
                        ...GatsbyImageSharpFluid
                    }
                }
                mobileImage: imageSharp(fluid: { originalName: { regex: "/rob-hand-shadow.jpg/" } }) {
                    fluid(quality: 90, maxWidth: 800, cropFocus: CENTER) {
                        ...GatsbyImageSharpFluid
                    }
                }
            }
        `}
        render={data => {
            // console.log('About', data)
            // const titleWords = data.title.frontmatter.title.split('|')

            const [showidx, setShowIndex] = useState(0)

            useEffect(() => {
                const timer = showidx < 7 ? setTimeout(() => setShowIndex(showidx + 1), animation.page.ms) : null
                return () => (timer ? clearTimeout(timer) : null)
            })

            return (
                <Box sx={{ variant: 'styles.spacer.h2' }}>
                    <BackgroundSetter>
                        <BackgroundImage imageData={data.desktopImage.fluid} className='div-background' />
                    </BackgroundSetter>
                    <div sx={{ zIndex: 'content' }}>
                        <Styled.h2 sx={{ color: 'primary' }}>
                            <Revealer showidx={showidx} hookidx='0'>
                                <span
                                    sx={{
                                        mr: 2,
                                    }}>
                                    I Collaborate.
                                </span>
                            </Revealer>
                            <Revealer showidx={showidx} hookidx='1'>
                                <span
                                    sx={{
                                        mr: 2,
                                    }}>
                                    I Craft.
                                </span>
                            </Revealer>
                            <Revealer showidx={showidx} hookidx='2'>
                                <span sx={{ '&:hover': { color: 'secondary' } }}>
                                    <Link to='/work/' sx={{ variant: 'styles.LinkHeavy' }}>
                                        I Create.
                                    </Link>
                                </span>
                            </Revealer>
                        </Styled.h2>

                        <Revealer showidx={showidx} hookidx='3'>
                            <Styled.root
                                as='div'
                                sx={{
                                    variant: 'text.hero.body',
                                    color: 'text',
                                    mb: 3,
                                    maxWidth: '60rem',
                                    clear: 'both',
                                    fontSize: '120%',
                                }}
                                dangerouslySetInnerHTML={{ __html: data.index.html }}
                            />
                        </Revealer>
                        <Revealer showidx={showidx} hookidx='4'>
                            <Styled.root
                                as='div'
                                sx={{
                                    variant: 'text.hero.body',
                                    color: 'text',
                                    mb: 7,
                                    maxWidth: '60rem',
                                    clear: 'both',
                                }}
                                dangerouslySetInnerHTML={{ __html: data.bio.html }}
                            />
                        </Revealer>
                    </div>
                </Box>
            )
        }}
    />
)

export default About
