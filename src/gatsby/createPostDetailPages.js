const _ = require('lodash')
const path = require(`path`)
const { getPrevNextFromList } = require('./getPrevNextFromList')

const createPostDetailPages = (edges, createPage, template, prefix) => {
    edges.forEach((post, index) => {
        // const { prev, next } = getPrevNextFromList(edges, post)
        const { slug } = post.node.fields

        createPage({
            path: prefix ? `/${prefix}${slug}` : slug,
            component: template,
            context: {
                slug,
                prev: index === 0 ? null : edges[index - 1].node,
                next: index === edges.length - 1 ? null : edges[index + 1].node,
            },
        })
    })
}

module.exports = {
    createPostDetailPages,
}
