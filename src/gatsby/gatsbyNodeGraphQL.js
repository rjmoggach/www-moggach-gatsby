const gatsbyNodeGraphQL = `
work: allMarkdownRemark(
    filter: { 
        fileAbsolutePath: {regex: "/work/"}
        frontmatter: { published: { eq: true } }
        fields: { sourceName: {eq: "work"}}
    }
    sort: { order: DESC, fields: [frontmatter___date] }
    limit: 1000
) {
    edges {
        node {
            id
            fields {
                slug
                sourceName
            }
            frontmatter {
                featured
                tags
                title
                subtitle
                date
                albums {
                    images {
                        caption
                        filename
                    }
                    path
                    slug
                    title
                }
                categories
                credits {
                    companyName
                    companySlug
                    personName
                    personSlug
                    roleSlug
                    roleTitle
                    visible
                }
            }
        }
    }
}
blog: allMarkdownRemark(
    filter: {
        fileAbsolutePath: {regex: "/blog/"}
        frontmatter: { published: { eq: true } }
        fields: { sourceName: {eq: "blog"}}
    }
    sort: { order: DESC, fields: [frontmatter___date] }
    limit: 1000
) {
    edges {
        node {
            id
            fields {
                slug
                sourceName
            }
            frontmatter {
                featured
                tags
                categories
                title
                subtitle
                date
            
            }
        }
    }
}
play: allMarkdownRemark(
    filter: {
        fileAbsolutePath: {regex: "/play/"}
        frontmatter: { published: { eq: true } }
        fields: { sourceName: {eq: "play"}}
    }
    sort: { order: DESC, fields: [frontmatter___date] }
    limit: 1000
) {
    edges {
        node {
            id
            fields {
                slug
                sourceName
            }
            frontmatter {
                featured
                tags
                categories
                title
                subtitle
            }
        }
    }
}
all: allMarkdownRemark(
        filter: { 
            fileAbsolutePath: {regex: "/work|blog|play/"}
            frontmatter: { published: { ne: false } }
            fields: { sourceName: {regex: "/work|blog|play/"}}
        }
        sort: { order: DESC, fields: [frontmatter___date] }
        limit: 1000
    ) {
        edges {
            node {
                id
                fields {
                    slug
                    sourceName
                }
                frontmatter {
                    tags
                    categories
                    title
                    subtitle
                    date
                    albums {
                        images {
                            caption
                            filename
                        }
                        path
                        slug
                        title
                    }
                }
            }
        }
    }
`

module.exports = gatsbyNodeGraphQL
