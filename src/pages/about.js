import React from 'react'
import { MenuSetter } from 'components/Header'
import { About } from 'components/About'
import { SEO } from 'components/SEO'
const AboutPage = () => {
    return (
        <React.Fragment>
            <SEO title='About' />
            <MenuSetter source='about' />
            <About />
        </React.Fragment>
    )
}

export default AboutPage
