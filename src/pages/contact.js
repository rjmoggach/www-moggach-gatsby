import React from 'react'
import { MenuSetter } from 'components/Header'
import { Contact } from 'components/Contact'
import { SEO } from 'components/SEO'

const ContactPage = () => {
    return (
        <React.Fragment>
            <SEO title='Contact' />
            <MenuSetter source='contact' />
            <Contact />
        </React.Fragment>
    )
}

export default ContactPage
