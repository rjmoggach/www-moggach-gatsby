/** @jsx jsx */
import { jsx, Styled, Box } from 'theme-ui'
import React from 'react'
import { graphql, Link } from 'gatsby'
import startCase from 'lodash/startCase'
import { WorkList } from 'components/Work/WorkList'
import { MenuSetter } from 'components/Header'

const CollectionDetailTemplate = ({ data, pageContext }) => {
    const { edges, collection, sourceName: source, collectionName } = pageContext
    let verb
    switch (collectionName) {
        case 'categories':
            verb = 'Category'
            break
        case 'tags':
            verb = 'Tagged'
            break
        default:
            verb = 'Posts about '
    }

    // console.log('SingleCollection', pageContext)
    switch (source) {
        case 'work': {
            return (
                <Box sx={{ variant: 'styles.spacer.list' }}>
                    <MenuSetter source='work' />
                    <Styled.h2 sx={{ mb: [2, 3, 4, 5], textAlign: ['center', 'center', 'left'] }}>
                        {startCase(collection)}
                    </Styled.h2>
                    <WorkList work={pageContext.edges} pageContext={pageContext} />
                </Box>
            )
            break
        }
        case 'blog': {
            console.log('CollectionDetailTemplate', edges)
            return (
                <React.Fragment>
                    <h2>{`${startCase(collection)} ${verb}`}</h2>
                    <div>
                        <ul>
                            {edges.map(({ node }, index) => (
                                <li key={index}>
                                    <Link sx={{ variant: 'styles.a' }} to={node.fields.slug}>
                                        {node.frontmatter.title}
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    </div>
                </React.Fragment>
            )
            break
        }
        default: {
            return (
                <div>
                    <h1>{`${startCase(collection)}`}</h1>
                    <div>
                        <ul>
                            {edges.map(({ node }, index) => (
                                <li key={index}>
                                    <Link sx={{ variant: 'styles.a' }} to={node.fields.slug}>
                                        {node.frontmatter.title}
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            )
            break
        }
    }
}

export default CollectionDetailTemplate
// export const pageQuery = graphql`
//     query($tag: String) {
//         allMarkdownRemark(
//             limit: 2000
//             sort: { fields: [frontmatter___date], order: DESC }
//             filter: { frontmatter: { tags: { in: [$tag] } } }
//         ) {
//             totalCount
//             edges {
//                 node {
//                     fields {
//                         slug
//                     }
//                     frontmatter {
//                         title
//                     }
//                 }
//             }
//         }
//     }
// `
