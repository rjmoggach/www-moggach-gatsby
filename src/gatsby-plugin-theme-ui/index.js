/**
 * This theme uses `theme-ui` under the hood.
 * @see https://theme-ui.com/
 * @see https://theme-ui.com/gatsby-plugin/
 */
import { transparentize, darken } from 'polished'
// Device	        Native (Pixels)	UIKit (Points)
// iPhone 8 Plus     1080 x 1920     414 x 736
// iPhone 8	          750 x 1334     375 x 667
// iPhone 7 Plus     1080 x 1920     414 x 736
// iPhone 6s Plus    1080 x 1920     375 x 667
export const breakpoints = ['480px', '720px', '960px', '1600px', '1920px', '2880px']
// export const breakpoints = ['480px', '640px', '960px', '1280px', '1920px', '2880px']

// TYPOGRAPHY ---------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------
// https://www.modularscale.com/?30,20&px&1.618
import ms from 'modularscale-js'
import { relative } from 'upath'
const modularscale = {
    base: [0.6],
    ratio: 1.2,
}

export const space = [0, `0.5rem`, `1rem`, `2rem`, `3rem`, `4rem`, `8rem`, `16rem`, `32rem`, '64rem']

const baseFonts = {
    system: `-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, Cantarell, "Fira Sans", "Droid Sans"`,
    sans: `Nunito, -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"`,
    serif: `Montserrat, Georgia ,Times, "Times New Roman" ,"MS Serif", "New York", Palatino, Garamond, serif`,
    mono: `Menlo, Monaco, "Courier New", monospace`,
}
const selection = {
    '::selection': {
        background: `highlight`,
        backgroundColor: `highlight`,
        textShadow: `none`,
        color: `text`,
    },
}

export const fonts = {
    ...baseFonts,
    body: baseFonts.serif,
    heading: baseFonts.sans,
    default: baseFonts.sans,
}

export const fontSizes = [
    `${ms(0, modularscale).toFixed(3)}rem`,
    `${ms(1, modularscale).toFixed(3)}rem`,
    `${ms(2, modularscale).toFixed(3)}rem`,
    `${ms(3, modularscale).toFixed(3)}rem`,
    `${ms(4, modularscale).toFixed(3)}rem`,
    `${ms(5, modularscale).toFixed(3)}rem`,
    `${ms(6, modularscale).toFixed(3)}rem`,
    `${ms(7, modularscale).toFixed(3)}rem`,
    `${ms(8, modularscale).toFixed(3)}rem`,
    `${ms(9, modularscale).toFixed(3)}rem`,
    `${ms(10, modularscale).toFixed(3)}rem`,
]

// export const fontSizes = [
//     `${ms(-1, modularscale).toFixed(3)}vw`,
//     `${ms(0, modularscale).toFixed(3)}vw`,
//     `${ms(1, modularscale).toFixed(3)}vw`,
//     `${ms(2, modularscale).toFixed(3)}vw`,
//     `${ms(3, modularscale).toFixed(3)}vw`,
//     `${ms(4, modularscale).toFixed(3)}vw`,
//     `${ms(5, modularscale).toFixed(3)}vw`,
//     `${ms(6, modularscale).toFixed(3)}vw`,
//     `${ms(7, modularscale).toFixed(3)}vw`,
//     `${ms(8, modularscale).toFixed(3)}vw`,
// ]

const baseLineHeights = {
    none: `${ms(1, modularscale).toFixed(3)}`,
    tight: `${ms(2, modularscale).toFixed(3)}`,
    snug: `${ms(3, modularscale).toFixed(3)}`,
    normal: `${ms(4, modularscale).toFixed(3)}`,
    relaxed: `${ms(5, modularscale).toFixed(3)}`,
    loose: `${ms(6, modularscale).toFixed(3)}`,
}

export const lineHeights = {
    ...baseLineHeights,
    text: baseLineHeights.normal,
    body: baseLineHeights.normal,
    heading: baseLineHeights.tight,
    default: baseLineHeights.snug,
}

// nunito 200, 300, 400, 600, 700, 800, 900
// aleo 300,400,700
const baseFontWeights = {
    light: `300`,
    normal: `400`,
    bold: `700`,
    extrabold: `800`,
    super: `900`,
}

export const fontWeights = {
    body: baseFontWeights.normal,
    heading: baseFontWeights.bold,
    ...baseFontWeights,
}

export const letterSpacings = {
    tighter: `-0.05em`,
    tight: `-0.025em`,
    normal: `0.01em`,
    wide: `0.025em`,
    wider: `0.05em`,
    widest: `0.1em`,
}

export const prism = {
    'code[class*="language-"], pre[class*="language-"]': {
        '-moz-tab-size': 4,
        '-o-tab-size': 4,
        tabSize: 4,
        '-webkit-hyphens': 'none',
        '-moz-hyphens': 'none',
        '-ms-hyphens': 'none',
        hyphens: 'none',
        whiteSpace: 'pre',
        whiteSpace: 'pre-wrap',
        wordBreak: 'break-all',
        wordWrap: 'break-word',
        fontFamily: 'Menlo, Monaco, "Courier New", monospace',
        fontSize: '15px',
        lineHeight: 1.5,
        color: 'text',
        textShadow: 0,
    },

    'pre > code[class*="language-"]': {
        fontSize: '1em',
    },

    'pre[class*="language-"], :not(pre) > code[class*="language-"]': {
        borderRadius: '1rem',
        color: '#DCCF8F',
        background:
            '#181914 url("data:image/jpeg,base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAMAAA/+4ADkFkb2JlAGTAAAAAAf/bAIQACQYGBgcGCQcHCQ0IBwgNDwsJCQsPEQ4ODw4OERENDg4ODg0RERQUFhQUERoaHBwaGiYmJiYmKysrKysrKysrKwEJCAgJCgkMCgoMDwwODA8TDg4ODhMVDg4PDg4VGhMRERERExoXGhYWFhoXHR0aGh0dJCQjJCQrKysrKysrKysr/8AAEQgAjACMAwEiAAIRAQMRAf/EAF4AAQEBAAAAAAAAAAAAAAAAAAABBwEBAQAAAAAAAAAAAAAAAAAAAAIQAAEDAwIHAQEAAAAAAAAAAADwAREhYaExkUFRcYGxwdHh8REBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8AyGFEjHaBS2fDDs2zkhKmBKktb7km+ZwwCnXPkLVmCTMItj6AXFxRS465/BTnkAJvkLkJe+7AKKoi2AtRS2zuAWsCb5GOlBN8gKfmuGHZ8MFqIth3ALmFoFwbwKWyAlTAp17uKqBvgBD8sM4fTjhvAhkzhaRkBMKBrfs7jGPIpzy7gFrAqnC0C0gB0EWwBDW2cBVQwm+QtPpa3wBO3sVvszCnLAhkzgL5/RLf13cLQd8/AGlu0Cb5HTx9KuAEieGJEdcehS3eRTp2ATdt3CpIm+QtZwAhROXFeb7swp/ahaM3kBE/jSIUBc/AWrgBN8uNFAl+b7sAXFxFn2YLUU5Ns7gFX8C4ib+hN8gFWXwK3bZglxEJm+gKdciLPsFV/TClsgJUwKJ5FVA7tvIFrfZhVfGJDcsCKaYgAqv6YRbE+RWOWBtu7+AL3yRalXLyKqAIIfk+zARbDgFyEsncYwJvlgFRW+GEWntIi2P0BooyFxcNr8Ep3+ANLbMO+QyhvbiqdgC0kVvgUUiLYgBS2QtPbiVI1/sgOmG9uO+Y8DW+7jS2zAOnj6O2BndwuIAUtkdRN8gFoK3wwXMQyZwHVbClsuNLd4E3yAUR6FVDBR+BafQGt93LVMxJTv8ABts4CVLhcfYWsCb5kC9/BHdU8CLYFY5bMAd+eX9MGthhpbA1vu4B7+RKkaW2Yq4AQtVBBFsAJU/AuIXBhN8gGWnstefhiZyWvLAEnbYS1uzSFP6Jvn4Baxx70JKkQojLib5AVTey1jjgkKJGO0AKWyOm7N7cSpgSpAdPH0Tfd/gp1z5C1ZgKqN9J2wFxcUUuAFLZAm+QC0Fb4YUVRFsAOvj4KW2dwtYE3yAWk/wS/PLMKfmuGHZ8MAXF/Ja32Yi5haAKWz4Ydm2cSpgU693Atb7km+Zwwh+WGcPpxw3gAkzCLY+iYUDW/Z3Adc/gpzyFrAqnALkJe+7DoItgAtRS2zuKqGE3yAx0oJvkdvYrfZmALURbDuL5/RLf13cAuDeBS2RpbtAm+QFVA3wR+3fUtFHoBDJnC0jIXH0HWsgMY8inPLuOkd9chp4z20ALQLSA8cI9jYAIa2zjzjBd8gRafS1vgiUho/kAKcsCGTOGWvoOpkAtB3z8Hm8x2Ff5ADp4+lXAlIvcmwH/2Q==") repeat left top',
        backgroundColor: 'backgroundDarker',
        boxShadow: 'lg',
    },

    'pre[class*="language-"]': {
        padding: '1.5rem',
        overflow: 'auto',
    },

    ':not(pre) > code[class*="language-"]': {
        px: 1,
        py: 2,
    },

    '.token.namespace': {
        opacity: 0.7,
    },
    '.token.comment, .token.prolog, .token.doctype, .token.cdata': {
        color: 'muted',
        fontStyle: 'italic',
    },
    '.token.number, .token.string, .token.char, .token.builtin, .token.inserted': {
        color: 'secondary',
    },

    '.token.attr-name': {
        color: 'highlight',
    },
    '.token.operator, .token.entity, .token.url, .language-css .token.string, .style .token.string': {
        color: 'accent',
    },
    '.token.selector, .token.regex': {
        color: '#859900',
    },
    '.token.atrule, .token.keyword': {
        color: 'highlight',
    },

    '.token.attr-value': {
        color: '#468966',
    },
    '.token.function, .token.variable, .token.placeholder': {
        color: '#b58900',
    },
    '.token.property, .token.tag, .token.boolean, .token.number, .token.constant, .token.symbol': {
        color: '#b89859',
    },
    '.token.tag': {
        color: '#ffb03b',
    },
    '.token.important, .token.statement, .token.deleted': {
        color: '#dc322f',
    },
    '.token.punctuation': {
        color: '#dccf8f',
    },
    '.token.entity': {
        cursor: 'help',
    },
    '.token.bold': {
        fontWeight: 'bold',
    },
    '.token.italic': {
        fontStyle: 'italic',
    },
}

// LAYOUT ---------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------
export const zIndices = {
    auto: 'auto',
    bg: '0',
    content: '100',
    mg: '200',
    header: '300',
    burgerMenu: '400',
    burger: '500',
    search: '600',
    top: '10000',
}

export const animation = {
    duration: `0.2s`,
    ms: 150,
    easing: `cubic-bezier(0.175, 0.885, 0.320, 1.275)`,
    quick: {
        duration: `0.075s`,
        ms: 75,
        easing: `cubic-bezier(0.175, 0.885, 0.320, 1.275)`,
    },
    slower: {
        duration: '0.65s',
        ms: 650,
        easing: `cubic-bezier(0.175, 0.885, 0.320, 1.275)`,
    },
    page: {
        duration: '0.5s',
        ms: 500,
        easing: `cubic-bezier(0.175, 0.885, 0.320, 1.275)`,
    },
    transition: 'opacity 0.3s ease-in, color 0.3s ease-in',
    all: 'all 0.3s ease-in-out',
}

export const transition = {
    default: `opacity ${animation.slower.duration} ease-in`,
}

export const radii = {
    none: `0`,
    sm: `0.125rem`,
    default: `0.25rem`,
    lg: `0.5rem`,
    full: `9999px`,
}

export const initialColorMode = `warm`

export const colors = {
    transparent: `transparent`,
    black: `#050505`,
    white: `#e6e6e6`,
    text: `#c9b6b6`,
    gray: `#322929`,
    backgroundLighter: `#40262f`,
    background: `#301d23`,
    backgroundDarker: `#29191e`,
    primary: `#d99f8c`,
    secondary: `#d67a5c`,
    accent: `#f3dc95`,
    highlight: `#8f4b3d`,
    muted: `#7a5c52`,
    button: transparentize(0.6, `#d67a5c`),
    buttonHover: darken(0.3, `#d67a5c`),
    buttonColor: `#301d23`,
    buttonColorHover: `#f3dc95`,

    modes: {
        dark: {
            text: `#909090`,
            backgroundLighter: `#2e2e2e`,
            background: `#1e1e1e`,
            backgroundDarker: `#1a1a1a`,
            darken: `#101010`,
            primary: `#4ca5e5`,
            secondary: `#7bb5d2`,
            accent: `#bf406b`,
            highlight: `#494e50`,
            muted: `#505050`,
            gray: `#252e37`,
            button: transparentize(0.65, `#1f7dad`),
            buttonHover: darken(0.2, `#1f7dad`),
            buttonColor: `#0a0a0a`,
            buttonColorHover: `#1f7dad`,
        },
        light: {
            text: `#666`,
            backgroundLighter: `#fefefe`,
            background: `#f9f9f9`,
            backgroundDarker: `#e9e9e9`,
            primary: `#777`,
            secondary: `#999`,
            accent: `#333`,
            highlight: `#bbb`,
            muted: `#888`,
            gray: `#aaa`,
            button: transparentize(0.6, `#888`),
            buttonHover: darken(0.3, `#888`),
            buttonColor: `#888`,
            buttonColorHover: `#f9f9f9`,
        },
        deep: {
            text: `#d9ddf2`,
            backgroundLighter: `#2e334d`,
            background: `#262a40`,
            backgroundDarker: `#222639`,
            primary: `#9480ff`,
            secondary: `#c6f`,
            accent: `#e9f`,
            highlight: `#5f527a`,
            muted: `#52587ae6`,
            gray: `#252e37`,
            button: transparentize(0.6, `#c6f`),
            buttonHover: darken(0.3, `#c6f`),
            buttonColor: `#262a40`,
            buttonColorHover: `#e9f`,
        },
    },
}

const link = {
    color: 'primary',
    position: 'relative',
    textDecoration: 'none',
    '&:before': {
        content: 'none',
    },
    '&:after': {
        content: '""',
        position: 'absolute',
        transform: 'translateY(0.2rem)',
        height: '4px',
        borderRadius: '2px',
        bottom: 0,
        width: '95%',
        left: '5%',
        opacity: 0.7,
        backgroundColor: 'secondary',
        transition: `${animation.duration} ${animation.easing} all ${animation.duration}`,
    },
    '&:hover': {
        color: 'accent',
        '&:after': {
            transform: 'translateY(0.3rem)',
            width: '102%',
            left: '0%',
            opacity: 1,
            backgroundColor: 'primary',
        },
    },
    transition: `color ${animation.duration} ease, opacity ${animation.duration}`,

    '&:focus': {
        outline: `none`,
        boxShadow: `none`,
    },
}

const linkSimple = {
    color: 'primary',
    position: 'relative',
    textDecoration: 'none',
    '&:before': {
        content: 'none',
    },
    '&:after': {
        content: '""',
    },
    '&:hover': {
        color: 'accent',
        '&:after': {},
    },
    transition: `color ${animation.duration} ease, opacity ${animation.duration}`,

    '&:focus': {
        outline: `none`,
        boxShadow: `none`,
    },
}

const linkHeavy = {
    color: 'primary',
    position: 'relative',
    textDecoration: 'none',
    '&:before': {
        content: 'none',
    },
    '&:after': {
        content: '""',
        position: 'absolute',
        transform: 'translateY(0.2rem)',
        height: '8px',
        borderRadius: '4px',
        bottom: 0,
        width: '95%',
        left: '5%',
        opacity: 0.7,
        backgroundColor: 'secondary',
        transition: `${animation.duration} ${animation.easing} all ${animation.duration}`,
    },
    '&:hover': {
        color: 'accent',
        '&:after': {
            transform: 'translateY(0.3rem)',
            width: '102%',
            left: '0%',
            opacity: 1,
            backgroundColor: 'primary',
        },
    },
    transition: `color ${animation.duration} ease, opacity ${animation.duration}`,

    '&:focus': {
        outline: `none`,
        boxShadow: `none`,
    },
}

const commonInputStyles = {
    py: 2,
    px: 3,
    fontSize: `100%`,
    borderRadius: `default`,
    appearance: `none`,
    lineHeight: `tight`,
}

export const shadows = {
    default: '0 1px 3px 0 rgba(0, 0, 0, 0.3), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
    md: '0 4px 6px -1px rgba(0, 0, 0, 0.3), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
    lg: '0 10px 15px -3px rgba(0, 0, 0, 0.3), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
    xl: '0 20px 25px -5px rgba(0, 0, 0, 0.3), 0 10px 10px -5px rgba(0, 0, 0, 0.04)',
    '2xl': '0 25px 50px -12px rgba(0, 0, 0, 0.4)',
    inner: 'inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)',
    outline: '0 0 0 3px rgba(66, 153, 225, 0.5)',
    none: 'none',
}

const commonButtonStyles = {
    py: 1,
    px: 2,
    cursor: `pointer`,
    fontSize: `60%`,
    lineHeight: `inherit`,
    fontFamily: `sans`,
    textTransform: 'uppercase',
    fontWeight: 'extrabold',
    letterSpacing: 'wider',
}

export const buttons = {
    simple: {
        ...commonButtonStyles,
        backgroundColor: `primary`,
        border: `none`,
        color: `text`,
        borderRadius: `default`,
        '&:hover': {
            backgroundColor: `primaryHover`,
        },
    },
    pill: {
        '&:focus': {
            outline: `none`,
            boxShadow: `default`,
        },
        ...commonButtonStyles,
        backgroundColor: `primary`,
        border: `none`,
        color: `background`,
        borderRadius: `full`,
        '&:hover': {
            backgroundColor: `primaryHover`,
        },
    },
    site: {
        '&:focus': {
            outline: `none`,
            boxShadow: `default`,
        },
        height: ['2rem', '2.5rem', '3rem'],
        px: ['1rem', '1.5rem', '2rem'],
        backgroundColor: `transparent`,

        borderWidth: `3px`,
        borderStyle: `solid`,
        borderColor: `button`,
        borderRadius: `2rem`,
        color: `button`,

        position: `relative`,
        fontSize: [1, 1, 2, 2],
        display: `flex`,
        flexFlow: `column wrap`,
        alignContent: `center`,
        justifyContent: `center`,
        alignItems: `center`,
        '&:hover': {
            backgroundColor: `buttonHover`,
            borderWidth: `3px`,
            borderStyle: `solid`,
            borderColor: `buttonHover`,
            color: `buttonColorHover`,
        },
        '&:after': { backgroundColor: 'transparent !important' },
        fontWeight: `extrabold`,

        cursor: `pointer`,
        '&:active': {
            boxShadow: `none`,
            border: `none`,
            top: `1px`,
        },
    },
}

export const inputs = {
    shadow: {
        ...commonInputStyles,
        border: `none`,
        color: `gray.7`,
        boxShadow: `default`,
        '&:focus': {
            outline: `none`,
            boxShadow: `outline`,
        },
    },
    inline: {
        ...commonInputStyles,
        backgroundColor: `gray.2`,
        borderWidth: `2px`,
        borderStyle: `solid`,
        borderColor: `gray.2`,
        color: `gray.7`,
        '&:focus': {
            outline: `none`,
            borderColor: `primary`,
            backgroundColor: `white`,
        },
    },
    underline: {
        ...commonInputStyles,
        backgroundColor: `transparent`,
        border: `none`,
        borderBottomWidth: `2px`,
        borderBottomStyle: `solid`,
        borderBottomColor: `primary`,
        borderRadius: `0px`,
        color: `gray.7`,
        '&:focus': {
            outline: `none`,
            borderColor: `primary`,
            backgroundColor: `white`,
        },
    },
}

const body = {
    fontWeight: 'light',
    fontSize: [3, 3, 3, 4, 5],
    fontSize: 'calc(0.75rem + 0.75vw)',
    lineHeight: `relaxed`,
    lineHeight: 'calc(1.2rem + 1.5vw)',
    my: 2,
}

const heading = {
    fontFamily: `heading`,
    lineHeight: `heading`,
    letterSpacing: `tight`,
    m: 0,
    my: 3,
    p: 0,
    fontWeight: 'bold',
}

export const styles = {
    root: {
        '*': {
            ...selection,
        },
        color: `text`,
        fontFamily: `default`,
        fontSize: [2, 2, 3, 4, 5],
        lineHeight: `snug`,
        h1: {
            color: `primary`,
            fontFamily: 'heading',
        },
        h2: {
            color: `secondary`,
            fontFamily: 'heading',
        },
        h3: {
            color: `highlight`,
            fontFamily: 'heading',
        },
        h4: {
            color: `primary`,
            fontFamily: 'heading',
        },
        h5: {
            color: 'muted',
        },
        p: {
            ...body,
        },
        blockquote: {
            mx: 3,
            my: 5,
            py: 2,
            px: 4,
            borderLeftColor: 'muted',
            borderLeftStyle: 'solid',
            borderLeftWidth: '4px',
            fontStyle: 'italic',
            backgroundColor: 'backgroundDarker',
        },
        ...prism,
        a: {
            ...link,
        },
    },
    h1: {
        ...heading,
        color: `primary`,
        fontSize: [6, 6, 7, 8, 9],
        fontSize: 'calc(1rem + 3.5vmin - 0.5vmin)',
        lineHeight: 'calc(1rem + 3.5vmin - 0.5vmin)',
    },
    h2: {
        ...heading,
        color: `secondary`,
        fontSize: [5, 5, 6, 7, 8],
        fontSize: 'calc(0.75rem + 3vmin - 0.5vmin)',
        lineHeight: 'calc(0.75rem + 3vmin - 0.5vmin)',
    },
    h3: {
        ...heading,
        color: `highlight`,
        fontWeight: 'extrabold',
        fontSize: [4, 4, 5, 6, 7],
        fontSize: 'calc(0.75rem + 2vmin - 0.5vmin)',
        lineHeight: 'calc(0.75rem + 2vmin - 0.5vmin)',
    },
    h4: {
        ...heading,
        color: `primary`,
        fontSize: [3, 3, 4, 5, 6],
    },
    h5: {
        ...heading,
        fontSize: [2, 2, 2, 3, 4],
    },
    h6: {
        ...heading,
        color: `primary`,
        fontSize: [1, 1, 2, 3, 4],
    },
    hr: {
        border: 0,
        height: '2px',
        backgroundColor: 'muted',
        opacity: 0.1,
        my: 5,
    },
    a: {
        textDecoration: `none`,
        cursor: `pointer`,
        color: `primary`,
        '&:hover': {
            color: `accent`,
        },
        clean: {
            color: 'muted',
            transition: animation.transition,
        },
        '&:after': { backgroundColor: 'transparent !important' },
    },
    p: {
        ...body,
    },
    ol: {
        listStyleType: `none`,
        pl: 0,
    },

    Link: {
        ...link,
    },
    LinkHeavy: {
        ...linkHeavy,
    },
    logo: {
        fontFamily: 'heading',
        textAlign: 'right',
        fontSize: 'calc(0.75rem + 3vmin - 0.5vmin)',
        lineHeight: 'calc(0.75rem + 3vmin - 0.5vmin)',
        letterSpacing: '-0.025rem',
        m: 0,
        p: 0,
        textShadow: 'lg',
        fontWeight: 'bold',
        '&:after': { backgroundColor: 'transparent !important' },
    },
    menu: {
        color: t => `${t.colors.muted} !important`,
        textDecoration: 'none',
        fontWeight: 'normal',
        textAlign: ['center', 'center', 'right'],
        fontSize: [3, 3, 4, 4, 5],
        '&:hover, &:focus': {
            color: t => `${t.colors.primary} !important`,
        },
        '&:before, &:after': {
            content: 'none',
            backgroundColor: 'transparent !important',
        },
        '&.active, &[aria-current="page"]': {
            color: `primary`,
        },
    },
    title: {
        textIndent: ['2vw', '2vw', 0],
        fontSize: 'calc(1.25rem + 4vmin - 0.5vmin)',
        lineHeight: 'calc(0.75rem + 4vmin + 0.25rem - 0.5vmin)',
        position: 'relative',
        zIndex: 'header',
        textAlign: ['left', 'left', 'right'],
        display: 'flex',
        flexFlow: ['row nowrap', 'row nowrap', 'column nowrap'],
        justifyContent: 'flex-end',
        color: 'secondary',
        textShadow: 'xl',
    },
    spacer: {
        h1: {
            position: 'relative',
            mt: '31.5vmin',
        },
        h2: {
            position: 'relative',
            mt: '32.25vmin',
        },
        work: {
            position: 'relative',
            mt: ['28vmin', '30vmin', '10vmin'],
        },
        list: {
            position: 'relative',
            mt: ['28vmin', '30vmin', '7vmin'],
        },
        detail: {
            position: 'relative',
            mt: ['30vmin', '30vmin', '10vmin'],
        },
    },
    roundedBox: {
        borderRadius: '1vmax',
        overflow: 'hidden',
        boxShadow: 'xl',
    },
}

export const layout = {
    header: {
        position: 'fixed',
        width: ['100vw', '100vw', '22vw'],
        minWidth: ['400px', 'auto'],
        height: ['25vmin', '25vmin', '100vh'],
        top: 0,
        left: 0,
        zIndex: 10000,
        flexFlow: ['row wrap', 'row wrap', 'column wrap'],
        alignItems: 'stretch',
    },
    logo: {
        position: 'relative',
        width: ['30vw', '30vw', '100%'],
        height: '25vmin',
        flex: '0 0 auto',
    },
    title: {
        mt: '6vmin',
        position: 'relative',
        flex: '0 0 auto',
        display: 'flex',
        flexFlow: ['row wrap', 'row wrap', 'column nowrap'],
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
        zIndex: 'header',
    },
    menu: {
        position: 'relative',
        width: '100%',
        display: 'block',
    },
    burger: {
        position: 'fixed',
        height: '25vmin',
        width: '10vw',
        top: 0,
        right: 0,
    },
    main: {
        position: 'relative',
        p: 0,
        m: 0,
        width: ['90vw', '90vw', '70vw'],
        left: ['auto', 'auto', '22vw'],
        mx: ['auto', 'auto', 0],
        minWidth: '400px',
    },
}

export default {
    animation,
    breakpoints,
    space,
    radii,
    initialColorMode,
    colors,
    fonts,
    fontSizes,
    fontWeights,
    letterSpacings,
    lineHeights,
    styles,
    buttons,
    shadows,
    zIndices,
    layout,
}
