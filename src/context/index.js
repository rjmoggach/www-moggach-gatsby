import React from 'react'

const mockMenu = {
    work: {
        title: 'Work',
        open: false,
        type: 'work',
        items: [
            { to: '/work', label: 'Work Home' },
            { to: '/work/reel', label: 'Reel' },
            { to: '/work/categories/advertising', label: 'Advertising' },
            { to: '/work/categories/feature-films', label: 'Feature Films' },
            { to: '/work/categories/television', label: 'Television' },
        ],
    },
    empty: {
        title: '',
        open: false,
        items: [],
    },
}

const defaultContextValue = {
    data: {
        menuType: '',
        menuOpen: false,
        burgerOpen: false,
        searchOpen: false,
        menu: {
            title: '',
            open: false,
            items: [],
        },
        menuHeight: 0,
    },
    set: () => {},
}

// const { Provider, Consumer } = React.createContext(defaultContextValue)
export const GlobalContext = React.createContext(defaultContextValue)
const { Consumer, Provider } = GlobalContext
class ContextProviderComponent extends React.Component {
    constructor() {
        super()
        this.setData = this.setData.bind(this)
        this.state = {
            ...defaultContextValue,
            set: this.setData,
        }
    }

    setData(newData) {
        this.setState(state => ({
            data: {
                ...state.data,
                ...newData,
            },
        }))
    }

    componentDidMount() {
        const s = this.props.path.split('/')[1]
        if (this.state.data.menuType.toLowerCase() !== s) {
            const menuType = s.charAt(0).toUpperCase() + s.substring(1)
            this.setState(this.setState({ menuType: menuType }))
        }
    }

    render() {
        return <Provider value={this.state}>{this.props.children}</Provider>
    }
}

export { Consumer as default, ContextProviderComponent }
