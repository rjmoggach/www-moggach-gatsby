const _ = require('lodash')
const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)
const { paginate } = require('gatsby-awesome-pagination')

const { createPostDetailPages } = require('./src/gatsby/createPostDetailPages')
const { createCollectionPages } = require('./src/gatsby/createCollectionPages')
const gatsbyNodeGraphQL = require('./src/gatsby/gatsbyNodeGraphQL')

// Remove trailing slashes unless it's only "/", then leave it as it is
const replaceTrailing = _path => (_path === `/` ? _path : _path.replace(/\/$/, ``))

// Remove slashes at the beginning and end
const replaceBoth = _path => _path.replace(/^\/|\/$/g, '')

function looseJsonParse(obj) {
    return Function('"use strict";return (' + obj + ')')()
}

exports.onCreateNode = ({ node, getNode, actions }) => {
    const { createNodeField } = actions
    // check that this is markdown
    switch (node.internal.type) {
        case 'MarkdownRemark': {
            const { permalink, layout, primaryTag } = node.frontmatter
            const parent = getNode(node.parent)
            if (parent.internal.type === 'File') {
                // SOURCE NAME - blog, work, play, pages
                const sourceName = parent.sourceInstanceName
                createNodeField({ name: `sourceName`, node, value: sourceName })
                // SLUG - URL PATH
                const slug = createFilePath({ node, getNode, basePath: `pages` })
                if (sourceName === 'work') {
                    // console.log('WORK SLUG', slug.substring(12))
                    createNodeField({
                        name: `slug`,
                        node,
                        value: `/${sourceName}${slug.substring(0, 5)}/${slug.substring(12)}`,
                    })
                } else {
                    createNodeField({ name: `slug`, node, value: `/${sourceName}${slug}` })
                }

                // PARENT DIRECTORY NAME
                const { relativePath } = parent
                const parentDirname = path.basename(path.dirname(relativePath))
                createNodeField({ name: `parentDirname`, node, value: parentDirname })
                // YEAR FOR POST FROM FOLDER
                const parentYear = path.dirname(relativePath).split('/')[0]
                if (/^[0-9]{4,}/.test(parentYear)) {
                    createNodeField({ name: `year`, node, value: parentYear })
                }
            }
        }
    }
}

exports.onCreatePage = ({ page, actions }) => {
    const { createPage } = actions

    if (page.path.match('/test/')) {
        page.context.layout = 'test'
        createPage(page)
    }
    if (page.path.match('/test-grid/')) {
        page.context.layout = 'test'
        createPage(page)
    }
}
// graphql function doesn't throw an error
// so we have to check to check for the result.errors to throw manually
const wrapper = promise =>
    promise.then(result => {
        if (result.errors) {
            throw result.errors
        }
        return result
    })

exports.createPages = async ({ graphql, actions }) => {
    const { createPage } = actions
    const templates = {
        blog: {
            detail: path.resolve('./src/templates/BlogDetailTemplate.js'),
            list: path.resolve('./src/templates/BlogListTemplate.js'),
        },
        play: {
            detail: path.resolve('./src/templates/PlayDetailTemplate.js'),
            list: path.resolve('./src/templates/PlayListTemplate.js'),
        },
        work: {
            detail: path.resolve('./src/templates/WorkDetailTemplate.js'),
            list: path.resolve('./src/templates/WorkListTemplate.js'),
        },
        collection: {
            detail: path.resolve('./src/templates/CollectionDetailTemplate.js'),
            list: path.resolve('./src/templates/CollectionListTemplate.js'),
        },
    }
    const result = await wrapper(
        graphql(
            `{
                ${gatsbyNodeGraphQL}
            }
            `
        )
    )

    // createPostDetailPages(edges, createPage, template, prefix, extraContext)
    createPostDetailPages(result.data.blog.edges, createPage, templates.blog.detail)
    createPostDetailPages(result.data.work.edges, createPage, templates.work.detail)
    // createPostDetailPages(result.data.playIndexPages.edges, createPage, templates.play.detail)
    paginate({
        createPage,
        items: result.data.blog.edges,
        itemsPerPage: 4,
        pathPrefix: '/blog/all',
        component: templates.blog.list,
    })
    paginate({
        createPage,
        items: result.data.work.edges,
        itemsPerPage: 12,
        pathPrefix: '/work/featured',
        component: templates.work.list,
        context: { featured: [true] },
    })
    paginate({
        createPage,
        items: result.data.work.edges,
        itemsPerPage: 12,
        pathPrefix: '/work/all',
        component: templates.work.list,
        context: { featured: [true, false] },
    })
    createCollectionPages(
        result.data.all.edges,
        createPage,
        'categories',
        templates.collection.list,
        templates.collection.detail
    )
    createCollectionPages(
        result.data.all.edges,
        createPage,
        'tags',
        templates.collection.list,
        templates.collection.detail
    )
}
