#!/usr/bin/env bash


for file in *.md; do
    filename=$(basename -- "$file")
    echo "FILENAME: ${filename}"
    extension="${filename##*.}"
    echo "EXTENSION: ${extension}"
    filebase="${filename%.*}"
    echo "FILEBODY: ${filebase}"
    date="${filebase:0:10}"
    echo "DATE: ${date}"
    slug="${filebase:11}"
    echo "SLUG: ${slug}"
    year="${filebase:0:4}"
    echo "YEAR: ${year}"
    month="${filebase:5:2}"
    echo "MONTH: ${month}"
    day="${filebase:8:2}"
    echo "DAY: ${day}"
    projectdir="${year}/${filebase}"
    mkdir -p $projectdir
    cp $file ${projectdir}/index.md
    cp $file ${projectdir}/images.md
    cp $file ${projectdir}/videos.md
    cp $file ${projectdir}/credits.md
    cp $file ${projectdir}/
done
