---
title: American Gods | Season 2 "Hall Of The Gods"
subtitle: Season 2 "Hall Of The Gods"
slug: american-gods-hall-of-the-gods
date: 2018-12-13
role:
headline: ''
summary:
excerpt:
published: true
featured: true
categories:
    - Television
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: american-gods-hall-of-the-gods.01.jpg
          - caption: ''
            filename: american-gods-hall-of-the-gods.02.jpg
          - caption: ''
            filename: american-gods-hall-of-the-gods.03.jpg
          - caption: ''
            filename: american-gods-hall-of-the-gods.04.jpg
          - caption: ''
            filename: american-gods-hall-of-the-gods.05.jpg
          - caption: ''
            filename: american-gods-hall-of-the-gods.06.jpg
          - caption: ''
            filename: american-gods-hall-of-the-gods.07.jpg
          - caption: ''
            filename: american-gods-hall-of-the-gods.08.jpg
          - caption: ''
            filename: american-gods-hall-of-the-gods.09.jpg
          - caption: ''
            filename: american-gods-hall-of-the-gods.10.jpg
      path: work/2018/american-gods-hall-of-the-gods/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2018/american-gods-hall-of-the-gods/videos/american-gods-hall-of-the-gods/american-gods-hall-of-the-gods.jpg
      path: work/2018/american-gods-hall-of-the-gods/videos/american-gods-hall-of-the-gods
      slug: american-gods-hall-of-the-gods
      provider: html5
      sources:
          - filename: american-gods-hall-of-the-gods-1280x720.mp4
            format: mp4
            height: '720'
            size:
            width: '1280'
          - filename: american-gods-hall-of-the-gods-960x540.mp4
            format: mp4
            height: '540'
            size:
            width: '960'
          - filename: american-gods-hall-of-the-gods-480x270.ogv
            format: ogv
            height: '270'
            size:
            width: '480'
          - filename: american-gods-hall-of-the-gods-640x360.mp4
            format: mp4
            height: '360'
            size:
            width: '640'
          - filename: american-gods-hall-of-the-gods-640x360.webm
            format: webm
            height: '360'
            size:
            width: '640'
          - filename: american-gods-hall-of-the-gods-480x270.mp4
            format: mp4
            height: '270'
            size:
            width: '480'
      title: American Gods Season 2 "Hall of The Gods"
credits:
    - companyName: Tendril
      companySlug: tendril
      roleSlug: design-animation
      roleTitle: Design & Animation
      visible: true
      role: Design & Animation
    - companyName: Tendril
      companySlug: tendril
      personName: Kate Bate
      personSlug: kate-bate
      roleSlug: design-ep
      roleTitle: Executive Producer
      visible: true
      role: '--Design & Animation/Executive Producer'
    - companyName: Tendril
      companySlug: tendril
      personName: Chris Bahry
      personSlug: chris-bahry
      roleSlug: creative-director
      roleTitle: Creative Director
      visible: true
      role: '--Design & Animation/Creative Director'
    - companyName: Tendril
      companySlug: tendril
      personName: ''
      personSlug: ''
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: true
      role: VFX
    - role: '--VFX/VFX Supervisor'
      roleTitle: VFX Supervisor
      roleSlug: vfx-supervisor
      companyName: Tendril
      companySlug: tendril
      personName: Robert Moggach
      personSlug: robert-moggach
      visible: true
---

Tendril is a wonderful design and animation studio based in Toronto. They're known for their ability to approach a creative vision with thoughtful and playful originality.

For the first episode of the second season, director Christopher Byrne approached Tendril's Creative Director Chris Bahry to dig in to the history and the mythology of the gods portrayed and give them a visual design with real "gravitas". Months of research and design resulted in some compelling concept and reference from which I was asked to develop a VFX process alongside Chris to deliver the 75 shots in the sequence in an accelerated schedule.

The result is what you see here. It was challenging with so many FX requirements and certainly corners were cut as we raced to the finish. The result however was compelling as usual for Tendril and full of happy accidents we would not have planned on in a longer production. The culture of Tendril and the crew involved came together to make something truly special.
