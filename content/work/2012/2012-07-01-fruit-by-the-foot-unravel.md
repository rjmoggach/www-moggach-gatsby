---
title: Fruit By The Foot Unravel
subtitle:
slug: fruit-by-the-foot-unravel
date: 2012-07-01
role:
headline: ''
summary: The Odd bunch unwrap themselves as Fruit by The Foot.
excerpt: The Odd bunch unwrap themselves as Fruit by The Foot.
published: false
featured: false
categories:
- Advertising
tags:
- CG
- compositing
- VFX
- comedy
albums:
-   cover:
    images:
    -   caption: ''
        filename: fruit-by-the-foot-unravel.04.jpg
    -   caption: ''
        filename: fruit-by-the-foot-unravel.05.jpg
    -   caption: ''
        filename: fruit-by-the-foot-unravel.06.jpg
    -   caption: ''
        filename: fruit-by-the-foot-unravel.07.jpg
    -   caption: ''
        filename: fruit-by-the-foot-unravel.08.jpg
    -   caption: ''
        filename: fruit-by-the-foot-unravel.09.jpg
    -   caption: ''
        filename: fruit-by-the-foot-unravel.10.jpg
    -   caption: ''
        filename: fruit-by-the-foot-unravel.11.jpg
    -   caption: ''
        filename: fruit-by-the-foot-unravel.12.jpg
    path: work/2012/fruit-by-the-foot-unravel/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2012/fruit-by-the-foot-unravel/videos/fruit-by-the-foot-unravel-alt.jpg
    path: work/2012/fruit-by-the-foot-unravel/videos/fruit-by-the-foot-unravel-alt
    slug: fruit-by-the-foot-unravel-alt
    sources:
    -   filename: fruit-by-the-foot-unravel-alt-1280x720.mp4
        format: mp4
        height: '720'
        size: 9797375
        width: '1280'
    -   filename: fruit-by-the-foot-unravel-alt-960x540.mp4
        format: mp4
        height: '540'
        size: 2031071
        width: '960'
    -   filename: fruit-by-the-foot-unravel-alt-640x360.ogv
        format: ogv
        height: '360'
        size: 1765446
        width: '640'
    -   filename: fruit-by-the-foot-unravel-alt-640x360.webm
        format: webm
        height: '360'
        size: 1695918
        width: '640'
    -   filename: fruit-by-the-foot-unravel-alt-640x360.mp4
        format: mp4
        height: '360'
        size: 1267525
        width: '640'
    -   filename: fruit-by-the-foot-unravel-alt-480x270.mp4
        format: mp4
        height: '270'
        size: 852175
        width: '480'
    title: Fruit By The Foot Unravel Alt
-   caption: ''
    cover: work/2012/fruit-by-the-foot-unravel/videos/fruit-by-the-foot-unravel-baby.jpg
    path: work/2012/fruit-by-the-foot-unravel/videos/fruit-by-the-foot-unravel-baby
    slug: fruit-by-the-foot-unravel-baby
    sources:
    -   filename: fruit-by-the-foot-unravel-baby-1280x720.mp4
        format: mp4
        height: '720'
        size: 9887691
        width: '1280'
    -   filename: fruit-by-the-foot-unravel-baby-640x360.ogv
        format: ogv
        height: '360'
        size: 1785387
        width: '640'
    -   filename: fruit-by-the-foot-unravel-baby-960x540.mp4
        format: mp4
        height: '540'
        size: 1716878
        width: '960'
    -   filename: fruit-by-the-foot-unravel-baby-640x360.webm
        format: webm
        height: '360'
        size: 1635320
        width: '640'
    -   filename: fruit-by-the-foot-unravel-baby-640x360.mp4
        format: mp4
        height: '360'
        size: 1084662
        width: '640'
    -   filename: fruit-by-the-foot-unravel-baby-480x270.mp4
        format: mp4
        height: '270'
        size: 749959
        width: '480'
    title: Fruit By The Foot Unravel Baby
-   caption: ''
    cover: work/2012/fruit-by-the-foot-unravel/videos/fruit-by-the-foot-unravel-teen.jpg
    path: work/2012/fruit-by-the-foot-unravel/videos/fruit-by-the-foot-unravel-teen
    slug: fruit-by-the-foot-unravel-teen
    sources:
    -   filename: fruit-by-the-foot-unravel-teen-1280x720.mp4
        format: mp4
        height: '720'
        size: 9310396
        width: '1280'
    -   filename: fruit-by-the-foot-unravel-teen-960x540.mp4
        format: mp4
        height: '540'
        size: 2127891
        width: '960'
    -   filename: fruit-by-the-foot-unravel-teen-640x360.ogv
        format: ogv
        height: '360'
        size: 1839933
        width: '640'
    -   filename: fruit-by-the-foot-unravel-teen-640x360.webm
        format: webm
        height: '360'
        size: 1767785
        width: '640'
    -   filename: fruit-by-the-foot-unravel-teen-640x360.mp4
        format: mp4
        height: '360'
        size: 1327352
        width: '640'
    -   filename: fruit-by-the-foot-unravel-teen-480x270.mp4
        format: mp4
        height: '270'
        size: 890878
        width: '480'
    title: Fruit By The Foot Unravel Teen
credits:
-   companyName: General Mills
    companySlug: general-mills
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Cossette
    companySlug: cossette
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: false
-   personName: Ed Lea
    personSlug: ed-lea
    roleSlug: agency-cd
    roleTitle: Creative Director
    visible: false
-   personName: Jennifer Wilson
    personSlug: jennifer-wilson
    roleSlug: agency-cd
    roleTitle: Creative Director
    visible: false
-   personName: Tony De Sousa
    personSlug: tony-de-sousa
    roleSlug: agency-producer
    roleTitle: Agency Producer
    visible: false
-   companyName: Soft Citizen
    companySlug: soft-citizen
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: 'The Perlorian Bros. '
    personSlug: perlorian-bros
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Link York
    personSlug: link-york
    roleSlug: prod-ep
    roleTitle: Executive Producer
    visible: false
-   personName: Merrie Wasson
    personSlug: merrie-wasson
    roleSlug: line-producer
    roleTitle: Line Producer
    visible: false
-   companyName: School Editing
    companySlug: school-editing
    roleSlug: edit
    roleTitle: Editorial
    visible: false
-   companyName: Soft Citizen
    companySlug: soft-citizen
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: 'The Perlorian Bros. '
    personSlug: perlorian-bros
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Link York
    personSlug: link-york
    roleSlug: prod-ep
    roleTitle: Executive Producer
    visible: false
-   personName: Merrie Wasson
    personSlug: merrie-wasson
    roleSlug: line-producer
    roleTitle: Line Producer
    visible: false
-   companyName: School Editing
    companySlug: school-editing
    roleSlug: edit
    roleTitle: Editorial
    visible: false
-   personName: Aaron Dark
    personSlug: aaron-dark
    roleSlug: editor
    roleTitle: Editor
    visible: false
-   personName: Sarah Brooks
    personSlug: sarah-brooks
    roleSlug: edit-ep
    roleTitle: Executive Producer
    visible: false
-   companyName: Dashing Collective
    companySlug: dshng
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Mary Anne Ledesma
    personSlug: mary-anne-ledesma
    roleSlug: vfx-producer
    roleTitle: VFX Producer
    visible: false
-   personName: Susan Armstrong
    personSlug: susan-armstrong
    roleSlug: flame
    roleTitle: Flame Artist
    visible: false
---
Dashing and The Perlorian Bros. (Soft Citizen) team up again to give you a
little more fun to unravel with Cossette's new Fruit by the Foot spots.