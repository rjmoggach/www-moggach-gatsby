---
title: Hudson Bay Company Badges
subtitle:
slug: hudson-bay-company-badges
date: 2012-04-02
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories:
- Advertising
tags:
- VFX
- compositing
- tracking
- photoreal
albums:
-   cover:
    images:
    -   caption: ''
        filename: hbc-badges.01.jpg
    -   caption: ''
        filename: hbc-badges.02.jpg
    -   caption: ''
        filename: hbc-badges.03.jpg
    -   caption: ''
        filename: hbc-badges.04.jpg
    path: work/2012/hudson-bay-company-badges/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2012/hudson-bay-company-badges/videos/hudson-bay-company-badges.jpg
    path: work/2012/hudson-bay-company-badges/videos/hudson-bay-company-badges
    slug: hudson-bay-company-badges
    sources:
    -   filename: hudson-bay-company-badges-1280x720.mp4
        format: mp4
        height: '720'
        size: 28957834
        width: '1280'
    -   filename: hudson-bay-company-badges-960x540.mp4
        format: mp4
        height: '540'
        size: 5872674
        width: '960'
    -   filename: hudson-bay-company-badges-640x360.ogv
        format: ogv
        height: '360'
        size: 4266171
        width: '640'
    -   filename: hudson-bay-company-badges-640x360.mp4
        format: mp4
        height: '360'
        size: 4101874
        width: '640'
    -   filename: hudson-bay-company-badges-640x360.webm
        format: webm
        height: '360'
        size: 3442531
        width: '640'
    -   filename: hudson-bay-company-badges-480x270.mp4
        format: mp4
        height: '270'
        size: 2944187
        width: '480'
    title: Hudson Bay Company Badges
credits:
-   companyName: Hudson's Bay Company
    companySlug: hudsons-bay-company
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: john st.
    companySlug: john-st
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: false
-   personName: Stephen Jurisic
    personSlug: stephen-jurisic
    roleSlug: agency-cd
    roleTitle: Creative Director
    visible: false
-   personName: Kyle Lamb
    personSlug: kyle-lamb
    roleSlug: agency-art-director
    roleTitle: Art Director
    visible: false
-   personName: Kurt Mills
    personSlug: kurt-mills
    roleSlug: agency-copywriter
    roleTitle: Copywriter
    visible: false
-   personName: Michelle Orlando
    personSlug: michelle-orlando
    roleSlug: agency-producer
    roleTitle: Agency Producer
    visible: false
-   companyName: Panic & Bob
    companySlug: panic-bob
    roleSlug: edit
    roleTitle: Editorial
    visible: false
-   personName: Michelle Czukar
    personSlug: michelle-czukar
    roleSlug: editor
    roleTitle: Editor
    visible: false
-   personName: Sam McLaren
    personSlug: sam-mclaren
    roleSlug: edit-ep
    roleTitle: Executive Producer
    visible: false
-   companyName: Dashing Collective
    companySlug: dshng
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Mary Anne Ledesma
    personSlug: mary-anne-ledesma
    roleSlug: vfx-producer
    roleTitle: VFX Producer
    visible: false
-   companyName: Notch
    companySlug: notch
    roleSlug: telecine
    roleTitle: DI / Color
    visible: false
-   personName: Bill Ferwerda
    personSlug: bill-ferwerda
    roleSlug: colorist
    roleTitle: Colorist
    visible: false
-   companyName: Imprint Music
    companySlug: imprint-music
    roleSlug: music
    roleTitle: Music
    visible: false
-   personName: Fraser MacDougall
    personSlug: fraser-macdougall
    roleSlug: writer-producer
    roleTitle: Writer / Producer
    visible: false
-   personName: Tim White
    personSlug: tim-white
    roleSlug: writer-producer
    roleTitle: Writer / Producer
    visible: false
-   personName: Corinne Murray
    personSlug: corinne-murray
    roleSlug: studio-manager
    roleTitle: Studio Manager
    visible: false
---
