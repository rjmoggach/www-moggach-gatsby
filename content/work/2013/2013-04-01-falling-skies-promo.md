---
title: Falling Skies Promo
subtitle:
slug: falling-skies-promo
date: 2013-04-01
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: falling-skies-bug.01.jpg
    -   caption: ''
        filename: falling-skies-bug.02.jpg
    -   caption: ''
        filename: falling-skies-bug.03.jpg
    -   caption: ''
        filename: falling-skies-bug.04.jpg
    -   caption: ''
        filename: falling-skies-bug.05.jpg
    -   caption: ''
        filename: falling-skies-bug.06.jpg
    -   caption: ''
        filename: falling-skies-bug.07.jpg
    -   caption: ''
        filename: falling-skies-bug.08.jpg
    -   caption: ''
        filename: falling-skies-bug.09.jpg
    path: work/2013/falling-skies-promo/images/falling-skies-bug-stills
    slug: falling-skies-bug-stills
    title: Falling Skies Bug Stills
-   cover:
    images:
    -   caption: ''
        filename: falling-skies-contact-lens.01.jpg
    -   caption: ''
        filename: falling-skies-contact-lens.02.jpg
    -   caption: ''
        filename: falling-skies-contact-lens.03.jpg
    -   caption: ''
        filename: falling-skies-contact-lens.04.jpg
    -   caption: ''
        filename: falling-skies-contact-lens.05.jpg
    -   caption: ''
        filename: falling-skies-contact-lens.06.jpg
    -   caption: ''
        filename: falling-skies-contact-lens.07.jpg
    path: work/2013/falling-skies-promo/images/falling-skies-contact-lens-stills
    slug: falling-skies-contact-lens-stills
    title: Falling Skies Contact Lens Stills
videos:
-   caption: ''
    cover: work/2013/falling-skies-promo/videos/falling-skies-bug.jpg
    path: work/2013/falling-skies-promo/videos/falling-skies-bug
    slug: falling-skies-bug
    sources:
    -   filename: falling-skies-bug-promo-1280x720.mp4
        format: mp4
        height: '720'
        size: 29773439
        width: '1280'
    -   filename: falling-skies-bug-promo-960x540.mp4
        format: mp4
        height: '540'
        size: 20676114
        width: '960'
    -   filename: falling-skies-bug-promo-640x360.mp4
        format: mp4
        height: '360'
        size: 12688116
        width: '640'
    -   filename: falling-skies-bug-promo-640x360.webm
        format: webm
        height: '360'
        size: 12257489
        width: '640'
    -   filename: falling-skies-bug-promo-640x360.ogv
        format: ogv
        height: '360'
        size: 11520777
        width: '640'
    -   filename: falling-skies-bug-promo-480x270.mp4
        format: mp4
        height: '270'
        size: 8285777
        width: '480'
    title: Falling Skies Bug
-   caption: ''
    cover: work/2013/falling-skies-promo/videos/falling-skies-contact-lens.jpg
    path: work/2013/falling-skies-promo/videos/falling-skies-contact-lens
    slug: falling-skies-contact-lens
    sources:
    -   filename: falling-skies-contact-lens-1280x720.mp4
        format: mp4
        height: '720'
        size: 3663131
        width: '1280'
    -   filename: falling-skies-contact-lens-960x540.mp4
        format: mp4
        height: '540'
        size: 2802892
        width: '960'
    -   filename: falling-skies-contact-lens-640x360.mp4
        format: mp4
        height: '360'
        size: 1997692
        width: '640'
    -   filename: falling-skies-contact-lens-640x360.ogv
        format: ogv
        height: '360'
        size: 1907323
        width: '640'
    -   filename: falling-skies-contact-lens-640x360.webm
        format: webm
        height: '360'
        size: 1736009
        width: '640'
    -   filename: falling-skies-contact-lens-480x270.mp4
        format: mp4
        height: '270'
        size: 1455472
        width: '480'
    title: Falling Skies Contact Lens
credits: []
---
