---
title: Scion Arrival
subtitle:
slug: scion-arrival
date: 2013-11-01
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags:
- automotive
albums:
-   cover:
    images:
    -   caption: ''
        filename: scion-arrival.01.jpg
    -   caption: ''
        filename: scion-arrival.02.jpg
    -   caption: ''
        filename: scion-arrival.03.jpg
    -   caption: ''
        filename: scion-arrival.04.jpg
    -   caption: ''
        filename: scion-arrival.05.jpg
    path: work/2013/scion-arrival/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2013/scion-arrival/videos/scion-arrival.jpg
    path: work/2013/scion-arrival/videos/scion-arrival
    slug: scion-arrival
    sources:
    -   filename: scion-arrival-1280x720.mp4
        format: mp4
        height: '720'
        size: 11892189
        width: '1280'
    -   filename: scion-arrival-960x540.mp4
        format: mp4
        height: '540'
        size: 9214996
        width: '960'
    -   filename: scion-arrival-640x360.ogv
        format: ogv
        height: '360'
        size: 6094325
        width: '640'
    -   filename: scion-arrival-640x360.webm
        format: webm
        height: '360'
        size: 6023177
        width: '640'
    -   filename: scion-arrival-640x360.mp4
        format: mp4
        height: '360'
        size: 5816372
        width: '640'
    -   filename: scion-arrival-480x270.mp4
        format: mp4
        height: '270'
        size: 3871626
        width: '480'
    title: Scion Arrival
credits: []
---
