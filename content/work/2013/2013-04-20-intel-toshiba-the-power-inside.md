---
title: Intel Toshiba | "The Power Inside"
subtitle: 'The Power Inside'
slug: intel-toshiba-the-power-inside
date: 2013-04-20
role:
headline: ''
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
    - Feature Films
tags:
    - colour
    - compositing
    - clothing
    - photogrammetry
    - matte painting
    - projection
albums:
    - cover:
      images:
          - caption: ''
            filename: intel-toshiba-the-power-inside.01.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.02.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.03.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.04.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.05.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.06.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.07.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.08.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.09.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.10.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.11.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.12.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.13.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.14.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.15.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.16.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.17.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.18.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.19.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.20.jpg
          - caption: ''
            filename: intel-toshiba-the-power-inside.21.jpg
      path: work/2013/intel-toshiba-the-power-inside/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2013/intel-toshiba-the-power-inside/videos/intel-toshiba-the-power-inside-ep1.jpg
      path: work/2013/intel-toshiba-the-power-inside/videos/intel-toshiba-the-power-inside-ep1
      slug: intel-toshiba-the-power-inside-ep1
      sources:
          - filename: intel-toshiba-the-power-inside-ep1-1280x270.mp4
            format: mp4
            height: '270'
            size: 61714767
            width: '1280'
          - filename: intel-toshiba-the-power-inside-ep1-960x540.mp4
            format: mp4
            height: '540'
            size: 48231650
            width: '960'
          - filename: intel-toshiba-the-power-inside-ep1-640x360.ogv
            format: ogv
            height: '360'
            size: 34938895
            width: '640'
          - filename: intel-toshiba-the-power-inside-ep1-480x270.mp4
            format: mp4
            height: '270'
            size: 33945528
            width: '480'
          - filename: intel-toshiba-the-power-inside-ep1-640x360.webm
            format: webm
            height: '360'
            size: 33917187
            width: '640'
          - filename: intel-toshiba-the-power-inside-ep1-640x360.mp4
            format: mp4
            height: '360'
            size: 25642464
            width: '640'
      title: Intel Toshiba The Power Inside Ep1
    - caption: ''
      cover: work/2013/intel-toshiba-the-power-inside/videos/intel-toshiba-the-power-inside-ep2.jpg
      path: work/2013/intel-toshiba-the-power-inside/videos/intel-toshiba-the-power-inside-ep2
      slug: intel-toshiba-the-power-inside-ep2
      sources:
          - filename: intel-toshiba-the-power-inside-ep2-1280x720.mp4
            format: mp4
            height: '720'
            size: 56500639
            width: '1280'
          - filename: intel-toshiba-the-power-inside-ep2-960x540.mp4
            format: mp4
            height: '540'
            size: 43879085
            width: '960'
          - filename: intel-toshiba-the-power-inside-ep2-640x360.ogv
            format: ogv
            height: '360'
            size: 30850683
            width: '640'
          - filename: intel-toshiba-the-power-inside-ep2-640x360.mp4
            format: mp4
            height: '360'
            size: 30121327
            width: '640'
          - filename: intel-toshiba-the-power-inside-ep2-640x360.webm
            format: webm
            height: '360'
            size: 29482507
            width: '640'
          - filename: intel-toshiba-the-power-inside-ep2-480x270.mp4
            format: mp4
            height: '270'
            size: 22268318
            width: '480'
      title: Intel Toshiba The Power Inside Ep2
    - caption: ''
      cover: work/2013/intel-toshiba-the-power-inside/videos/intel-toshiba-the-power-inside-ep3.jpg
      path: work/2013/intel-toshiba-the-power-inside/videos/intel-toshiba-the-power-inside-ep3
      slug: intel-toshiba-the-power-inside-ep3
      sources:
          - filename: intel-toshiba-the-power-inside-ep3-1280x720.mp4
            format: mp4
            height: '720'
            size: 90218117
            width: '1280'
          - filename: intel-toshiba-the-power-inside-ep3-960x540.mp4
            format: mp4
            height: '540'
            size: 68128340
            width: '960'
          - filename: intel-toshiba-the-power-inside-ep3-640x360.webm
            format: webm
            height: '360'
            size: 47328913
            width: '640'
          - filename: intel-toshiba-the-power-inside-ep3-640x360.ogv
            format: ogv
            height: '360'
            size: 45812532
            width: '640'
          - filename: intel-toshiba-the-power-inside-ep3-640x360.mp4
            format: mp4
            height: '360'
            size: 43690377
            width: '640'
          - filename: intel-toshiba-the-power-inside-ep3-480x270.mp4
            format: mp4
            height: '270'
            size: 30566248
            width: '480'
      title: Intel Toshiba The Power Inside Ep3
    - caption: ''
      cover: work/2013/intel-toshiba-the-power-inside/videos/intel-toshiba-the-power-inside-ep4.jpg
      path: work/2013/intel-toshiba-the-power-inside/videos/intel-toshiba-the-power-inside-ep4
      slug: intel-toshiba-the-power-inside-ep4
      sources:
          - filename: intel-toshiba-the-power-inside-ep4-1280x720.mp4
            format: mp4
            height: '720'
            size: 62550483
            width: '1280'
          - filename: intel-toshiba-the-power-inside-ep4-960x540.mp4
            format: mp4
            height: '540'
            size: 49120640
            width: '960'
          - filename: intel-toshiba-the-power-inside-ep4-640x360.ogv
            format: ogv
            height: '360'
            size: 39116862
            width: '640'
          - filename: intel-toshiba-the-power-inside-ep4-640x360.webm
            format: webm
            height: '360'
            size: 38880426
            width: '640'
          - filename: intel-toshiba-the-power-inside-ep4-640x360.mp4
            format: mp4
            height: '360'
            size: 33860428
            width: '640'
          - filename: intel-toshiba-the-power-inside-ep4-480x270.mp4
            format: mp4
            height: '270'
            size: 25284939
            width: '480'
      title: Intel Toshiba The Power Inside Ep4
    - caption: ''
      cover: work/2013/intel-toshiba-the-power-inside/videos/intel-toshiba-the-power-inside-ep5.jpg
      path: work/2013/intel-toshiba-the-power-inside/videos/intel-toshiba-the-power-inside-ep5
      slug: intel-toshiba-the-power-inside-ep5
      sources:
          - filename: intel-toshiba-the-power-inside-ep5-1280x720.mp4
            format: mp4
            height: '720'
            size: 75961178
            width: '1280'
          - filename: intel-toshiba-the-power-inside-ep5-960x540.mp4
            format: mp4
            height: '540'
            size: 58184600
            width: '960'
          - filename: intel-toshiba-the-power-inside-ep5-640x360.ogv
            format: ogv
            height: '360'
            size: 42996417
            width: '640'
          - filename: intel-toshiba-the-power-inside-ep5-640x360.webm
            format: webm
            height: '360'
            size: 42659500
            width: '640'
          - filename: intel-toshiba-the-power-inside-ep5-640x360.mp4
            format: mp4
            height: '360'
            size: 38293837
            width: '640'
          - filename: intel-toshiba-the-power-inside-ep5-480x270.mp4
            format: mp4
            height: '270'
            size: 27636694
            width: '480'
      title: Intel Toshiba The Power Inside Ep5
    - caption: ''
      cover: work/2013/intel-toshiba-the-power-inside/videos/intel-toshiba-the-power-inside-ep6.jpg
      path: work/2013/intel-toshiba-the-power-inside/videos/intel-toshiba-the-power-inside-ep6
      slug: intel-toshiba-the-power-inside-ep6
      sources:
          - filename: intel-toshiba-the-power-inside-ep6-1280x720.mp4
            format: mp4
            height: '720'
            size: 184203886
            width: '1280'
          - filename: intel-toshiba-the-power-inside-ep6-960x540.mp4
            format: mp4
            height: '540'
            size: 142282686
            width: '960'
          - filename: intel-toshiba-the-power-inside-ep6-640x360.webm
            format: webm
            height: '360'
            size: 94329524
            width: '640'
          - filename: intel-toshiba-the-power-inside-ep6-640x360.ogv
            format: ogv
            height: '360'
            size: 93981199
            width: '640'
          - filename: intel-toshiba-the-power-inside-ep6-640x360.mp4
            format: mp4
            height: '360'
            size: 92037187
            width: '640'
          - filename: intel-toshiba-the-power-inside-ep6-480x270.mp4
            format: mp4
            height: '270'
            size: 64044034
            width: '480'
      title: Intel Toshiba The Power Inside Ep6
    - caption: ''
      cover: work/2013/intel-toshiba-the-power-inside/videos/intel-toshiba-the-power-inside-trailer.jpg
      path: work/2013/intel-toshiba-the-power-inside/videos/intel-toshiba-the-power-inside-trailer
      slug: intel-toshiba-the-power-inside-trailer
      sources:
          - filename: intel-toshiba-the-power-inside-trailer-1280x720.mp4
            format: mp4
            height: '720'
            size: 17931123
            width: '1280'
          - filename: intel-toshiba-the-power-inside-trailer-960x540.mp4
            format: mp4
            height: '540'
            size: 13763301
            width: '960'
          - filename: intel-toshiba-the-power-inside-trailer-640x360.mp4
            format: mp4
            height: '360'
            size: 9445210
            width: '640'
          - filename: intel-toshiba-the-power-inside-trailer-640x360.webm
            format: webm
            height: '360'
            size: 9282890
            width: '640'
          - filename: intel-toshiba-the-power-inside-trailer-640x360.ogv
            format: ogv
            height: '360'
            size: 9159962
            width: '640'
          - filename: intel-toshiba-the-power-inside-trailer-480x270.mp4
            format: mp4
            height: '270'
            size: 6771096
            width: '480'
      title: Intel Toshiba The Power Inside Trailer
credits:
    - companyName: Wieden + Kennedy
      companySlug: wk
      personName: Ben Grylewicz
      personSlug: ben-grylewicz
      roleSlug: agency-tvhead
      roleTitle: Head of Broadcast
      visible: true
    - companyName: Wieden + Kennedy
      companySlug: wk
      personName: Kirsten Acheson
      personSlug: kirsten-acheson
      roleSlug: agency-producer
      roleTitle: Agency Producer
      visible: true
    - companyName: Dashing Collective
      companySlug: dshng
      personName: Mark Kurtz
      personSlug: mark-kurtz
      roleSlug: vfx-ep
      roleTitle: Executive Producer
      visible: true
    - companyName: Dashing Collective
      companySlug: dshng
      personName: Mary Anne Ledesma
      personSlug: mary-anne-ledesma
      roleSlug: vfx-producer
      roleTitle: VFX Producer
      visible: true
    - companyName: Dashing Collective
      companySlug: dshng
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: flame
      roleTitle: Flame Artist
      visible: true
---
