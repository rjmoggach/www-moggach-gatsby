---
title: Ghost Recon Future Soldier
subtitle:
slug: ghost-recon-future-soldier
date: 2009-10-01
role:
headline: directed by Ben Mor,Little Minx, for Ubisoft
summary: Soldiers of the future battle against an evil tyrant's revolution.
excerpt: Soldiers of the future battle against an evil tyrant's revolution.
published: false
featured: false
categories:
- Advertising
tags:
- video game
- VFX
- compositing
- CG
- effects
- photoreal
- warfare
albums:
-   cover:
    images:
    -   caption: ''
        filename: ghost-recon-future-soldier.01.jpg
    -   caption: ''
        filename: ghost-recon-future-soldier.02.jpg
    -   caption: ''
        filename: ghost-recon-future-soldier.03.jpg
    -   caption: ''
        filename: ghost-recon-future-soldier.04.jpg
    path: work/2009/ghost-recon-future-soldier/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2009/ghost-recon-future-soldier/videos/ghost-recon-future-soldier.jpg
    path: work/2009/ghost-recon-future-soldier/videos/ghost-recon-future-soldier
    slug: ghost-recon-future-soldier
    sources:
    -   filename: ghost-recon-future-soldier-640x360.mp4
        format: mp4
        height: '360'
        size: 16315665
        width: '640'
    title: Ghost Recon Future Soldier
credits:
-   companyName: Ubisoft
    companySlug: ubisoft
    roleSlug: client
    roleTitle: Client
    visible: true
-   companyName: Little Minx
    companySlug: little-minx
    roleSlug: production
    roleTitle: Production Company
    visible: true
-   personName: Ben Mor
    personSlug: ben-mor
    roleSlug: director
    roleTitle: Director
    visible: true
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: true
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: flame
    roleTitle: Flame Artist
    visible: true
---
Soldiers of the future battle against an evil tyrant's revolution.