---
title: Sos Lorran E Ge
subtitle:
slug: sos-lorran-e-ge
date: 2009-10-02
role:
headline: directed by Casey Affleck, RSA FIlms
summary:
excerpt:
published: false
featured: false
categories:
- Advertising
tags:
- VFX
- photoreal
- compositing
- colour
albums:
-   cover:
    images:
    -   caption: ''
        filename: sos-lorran-e-ge.01.jpg
    -   caption: ''
        filename: sos-lorran-e-ge.02.jpg
    -   caption: ''
        filename: sos-lorran-e-ge.03.jpg
    -   caption: ''
        filename: sos-lorran-e-ge.04.jpg
    -   caption: ''
        filename: sos-lorran-e-ge.05.jpg
    -   caption: ''
        filename: sos-lorran-e-ge.06.jpg
    -   caption: ''
        filename: sos-lorran-e-ge.07.jpg
    path: work/2009/sos-lorran-e-ge/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2009/sos-lorran-e-ge/videos/sos-lorran-e-ge.jpg
    path: work/2009/sos-lorran-e-ge/videos/sos-lorran-e-ge
    slug: sos-lorran-e-ge
    sources:
    -   filename: sos-lorran-e-ge-1280x720.mp4
        format: mp4
        height: '720'
        size: 158399097
        width: '1280'
    -   filename: sos-lorran-e-ge-960x540.mp4
        format: mp4
        height: '540'
        size: 109478068
        width: '960'
    -   filename: sos-lorran-e-ge-640x360.mp4
        format: mp4
        height: '360'
        size: 67330361
        width: '640'
    -   filename: sos-lorran-e-ge-640x360.ogv
        format: ogv
        height: '360'
        size: 66861031
        width: '640'
    -   filename: sos-lorran-e-ge-640x360.webm
        format: webm
        height: '360'
        size: 49467119
        width: '640'
    -   filename: sos-lorran-e-ge-480x270.mp4
        format: mp4
        height: '270'
        size: 42206769
        width: '480'
    title: Sos Lorran E Ge
credits:
-   companyName: RSA Films
    companySlug: rsa-films
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Casey Affleck
    personSlug: casey-affleck
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: flame
    roleTitle: Flame Artist
    visible: false
---
