---
title: Kia Forte Pioneers
subtitle:
slug: kia-forte-pioneers
date: 2009-06-15
role:
headline: directed by Tarsem, @radical media for David + Goliath
summary: Kia adapts pioneering ideas with pioneering affordability.
excerpt: Kia adapts pioneering ideas with pioneering affordability.
published: false
featured: false
categories:
- Advertising
tags:
- automotive
- VFX
- compositing
- photoreal
- CG
- projections
- projection
- colour
albums:
-   cover:
    images:
    -   caption: ''
        filename: kia-forte-pioneers.01.jpg
    -   caption: ''
        filename: kia-forte-pioneers.02.jpg
    -   caption: ''
        filename: kia-forte-pioneers.03.jpg
    -   caption: ''
        filename: kia-forte-pioneers.04.jpg
    -   caption: ''
        filename: kia-forte-pioneers.05.jpg
    -   caption: ''
        filename: kia-forte-pioneers.06.jpg
    path: work/2009/kia-forte-pioneers/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2009/kia-forte-pioneers/videos/kia-forte-pioneers.jpg
    path: work/2009/kia-forte-pioneers/videos/kia-forte-pioneers
    slug: kia-forte-pioneers
    sources:
    -   filename: kia-forte-pioneers-1280x720.mp4
        format: mp4
        height: '720'
        size: 20779956
        width: '1280'
    -   filename: kia-forte-pioneers-960x540.mp4
        format: mp4
        height: '540'
        size: 15593771
        width: '960'
    -   filename: kia-forte-pioneers-640x360.mp4
        format: mp4
        height: '360'
        size: 8984962
        width: '640'
    -   filename: kia-forte-pioneers-640x360.ogv
        format: ogv
        height: '360'
        size: 8167010
        width: '640'
    -   filename: kia-forte-pioneers-640x360.webm
        format: webm
        height: '360'
        size: 6239575
        width: '640'
    -   filename: kia-forte-pioneers-480x270.mp4
        format: mp4
        height: '270'
        size: 5170227
        width: '480'
    title: Kia Forte Pioneers
credits:
-   companyName: Kia
    companySlug: kia
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: David + Goliath
    companySlug: david-goliath
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: false
-   companyName: '@Radical Media'
    companySlug: radical-media
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: 'Tarsem '
    personSlug: tarsem
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
Kia adapts pioneering ideas with pioneering affordability.