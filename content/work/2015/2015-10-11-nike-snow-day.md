---
title: Nike | "Snow Day"
subtitle: 'Snow Day'
slug: nike-snow-day
date: 2015-10-11
role:
headline: Professional athletes relive their winter youth.
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
tags:
    - VFX
    - animation
    - CG
    - environment
    - tracking
    - set extensions
    - clothing
albums:
    - cover:
      images:
          - caption: ''
            filename: nike-snow-day.01.jpg
          - caption: ''
            filename: nike-snow-day.02.jpg
          - caption: ''
            filename: nike-snow-day.03.jpg
          - caption: ''
            filename: nike-snow-day.04.jpg
          - caption: ''
            filename: nike-snow-day.05.jpg
          - caption: ''
            filename: nike-snow-day.06.jpg
      path: work/2015/nike-snow-day/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2015/nike-snow-day/videos/nike-snow-day.jpg
      path: work/2015/nike-snow-day/videos/nike-snow-day
      provider: html5
      slug: nike-snow-day
      sources:
          - filename: nike-snow-day-1280x720.mp4
            format: mp4
            height: '720'
            size: 43027306
            width: '1280'
          - filename: nike-snow-day-960x540.mp4
            format: mp4
            height: '540'
            size: 31846035
            width: '960'
          - filename: nike-snow-day-640x360.mp4
            format: mp4
            height: '360'
            size: 19921653
            width: '640'
          - filename: nike-snow-day-640x360.webm
            format: webm
            height: '360'
            size: 18529472
            width: '640'
          - filename: nike-snow-day-640x360.ogv
            format: ogv
            height: '360'
            size: 18439613
            width: '640'
          - filename: nike-snow-day-480x270.mp4
            format: mp4
            height: '270'
            size: 13075904
            width: '480'
      title: Nike Snow Day
credits:
    - companyName: The Mission
      companySlug: mission
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: true
    - companyName: The Mission
      companySlug: mission
      personName: Joseph Brattesani
      personSlug: joseph-brattesani
      roleSlug: lead flame
      roleTitle: Lead Flame
      visible: true
    - companyName: The Mission
      companySlug: mission
      personName: Michael Pardee
      personSlug: michael-pardee
      roleSlug: vfx-ep
      roleTitle: Executive Producer
      visible: true
    - companyName: The Mission
      companySlug: mission
      personName: Diana Cheng
      personSlug: diana-cheng
      roleSlug: vfx-producer
      roleTitle: VFX Producer
      visible: true
    - companyName: The Mission
      companySlug: mission
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: flame
      roleTitle: Flame Artist
      visible: true
---

Giant spot executed flawlessly by the team at The Mission in Venice Beach. I had the pleasure of providing "procedural" breath for all the athletes who were actually shot on a massive set build in Los Angeles.
