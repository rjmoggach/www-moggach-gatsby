---
title: Verizon
subtitle: Visualize The Win
slug: verizon-visualize-the-win
date: 2015-12-24
role:
headline: Verizon summarizes it's dominance using a collection of colored of balls.
summary:
excerpt:
published: false
featured: false
categories:
    - Advertising
tags:
    - motion control
    - photogrammetry
    - CG
    - VFX
    - animation
    - effects
    - compositing
    - tracking
    - rig removal
    - set extensions
    - photoreal
    - cellular
albums:
    - cover:
      images:
          - caption: ''
            filename: verizon-visualize-the-win.01.jpg
          - caption: ''
            filename: verizon-visualize-the-win.02.jpg
          - caption: ''
            filename: verizon-visualize-the-win.03.jpg
          - caption: ''
            filename: verizon-visualize-the-win.04.jpg
          - caption: ''
            filename: verizon-visualize-the-win.05.jpg
          - caption: ''
            filename: verizon-visualize-the-win.06.jpg
          - caption: ''
            filename: verizon-visualize-the-win.07.jpg
      path: work/2015/verizon-visualize-the-win/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2015/verizon-visualize-the-win/videos/verizon-visualize-the-win.jpg
      path: work/2015/verizon-visualize-the-win/videos/verizon-visualize-the-win
      provider: html5
      slug: verizon-visualize-the-win
      sources:
          - filename: verizon-visualize-the-win-1280x720.mp4
            format: mp4
            height: '720'
            size: 3552309
            width: '1280'
          - filename: verizon-visualize-the-win-960x540.mp4
            format: mp4
            height: '540'
            size: 2683987
            width: '960'
          - filename: verizon-visualize-the-win-640x360.ogv
            format: ogv
            height: '360'
            size: 2555165
            width: '640'
          - filename: verizon-visualize-the-win-640x360.webm
            format: webm
            height: '360'
            size: 2314684
            width: '640'
          - filename: verizon-visualize-the-win-640x360.mp4
            format: mp4
            height: '360'
            size: 1905318
            width: '640'
          - filename: verizon-visualize-the-win-480x270.mp4
            format: mp4
            height: '270'
            size: 1480244
            width: '480'
      title: Verizon Visualize The Win
credits:
    - companyName: Verizon
      companySlug: verizon
      roleSlug: client
      roleTitle: Client
      visible: true
    - companyName: Wieden + Kennedy
      companySlug: wk
      roleSlug: agency
      roleTitle: Advertising Agency
      visible: true
    - companyName: Wieden + Kennedy
      companySlug: wk
      personName: Endy Hedman
      personSlug: endy-hedman
      roleSlug: agency-producer
      roleTitle: Agency Producer
      visible: true
    - companyName: Joint Editorial
      companySlug: joint-editorial
      roleSlug: edit
      roleTitle: Editorial
      visible: true
    - companyName: Joint Editorial
      companySlug: joint-editorial
      personName: Alex Thiesen
      personSlug: alex-thiesen
      roleSlug: edit-ep
      roleTitle: Executive Producer
      visible: true
    - companyName: Alteration Services
      companySlug: alteration
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: true
    - companyName: Alteration Services
      companySlug: alteration
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: vfx-supervisor
      roleTitle: VFX Supervisor
      visible: true
    - companyName: Alteration Services
      companySlug: alteration
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: colorist
      roleTitle: Colorist
      visible: true
    - companyName: Alteration Services
      companySlug: alteration
      personName: Doug Law
      personSlug: doug-law
      roleSlug: cg-generalist
      roleTitle: CG Generalist
      visible: true
---

Fun little job that gained some notoriety for the competition's numerous rebuttals. I was onset sup for this one and also finished the color, vfx (including some CG balls), and finishing.

> Thanks to friends at W+K Portland and Joint Editorial for the opportunity to once again collaborate with great success.
