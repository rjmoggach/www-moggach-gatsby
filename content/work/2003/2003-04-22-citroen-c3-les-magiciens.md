---
title: Citroen C3 Les Magiciens
subtitle:
slug: citroen-c3-les-magiciens
date: 2003-04-22
role:
headline: directed by Tarsem, @radical media
summary: Kids use magic to transform the Citroen C3 and the world around them.
excerpt: Kids use magic to transform the Citroen C3 and the world around them.
published: false
featured: false
categories: []
tags:
- automotive
albums:
-   cover:
    images:
    -   caption: ''
        filename: citroen-c3-les-magiciens.01.jpg
    -   caption: ''
        filename: citroen-c3-les-magiciens.02.jpg
    -   caption: ''
        filename: citroen-c3-les-magiciens.03.jpg
    -   caption: ''
        filename: citroen-c3-les-magiciens.04.jpg
    -   caption: ''
        filename: citroen-c3-les-magiciens.05.jpg
    -   caption: ''
        filename: citroen-c3-les-magiciens.06.jpg
    path: work/2003/citroen-c3-les-magiciens/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2003/citroen-c3-les-magiciens/videos/citroen-c3-les-magiciens.jpg
    path: work/2003/citroen-c3-les-magiciens/videos/citroen-c3-les-magiciens
    slug: citroen-c3-les-magiciens
    sources:
    -   filename: citroen-c3-les-magiciens.1280x960.mp4
        format: mp4
        height: '960'
        size: 9665430
        width: '1280'
    -   filename: citroen-c3-les-magiciens.960x720.mp4
        format: mp4
        height: '720'
        size: 6104115
        width: '960'
    -   filename: citroen-c3-les-magiciens.640x360.ogv
        format: ogv
        height: '360'
        size: 4243860
        width: '640'
    -   filename: citroen-c3-les-magiciens.640x480.mp4
        format: mp4
        height: '480'
        size: 4162794
        width: '640'
    -   filename: citroen-c3-les-magiciens.640x360.webm
        format: webm
        height: '360'
        size: 3040544
        width: '640'
    -   filename: citroen-c3-les-magiciens.480x360.mp4
        format: mp4
        height: '360'
        size: 2328757
        width: '480'
    title: Citroen C3 Les Magiciens
credits:
-   companyName: Citroen
    companySlug: citroen
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: '@Radical Media'
    companySlug: radical-media
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: 'Tarsem '
    personSlug: tarsem
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Play Productions
    companySlug: play
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Tom Sparks
    personSlug: tom-sparks
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: inferno-artist
    roleTitle: Inferno Artist
    visible: false
---
Flying siblings arguing, as you do, about the configuration of Citro?n's
newest car, use their magical gloves to transform the car. Drawing loose
reference from Japanese animation, the task at hand was to create a magical,
transformation effect that was playful, whimsical and elegant at the same
time. This transition was to be applied to the car as it changed form and in
some instances to the entire landscape as the kids moved from locale to
locale. All the elements were generated in Flame using the built in 3D
capabilities. 3D models of the car were used within Flame to approximate the
surface of the car and create realistic occlusions. For most of the commercial
different takes of both children were used and needed to be combined in post.
The car was shot in all of it's four configurations in situe and animated in
Flame. Matte paintings of the various locations were created to ease the
transitions and create a more interesting visual.