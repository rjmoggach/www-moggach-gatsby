---
title: Levis Dancer
subtitle:
slug: levis-dancer
date: 2003-11-01
role:
headline: directed by Robert Moggach
summary: Mysterious man tests his 501s in a shamanistic dance in the desert.
excerpt: Mysterious man tests his 501s in a shamanistic dance in the desert.
published: false
featured: false
categories: []
tags:
- Live Action
- compositing
albums:
-   cover:
    images:
    -   caption: ''
        filename: levis-dancer.01.jpg
    -   caption: ''
        filename: levis-dancer.02.jpg
    -   caption: ''
        filename: levis-dancer.03.jpg
    -   caption: ''
        filename: levis-dancer.04.jpg
    -   caption: ''
        filename: levis-dancer.05.jpg
    -   caption: ''
        filename: levis-dancer.06.jpg
    -   caption: ''
        filename: levis-dancer.07.jpg
    -   caption: ''
        filename: levis-dancer.08.jpg
    -   caption: ''
        filename: levis-dancer.09.jpg
    -   caption: ''
        filename: levis-dancer.10.jpg
    -   caption: ''
        filename: levis-dancer.11.jpg
    -   caption: ''
        filename: levis-dancer.12.jpg
    -   caption: ''
        filename: levis-dancer.13.jpg
    -   caption: ''
        filename: levis-dancer.14.jpg
    path: work/2003/levis-dancer/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2003/levis-dancer/videos/levis-dancer.jpg
    path: work/2003/levis-dancer/videos/levis-dancer
    slug: levis-dancer
    sources:
    -   filename: levis-dancer-24-960x540.mp4
        format: mp4
        height: '540'
        size: 22091632
        width: '960'
    -   filename: levis-dancer-24-1280x720.mp4
        format: mp4
        height: '720'
        size: 21063802
        width: '1280'
    -   filename: levis-dancer-24-640x360.ogv
        format: ogv
        height: '360'
        size: 10704105
        width: '640'
    -   filename: levis-dancer-24-640x360.mp4
        format: mp4
        height: '360'
        size: 10625145
        width: '640'
    -   filename: levis-dancer-24-640x360.webm
        format: webm
        height: '360'
        size: 10427116
        width: '640'
    -   filename: levis-dancer-24-480x270.mp4
        format: mp4
        height: '270'
        size: 7086899
        width: '480'
    title: Levis Dancer
credits:
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Harris Charalambous
    personSlug: harris-charalambous
    roleSlug: dp
    roleTitle: Director of Photography
    visible: false
---
This commercial depicts a shamanistic figure going about his daily routine of
meditative dance in a harsh yet visually pristine environment. Once his
personal challenges are met he returns to a state of rest and contemplation.
This film was shot in two days at El Mirage Dry Lake Bed in Southern
California by DP Harris Charalambous. Telecine was done by Fergus McCall at
The Mill London. Mark Reynolds of Swordfish edited the commercial and Robert
Moggach did the inferno/online work while at Play Productions in London. This
commercial was featured on the cover of Shots Magazine in August of 2004.