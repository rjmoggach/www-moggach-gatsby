---
title: Pepsi Gladiator
subtitle:
slug: pepsi-gladiator
date: 2003-10-01
role:
headline: directed by Tarsem, @radical media
summary: Britney, Beyonce & Pink take on emperor Enrique for Pepsi.
excerpt: Britney, Beyonce & Pink take on emperor Enrique for Pepsi.
published: false
featured: false
categories: []
tags:
- crowds
- environment
- set extensions
- compositing
albums:
-   cover:
    images:
    -   caption: ''
        filename: pepsi-gladiator.01.jpg
    -   caption: ''
        filename: pepsi-gladiator.02.jpg
    -   caption: ''
        filename: pepsi-gladiator.03.jpg
    -   caption: ''
        filename: pepsi-gladiator.04.jpg
    -   caption: ''
        filename: pepsi-gladiator.05.jpg
    -   caption: ''
        filename: pepsi-gladiator.06.jpg
    -   caption: ''
        filename: pepsi-gladiator.07.jpg
    -   caption: ''
        filename: pepsi-gladiator.08.jpg
    -   caption: ''
        filename: pepsi-gladiator.09.jpg
    -   caption: ''
        filename: pepsi-gladiator.10.jpg
    -   caption: ''
        filename: pepsi-gladiator.11.jpg
    -   caption: ''
        filename: pepsi-gladiator.12.jpg
    path: work/2003/pepsi-gladiator/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2003/pepsi-gladiator/videos/pepsi-gladiator.jpg
    path: work/2003/pepsi-gladiator/videos/pepsi-gladiator
    slug: pepsi-gladiator
    sources:
    -   filename: pepsi-gladiator-960x540.mp4
        format: mp4
        height: '540'
        size: 70604124
        width: '960'
    -   filename: pepsi-gladiator-1280x720.mp4
        format: mp4
        height: '720'
        size: 63719189
        width: '1280'
    -   filename: pepsi-gladiator-480x270.mp4
        format: mp4
        height: '270'
        size: 32202326
        width: '480'
    -   filename: pepsi-gladiator-640x360.mp4
        format: mp4
        height: '360'
        size: 32194206
        width: '640'
    -   filename: pepsi-gladiator-640x360.ogv
        format: ogv
        height: '360'
        size: 31399487
        width: '640'
    -   filename: pepsi-gladiator-640x360.webm
        format: webm
        height: '360'
        size: 31291581
        width: '640'
    title: Pepsi Gladiator
credits:
-   companyName: Pepsi
    companySlug: pepsi
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: '@Radical Media'
    companySlug: radical-media
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: 'Tarsem '
    personSlug: tarsem
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Play Productions
    companySlug: play
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Tom Sparks
    personSlug: tom-sparks
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: inferno-artist
    roleTitle: Inferno Artist
    visible: false
---
Set in the collosseum of ancient Rome, this is a story of three gladiators,
played by Britney Spears, Beyonce Knowles, and Pink, joining together to usurp
the Pepsi-hoarding emperor, Enrique Iglesias. The three girls bring the crowd
to an angry roar with a rendition of Queen's "We Will Rock You" and the
emperor is catapulted to the lions by the ensuing noise. We had the task of
creating the entire collosseum of screaming, cheering spectators from one tier
of one half of a replica of the collosseum. CG artists at
[Glassworks](http://www.glassworks.co.uk) in London created tens of layers of
stadium and crowd elements which were all combined in flame. The challenges
included not having any blue or green screens to ease the integration of the
live action and no motion control for the more than 50 wild camera shots
requiring CG. The 3 minute commercial was finished at film resolution and 12
bit colourspace to maximize the quality and allow us to more easily integrate
the CG elements. This in itself presented a problem given the huge amounts of
data that needed to move seamlessly between facilities on an almost daily
basis.