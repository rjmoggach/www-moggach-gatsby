---
title: Veronica Guerin
subtitle:
slug: veronica-guerin
date: 2003-10-17
role:
headline: directed by Joel Schumacher, Touchstone
summary:
excerpt:
published: false
featured: false
categories:
- Feature Films
tags:
- crowds
- matte painting
- compositing
albums:
-   cover:
    images:
    -   caption: ''
        filename: veronica-guerin.01.jpg
    -   caption: ''
        filename: veronica-guerin.02.jpg
    path: work/2003/veronica-guerin/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2003/veronica-guerin/videos/veronica-guerin.jpg
    path: work/2003/veronica-guerin/videos/veronica-guerin
    slug: veronica-guerin
    sources:
    -   filename: veronica-guerin-1280x720.mp4
        format: mp4
        height: '720'
        size: 8537193
        width: '1280'
    -   filename: veronica-guerin-960x540.mp4
        format: mp4
        height: '540'
        size: 6639692
        width: '960'
    -   filename: veronica-guerin-640x360.ogv
        format: ogv
        height: '360'
        size: 4905482
        width: '640'
    -   filename: veronica-guerin-640x360.mp4
        format: mp4
        height: '360'
        size: 4737646
        width: '640'
    -   filename: veronica-guerin-480x270.mp4
        format: mp4
        height: '270'
        size: 3721088
        width: '480'
    -   filename: veronica-guerin-640x360.webm
        format: webm
        height: '360'
        size: 3292451
        width: '640'
    title: Veronica Guerin
credits:
-   companyName: Touchstone Pictures
    companySlug: touchstone-pictures
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Joel Schumacher
    personSlug: joel-schumacher
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: lead flame
    roleTitle: Lead Flame
    visible: false
---
