---
title: Austin Powers Goldmember
subtitle:
slug: austin-powers-goldmember
date: 2002-07-26
role:
headline: directed by Jay Roach, New Line Cinema
summary:
excerpt:
published: false
featured: false
categories:
- Feature Films
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: austin-powers-goldmember.01.jpg
    -   caption: ''
        filename: austin-powers-goldmember.02.jpg
    -   caption: ''
        filename: austin-powers-goldmember.03.jpg
    path: work/2002/austin-powers-goldmember/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2002/austin-powers-goldmember/videos/austin-powers-goldmember.jpg
    path: work/2002/austin-powers-goldmember/videos/austin-powers-goldmember
    slug: austin-powers-goldmember
    sources:
    -   filename: austin-powers-goldmember.960x540.mp4
        format: mp4
        height: '540'
        size: 13483412
        width: '960'
    -   filename: austin-powers-goldmember.1280x720.mp4
        format: mp4
        height: '720'
        size: 12222154
        width: '1280'
    -   filename: austin-powers-goldmember.640x360.mp4
        format: mp4
        height: '360'
        size: 6232625
        width: '640'
    -   filename: austin-powers-goldmember.640x360.ogv
        format: ogv
        height: '360'
        size: 6232008
        width: '640'
    -   filename: austin-powers-goldmember.640x360.webm
        format: webm
        height: '360'
        size: 6194855
        width: '640'
    -   filename: austin-powers-goldmember.480x270.mp4
        format: mp4
        height: '270'
        size: 5017114
        width: '480'
    title: Austin Powers Goldmember
credits:
-   companyName: New Line Cinema
    companySlug: new-line-cinema
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Jay Roach
    personSlug: jay-roach
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: lead flame
    roleTitle: Lead Flame
    visible: false
---
