---
title: Aphex Twin Rubber Johnny
subtitle:
slug: aphex-twin-rubber-johnny
date: 2002-02-01
role:
headline: directed by Chris Cunningham, RSA Films UK
summary:
excerpt:
published: false
featured: false
categories:
- Music Videos
tags:
- compositing
- head replacement
albums:
-   cover:
    images:
    -   caption: ''
        filename: aphex-twin-rubber-johnny.01.jpg
    -   caption: ''
        filename: aphex-twin-rubber-johnny.02.jpg
    -   caption: ''
        filename: aphex-twin-rubber-johnny.03.jpg
    -   caption: ''
        filename: aphex-twin-rubber-johnny.04.jpg
    path: work/2002/aphex-twin-rubber-johnny/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2002/aphex-twin-rubber-johnny/videos/aphex-twin-rubber-johnny.jpg
    path: work/2002/aphex-twin-rubber-johnny/videos/aphex-twin-rubber-johnny
    slug: aphex-twin-rubber-johnny
    sources:
    -   filename: aphex-twin-rubber-johnny.1280x720.mp4
        format: mp4
        height: '720'
        size: 117222721
        width: '1280'
    -   filename: aphex-twin-rubber-johnny.960x540.mp4
        format: mp4
        height: '540'
        size: 83695956
        width: '960'
    -   filename: aphex-twin-rubber-johnny.640x360.mp4
        format: mp4
        height: '360'
        size: 48913229
        width: '640'
    -   filename: aphex-twin-rubber-johnny.640x360.ogv
        format: ogv
        height: '360'
        size: 31530654
        width: '640'
    -   filename: aphex-twin-rubber-johnny.480x270.mp4
        format: mp4
        height: '270'
        size: 28006556
        width: '480'
    -   filename: aphex-twin-rubber-johnny.640x360.webm
        format: webm
        height: '360'
        size: 23222841
        width: '640'
    title: Aphex Twin Rubber Johnny
credits:
-   personName: Chris Cunningham
    personSlug: chris-cunningham
    roleSlug: director
    roleTitle: Director
    visible: true
-   companyName: The Mill UK
    companySlug: mill-uk
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: true
-   companyName: Smoke & Mirrors
    companySlug: smoke-mirrors
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: true
-   personName: Judy Roberts
    personSlug: judy-roberts
    roleSlug: flame
    roleTitle: Flame Artist
    visible: true
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: flame
    roleTitle: Flame Artist
    visible: true
---
