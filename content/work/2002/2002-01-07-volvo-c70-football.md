---
title: Volvo C70 "Football"
subtitle:
slug: volvo-c70-football
date: 2002-01-01
role:
headline: directed by Alex Turner, Godman
summary: Volvo C70 driver is envied by rival football fans.
excerpt: Volvo C70 driver is envied by rival football fans.
published: false
featured: false
categories: []
tags:
- automotive
albums:
-   cover:
    images:
    -   caption: ''
        filename: volvo-c70-football.01.jpg
    -   caption: ''
        filename: volvo-c70-football.02.jpg
    -   caption: ''
        filename: volvo-c70-football.03.jpg
    -   caption: ''
        filename: volvo-c70-football.04.jpg
    path: work/2002/volvo-c70-football/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2002/volvo-c70-football/videos/volvo-c70-football.jpg
    path: work/2002/volvo-c70-football/videos/volvo-c70-football
    slug: volvo-c70-football
    sources:
    -   filename: volvo-c70-football-1280x720.mp4
        format: mp4
        height: '720'
        size: 21016299
        width: '1280'
    -   filename: volvo-c70-football-960x540.mp4
        format: mp4
        height: '540'
        size: 15935736
        width: '960'
    -   filename: volvo-c70-football-640x360.ogv
        format: ogv
        height: '360'
        size: 10463672
        width: '640'
    -   filename: volvo-c70-football-640x360.mp4
        format: mp4
        height: '360'
        size: 10226007
        width: '640'
    -   filename: volvo-c70-football-640x360.webm
        format: webm
        height: '360'
        size: 8930585
        width: '640'
    -   filename: volvo-c70-football-480x270.mp4
        format: mp4
        height: '270'
        size: 6816852
        width: '480'
    title: Volvo C70 Football
credits:
-   companyName: Volvo
    companySlug: volvo
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Godman
    companySlug: godman
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Alex Turner
    personSlug: alex-turner
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Smoke & Mirrors
    companySlug: smoke-mirrors
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
This film for the Volvo C70 convertible tells the story of the safety that a
driver feels in the car despite it being a convertible. A football fan drives
through the streets of a picturesque Italian village, daydreaming, and unaware
that the village is full of the opposite team's fans. When he realises that he
is in potentially hostile territory he locks the doors of his convertible and
feels safe in his car. Our task in this commercial was not create elaborate
special effects that defy any imagination but rather to enhance the beauty of
the footage and the car by isolating the car and making it's surface appear
shinier and more reflective. In all of the shots of the car, the car was
isolated and enhanced, separately from the rest of the footage. Other effects
work included animating the locking of the door, combining multiple takes of
live action crowds to create a stronger composition, and integrating /
creating footage for the televisions that appear throughout the commercial.