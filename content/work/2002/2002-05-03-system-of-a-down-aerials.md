---
title: System Of A Down Aerials
subtitle:
slug: system-of-a-down-aerials
date: 2002-05-01
role:
headline: directed by David Slade, RSA Films
summary: Oddly deformed boy dreams of stardom.
excerpt: Oddly deformed boy dreams of stardom.
published: false
featured: false
categories:
- Music Videos
tags:
- compositing
- facial warping
albums:
-   cover:
    images:
    -   caption: ''
        filename: system-of-a-down-aerials.01.jpg
    -   caption: ''
        filename: system-of-a-down-aerials.02.jpg
    -   caption: ''
        filename: system-of-a-down-aerials.03.jpg
    -   caption: ''
        filename: system-of-a-down-aerials.04.jpg
    -   caption: ''
        filename: system-of-a-down-aerials.05.jpg
    -   caption: ''
        filename: system-of-a-down-aerials.06.jpg
    path: work/2002/system-of-a-down-aerials/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2002/system-of-a-down-aerials/videos/system-of-a-down-aerials.jpg
    path: work/2002/system-of-a-down-aerials/videos/system-of-a-down-aerials
    slug: system-of-a-down-aerials
    sources:
    -   filename: system-of-a-down-aerials-1280x720.mp4
        format: mp4
        height: '720'
        size: 12215128
        width: '1280'
    -   filename: system-of-a-down-aerials-960x540.mp4
        format: mp4
        height: '540'
        size: 9806423
        width: '960'
    -   filename: system-of-a-down-aerials-640x360.ogv
        format: ogv
        height: '360'
        size: 6091836
        width: '640'
    -   filename: system-of-a-down-aerials-640x360.mp4
        format: mp4
        height: '360'
        size: 5839897
        width: '640'
    -   filename: system-of-a-down-aerials-640x360.webm
        format: webm
        height: '360'
        size: 4911168
        width: '640'
    -   filename: system-of-a-down-aerials-480x270.mp4
        format: mp4
        height: '270'
        size: 3756740
        width: '480'
    title: System Of A Down Aerials
credits:
-   companyName: Black Dog
    companySlug: black-dog
    roleSlug: production
    roleTitle: Production Company
    visible: true
-   personName: David Slade
    personSlug: david-slade
    roleSlug: director
    roleTitle: Director
    visible: true
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: true
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: true
---
Visionary music video director, David Slade, had a very defined idea for the
look of this video. The surreal distortion of a otherwise normal boy caught up
in the throws of stardom had to be believable and not interpreted as a special
effect. Using a proprietary combination of selective matting, warping,
sharpening, shading, and adding film grain I created a look that he was
instantly happy with. The technique I developed allowed for a completely
interactive process for the more than 50 shots in the video. Instead of the
usual waiting associated with the detailed nature of this type of project, I
was able to keep David involved in the creative development through the
evolution of every shot. The result is an unsettling set of images that left a
lot of people questioning the methodology and garnering critical acclaim in
the UK and North America.