---
title: Catch Me If You Can
subtitle:
slug: catch-me-if-you-can
date: 2002-11-01
role:
headline: directed by Steven Spielberg, DreamWorks
summary: Leonardo DiCaprio and Tom Hanks on a plane.
excerpt: Leonardo DiCaprio and Tom Hanks on a plane.
published: false
featured: false
categories:
- Feature Films
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: catch-me-if-you-can.01.jpg
    -   caption: ''
        filename: catch-me-if-you-can.02.jpg
    -   caption: ''
        filename: catch-me-if-you-can.03.jpg
    path: work/2002/catch-me-if-you-can/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2002/catch-me-if-you-can/videos/catch-me-if-you-can.jpg
    path: work/2002/catch-me-if-you-can/videos/catch-me-if-you-can
    slug: catch-me-if-you-can
    sources:
    -   filename: catch-me-if-you-can-1280x720.mp4
        format: mp4
        height: '720'
        size: 14986688
        width: '1280'
    -   filename: catch-me-if-you-can-960x540.mp4
        format: mp4
        height: '540'
        size: 10703453
        width: '960'
    -   filename: catch-me-if-you-can-640x360.mp4
        format: mp4
        height: '360'
        size: 6440348
        width: '640'
    -   filename: catch-me-if-you-can-640x360.ogv
        format: ogv
        height: '360'
        size: 5579856
        width: '640'
    -   filename: catch-me-if-you-can-480x270.mp4
        format: mp4
        height: '270'
        size: 4369246
        width: '480'
    -   filename: catch-me-if-you-can-640x360.webm
        format: webm
        height: '360'
        size: 4133397
        width: '640'
    title: Catch Me If You Can
credits:
-   companyName: Dreamworks
    companySlug: dreamworks
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Steven Spielberg
    personSlug: steven-spielberg
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: flame
    roleTitle: Flame Artist
    visible: false
---
I had the distinct privelege of working for director Steven Spielberg on his
film "Catch Me If You Can on approximately ten visual effects shots. We had
the task of creating a view of the cloudscape and of New York from inside the
plane. It was a relatively simple task of keying blue screen footage on top of
flame generated cloudscapes and city backgrounds. It's a simple task but one
that I still remember fondly given the stature of the director and his cast.