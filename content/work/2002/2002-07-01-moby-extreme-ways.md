---
title: Moby Extreme Ways
subtitle:
slug: moby-extreme-ways
date: 2002-07-01
role:
headline: directed by Wayne Isham, The Institute
summary:
excerpt:
published: false
featured: false
categories:
- Music Videos
tags:
- compositing
- motion control
- matte painting
- crowds
albums:
-   cover:
    images:
    -   caption: ''
        filename: moby-extreme-ways.01.jpg
    -   caption: ''
        filename: moby-extreme-ways.02.jpg
    -   caption: ''
        filename: moby-extreme-ways.03.jpg
    -   caption: ''
        filename: moby-extreme-ways.04.jpg
    -   caption: ''
        filename: moby-extreme-ways.05.jpg
    -   caption: ''
        filename: moby-extreme-ways.06.jpg
    -   caption: ''
        filename: moby-extreme-ways.07.jpg
    path: work/2002/moby-extreme-ways/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2002/moby-extreme-ways/videos/moby-extreme-ways.jpg
    path: work/2002/moby-extreme-ways/videos/moby-extreme-ways
    slug: moby-extreme-ways
    sources:
    -   filename: moby-extreme-ways-1280x720.mp4
        format: mp4
        height: '720'
        size: 6012350
        width: '1280'
    -   filename: moby-extreme-ways-960x540.mp4
        format: mp4
        height: '540'
        size: 5054326
        width: '960'
    -   filename: moby-extreme-ways-640x360.mp4
        format: mp4
        height: '360'
        size: 2894534
        width: '640'
    -   filename: moby-extreme-ways-640x360.ogv
        format: ogv
        height: '360'
        size: 2892961
        width: '640'
    -   filename: moby-extreme-ways-640x360.webm
        format: webm
        height: '360'
        size: 2292124
        width: '640'
    -   filename: moby-extreme-ways-480x270.mp4
        format: mp4
        height: '270'
        size: 1884466
        width: '480'
    title: Moby Extreme Ways
credits:
-   companyName: A Band Apart
    companySlug: band-apart
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Wayne Isham
    personSlug: wayne-isham
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: inferno-artist
    roleTitle: Inferno Artist
    visible: false
---
