---
title: Bmw Films Ticker
subtitle:
slug: bmw-films-ticker
date: 2002-08-31
role:
headline: directed by Joe Carnahan, RSA
summary:
excerpt:
published: false
featured: false
categories:
- Advertising
tags:
- automotive
- compositing
- warfare
- CG
- explosions
albums:
-   cover:
    images:
    -   caption: ''
        filename: bmw-films-ticker.01.jpg
    -   caption: ''
        filename: bmw-films-ticker.02.jpg
    -   caption: ''
        filename: bmw-films-ticker.03.jpg
    -   caption: ''
        filename: bmw-films-ticker.04.jpg
    -   caption: ''
        filename: bmw-films-ticker.05.jpg
    -   caption: ''
        filename: bmw-films-ticker.06.jpg
    -   caption: ''
        filename: bmw-films-ticker.07.jpg
    -   caption: ''
        filename: bmw-films-ticker.08.jpg
    -   caption: ''
        filename: bmw-films-ticker.09.jpg
    -   caption: ''
        filename: bmw-films-ticker.10.jpg
    path: work/2002/bmw-films-ticker/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2002/bmw-films-ticker/videos/bmw-films-ticker.jpg
    path: work/2002/bmw-films-ticker/videos/bmw-films-ticker
    slug: bmw-films-ticker
    sources:
    -   filename: bmw-films-ticker-1280x720.mp4
        format: mp4
        height: '720'
        size: 134681649
        width: '1280'
    -   filename: bmw-films-ticker-960x540.mp4
        format: mp4
        height: '540'
        size: 90658929
        width: '960'
    -   filename: bmw-films-ticker-640x360.ogv
        format: ogv
        height: '360'
        size: 61509012
        width: '640'
    -   filename: bmw-films-ticker-640x360.mp4
        format: mp4
        height: '360'
        size: 58107018
        width: '640'
    -   filename: bmw-films-ticker-640x360.webm
        format: webm
        height: '360'
        size: 43814967
        width: '640'
    -   filename: bmw-films-ticker-480x270.mp4
        format: mp4
        height: '270'
        size: 38429832
        width: '480'
    title: Bmw Films Ticker
credits:
-   companyName: BMW
    companySlug: bmw
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: RSA Films
    companySlug: rsa-films
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Joe Carnahan
    personSlug: joe-carnahan
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Mitch Drain
    personSlug: mitch-drain
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: inferno-artist
    roleTitle: Inferno Artist
    visible: false
---
A human organ being transported to an awaiting leader of an oppressed third
world country is the basis of the story of this short film/commercial. As the
human organ is transported, opponents of this great leader constantly try to
kill the delivery men who are driving the new BMW Z3.The BMW Films series is
an amazing venture by numerous leading live action directors to create short
films instead of commercials but still maintain a commercial marketability.
This was a challenging job for many of reasons, first of which was the film
finish which instantly adds a layer of complexity with it's larger image
resolution. The second challenge was to design and create a timer/thermometer
which appears throughout the commercial. I worked closely with two motion
graphic designers to create an animated clock. Using mathematical expressions
to control the time/temperature based on an animated scale, I developed an
effect that was reusable for the more than 30 shots throughout the film. It
was made difficult by the hand held camera motion and the dirt and scratches
that obscured the digital face. Our third big challenge was to create
realistic gunfire throughout. All of this was done entirely in Inferno using
generated and shot elements, tracked in 3D, to allow for realistic depth in
the motion of the 2D elements. Other visual effects included smoke
enhancement, shell casings flying out of the guns, explosion enhancement and a
digital helicopter!