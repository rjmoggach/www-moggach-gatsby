---
title: Buick Tiger Vs History
subtitle:
slug: buick-tiger-vs-history
date: 2002-07-10
role:
headline: directed by Tony Scott, RSA Films
summary: Tiger is haunted by the ever-present Harvey Earl.
excerpt: Tiger is haunted by the ever-present Harvey Earl.
published: false
featured: false
categories:
- Advertising
tags:
- automotive
- compositing
- projection
albums:
-   cover:
    images:
    -   caption: ''
        filename: buick-tiger-vs-history.01.jpg
    -   caption: ''
        filename: buick-tiger-vs-history.02.jpg
    -   caption: ''
        filename: buick-tiger-vs-history.03.jpg
    -   caption: ''
        filename: buick-tiger-vs-history.04.jpg
    path: work/2002/buick-tiger-vs-history/images/stills
    slug: stills
    title: Stills
videos: []
credits:
-   companyName: Buick
    companySlug: buick
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: RSA Films
    companySlug: rsa-films
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Tony Scott
    personSlug: tony-scott
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Paul Cameron
    personSlug: paul-cameron
    roleSlug: dp
    roleTitle: Director of Photography
    visible: false
---
This campaign was created to convey the history of the Buick brand and one of
it's driving forces, Harvey Earl., who designed a lot of the great cars in the
company's history. His ghost along with the ghosts of golfers and cars are set
in real environments alongside Tiger Woods. It's a romantic set of commercials
with numerous stylized yet elegant digital visual effects. I had the task of
putting Tiger Woods into many of the environments using footage shot on a
green screen. His time is limited so we worked quickly to capture what we
needed on set for me to combine later digitally. I also had to create a unique
look for the ghosts in all the commercials that wasn't too stereotypical and
had an elegance inherent in the story. The resulting light based solution
proved to be the right solution for the job.