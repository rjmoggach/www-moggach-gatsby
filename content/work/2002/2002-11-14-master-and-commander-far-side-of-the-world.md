---
title: Master And Commander Far Side Of The World
subtitle:
slug: master-and-commander-far-side-of-the-world
date: 2002-11-14
role:
headline: directed by Peter Weir, 20th Century Fox
summary:
excerpt:
published: false
featured: false
categories:
- Feature Films
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: master-and-commander-far-side-of-the-world.01.jpg
    path: work/2002/master-and-commander-far-side-of-the-world/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2002/master-and-commander-far-side-of-the-world/videos/master-and-commander-far-side-of-the-world.jpg
    path: work/2002/master-and-commander-far-side-of-the-world/videos/master-and-commander-far-side-of-the-world
    slug: master-and-commander-far-side-of-the-world
    sources:
    -   filename: master-and-commander-far-side-of-the-world-1280x720.mp4
        format: mp4
        height: '720'
        size: 2763939
        width: '1280'
    -   filename: master-and-commander-far-side-of-the-world-960x540.mp4
        format: mp4
        height: '540'
        size: 2044122
        width: '960'
    -   filename: master-and-commander-far-side-of-the-world-640x360.mp4
        format: mp4
        height: '360'
        size: 1346824
        width: '640'
    -   filename: master-and-commander-far-side-of-the-world-640x360.ogv
        format: ogv
        height: '360'
        size: 1191813
        width: '640'
    -   filename: master-and-commander-far-side-of-the-world-480x270.mp4
        format: mp4
        height: '270'
        size: 1029217
        width: '480'
    -   filename: master-and-commander-far-side-of-the-world-640x360.webm
        format: webm
        height: '360'
        size: 849434
        width: '640'
    title: Master And Commander Far Side Of The World
credits:
-   companyName: 20th Century Fox
    companySlug: 20th-century-fox
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Peter Weir
    personSlug: peter-weir
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: lead flame
    roleTitle: Lead Flame
    visible: false
---
