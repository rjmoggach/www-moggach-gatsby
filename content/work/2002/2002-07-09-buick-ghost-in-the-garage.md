---
title: Buick Ghost In The Garage
subtitle:
slug: buick-ghost-in-the-garage
date: 2002-07-09
role:
headline: directed by Tony Scott, RSA Films
summary:
excerpt:
published: false
featured: false
categories:
- Advertising
tags:
- automotive
- compositing
albums:
-   cover:
    images:
    -   caption: ''
        filename: buick-ghost-in-the-garage.01.jpg
    -   caption: ''
        filename: buick-ghost-in-the-garage.02.jpg
    -   caption: ''
        filename: buick-ghost-in-the-garage.03.jpg
    -   caption: ''
        filename: buick-ghost-in-the-garage.04.jpg
    path: work/2002/buick-ghost-in-the-garage/images/stills
    slug: stills
    title: Stills
videos: []
credits:
-   companyName: Buick
    companySlug: buick
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: RSA Films
    companySlug: rsa-films
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Tony Scott
    personSlug: tony-scott
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Paul Cameron
    personSlug: paul-cameron
    roleSlug: dp
    roleTitle: Director of Photography
    visible: false
---
