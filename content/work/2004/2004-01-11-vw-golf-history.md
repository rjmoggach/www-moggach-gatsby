---
title: Vw Golf History
subtitle:
slug: vw-golf-history
date: 2004-01-01
role:
headline: directed by Chris Palmer, Gorgeous
summary:
excerpt:
published: false
featured: false
categories: []
tags:
- automotive
albums:
-   cover:
    images:
    -   caption: ''
        filename: vw-golf-history.01.jpg
    -   caption: ''
        filename: vw-golf-history.02.jpg
    -   caption: ''
        filename: vw-golf-history.03.jpg
    -   caption: ''
        filename: vw-golf-history.04.jpg
    -   caption: ''
        filename: vw-golf-history.05.jpg
    -   caption: ''
        filename: vw-golf-history.06.jpg
    -   caption: ''
        filename: vw-golf-history.07.jpg
    path: work/2004/vw-golf-history/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2004/vw-golf-history/videos/vw-golf-history.jpg
    path: work/2004/vw-golf-history/videos/vw-golf-history
    slug: vw-golf-history
    sources:
    -   filename: vw-golf-history-1280x720.mp4
        format: mp4
        height: '720'
        size: 20926747
        width: '1280'
    -   filename: vw-golf-history-960x540.mp4
        format: mp4
        height: '540'
        size: 19596919
        width: '960'
    -   filename: vw-golf-history-640x360.ogv
        format: ogv
        height: '360'
        size: 10531198
        width: '640'
    -   filename: vw-golf-history-640x360.mp4
        format: mp4
        height: '360'
        size: 10390911
        width: '640'
    -   filename: vw-golf-history-640x360.webm
        format: webm
        height: '360'
        size: 8637381
        width: '640'
    -   filename: vw-golf-history-480x270.mp4
        format: mp4
        height: '270'
        size: 6162052
        width: '480'
    title: Vw Golf History
credits:
-   companyName: Volkswagen
    companySlug: volkswagen
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Gorgeous
    companySlug: gorgeous
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Chris Palmer
    personSlug: chris-palmer
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Play Productions
    companySlug: play
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Tom Sparks
    personSlug: tom-sparks
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: inferno-artist
    roleTitle: Inferno Artist
    visible: false
---
