---
title: The Sims 2 Little People
subtitle:
slug: the-sims-2-little-people
date: 2004-11-02
role:
headline: directed by ACNE
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: the-sims-2-little-people.01.jpg
    -   caption: ''
        filename: the-sims-2-little-people.02.jpg
    -   caption: ''
        filename: the-sims-2-little-people.03.jpg
    -   caption: ''
        filename: the-sims-2-little-people.04.jpg
    path: work/2004/the-sims-2-little-people/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2004/the-sims-2-little-people/videos/the-sims-2-little-people.jpg
    path: work/2004/the-sims-2-little-people/videos/the-sims-2-little-people
    slug: the-sims-2-little-people
    sources:
    -   filename: the-sims-2-little-people.1280x960.mp4
        format: mp4
        height: '960'
        size: 12926053
        width: '1280'
    -   filename: the-sims-2-little-people.960x720.mp4
        format: mp4
        height: '720'
        size: 7621403
        width: '960'
    -   filename: the-sims-2-little-people.640x360.ogv
        format: ogv
        height: '360'
        size: 5759612
        width: '640'
    -   filename: the-sims-2-little-people.640x480.mp4
        format: mp4
        height: '480'
        size: 5573970
        width: '640'
    -   filename: the-sims-2-little-people.640x360.webm
        format: webm
        height: '360'
        size: 3633930
        width: '640'
    -   filename: the-sims-2-little-people.480x360.mp4
        format: mp4
        height: '360'
        size: 3582623
        width: '480'
    title: The Sims 2 Little People
credits:
-   companyName: ACNE Collective
    companySlug: acne-collective
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
