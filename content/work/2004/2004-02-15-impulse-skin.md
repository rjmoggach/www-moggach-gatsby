---
title: Impulse Skin
subtitle:
slug: impulse-skin
date: 2004-02-15
role:
headline: directed by Steve Hudson, Outsider
summary: Women without their bodies feel and seem incomplete.
excerpt: Women without their bodies feel and seem incomplete.
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: impulse-skin.01.jpg
    -   caption: ''
        filename: impulse-skin.02.jpg
    -   caption: ''
        filename: impulse-skin.03.jpg
    -   caption: ''
        filename: impulse-skin.04.jpg
    -   caption: ''
        filename: impulse-skin.05.jpg
    path: work/2004/impulse-skin/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2004/impulse-skin/videos/impulse-skin.jpg
    path: work/2004/impulse-skin/videos/impulse-skin
    slug: impulse-skin
    sources:
    -   filename: impulse-skin-1280x720.mp4
        format: mp4
        height: '720'
        size: 14302127
        width: '1280'
    -   filename: impulse-skin-960x540.mp4
        format: mp4
        height: '540'
        size: 12096146
        width: '960'
    -   filename: impulse-skin-640x360.mp4
        format: mp4
        height: '360'
        size: 7238040
        width: '640'
    -   filename: impulse-skin-640x360.ogv
        format: ogv
        height: '360'
        size: 7098826
        width: '640'
    -   filename: impulse-skin-640x360.webm
        format: webm
        height: '360'
        size: 6060796
        width: '640'
    -   filename: impulse-skin-480x270.mp4
        format: mp4
        height: '270'
        size: 4980775
        width: '480'
    title: Impulse Skin
credits:
-   companyName: Impulse
    companySlug: impulse
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Outsider
    companySlug: outsider
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Steve Hudson
    personSlug: steve-hudson
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Play Productions
    companySlug: play
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Tom Sparks
    personSlug: tom-sparks
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: inferno-artist
    roleTitle: Inferno Artist
    visible: false
---
Women without their bodies feel and seem incomplete.