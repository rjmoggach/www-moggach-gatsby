---
title: Garbage Androgyny
subtitle:
slug: garbage-androgyny
date: 2001-08-01
role:
headline: directed by Don Cameron
summary: Uber-stylish metrosexuals in full androgynous glory.
excerpt: Uber-stylish metrosexuals in full androgynous glory.
published: false
featured: false
categories:
- Music Videos
tags:
- compositing
- design
- motion control
- matte painting
albums:
-   cover:
    images:
    -   caption: ''
        filename: garbage-androgyny.01.jpg
    -   caption: ''
        filename: garbage-androgyny.02.jpg
    -   caption: ''
        filename: garbage-androgyny.03.jpg
    -   caption: ''
        filename: garbage-androgyny.04.jpg
    -   caption: ''
        filename: garbage-androgyny.05.jpg
    -   caption: ''
        filename: garbage-androgyny.06.jpg
    -   caption: ''
        filename: garbage-androgyny.07.jpg
    path: work/2001/garbage-androgyny/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2001/garbage-androgyny/videos/garbage-androgyny.jpg
    path: work/2001/garbage-androgyny/videos/garbage-androgyny
    slug: garbage-androgyny
    sources:
    -   filename: garbage-androgyny.1280x720.mp4
        format: mp4
        height: '720'
        size: 44704847
        width: '1280'
    -   filename: garbage-androgyny.960x540.mp4
        format: mp4
        height: '540'
        size: 29828852
        width: '960'
    -   filename: garbage-androgyny.640x360.mp4
        format: mp4
        height: '360'
        size: 19289592
        width: '640'
    -   filename: garbage-androgyny.640x360.ogv
        format: ogv
        height: '360'
        size: 18657862
        width: '640'
    -   filename: garbage-androgyny.640x360.webm
        format: webm
        height: '360'
        size: 13151514
        width: '640'
    -   filename: garbage-androgyny.480x270.mp4
        format: mp4
        height: '270'
        size: 12281383
        width: '480'
    title: Garbage Androgyny
credits:
-   companyName: Method Films
    companySlug: method
    roleSlug: production
    roleTitle: Production Company
    visible: true
-   personName: Donald Cameron
    personSlug: donald-cameron
    roleSlug: director
    roleTitle: Director
    visible: true
-   companyName: Smoke & Mirrors
    companySlug: smoke-mirrors
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: true
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: true
---
This music video, made for the first single on garbage's new album, involved
the design of four completely different and individual effects. I was involved
with three. (Judy Roberts created a matte intensive treatment for another
section of the video) First was the task of making Shirley Manson look like a
classic Balinese girl painting using matte painted elements and green screen
footage. To create the porcelain like skin that is characteristic of these
beautiful painting and the stylised hair that is made up of quick brush
strokes was the hardest task. Her footage was heavily treated and combined
with multiple layers of painted elements to create this proprietary look.
Second was creating a stylized backdrop and environment for a limousine
sequence which involved multiple motion control passes and painted / keyed
elements. The car, shot in a studio, had to look like it was on this stylised
freeway. It also had to keep it's highly polished and rich aesthetic. Motion
control data was converted to a flame camera move to ease the process of
removing the reflections of the set and creating reflections of the street
lights and cityscape. Third was the creation of a city backdrop for Shirley's
final performance which again used multiple layers of matte painted elements
and heavily treated footage of shirley.