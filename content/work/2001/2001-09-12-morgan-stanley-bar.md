---
title: Morgan Stanley Bar
subtitle:
slug: morgan-stanley-bar
date: 2001-09-01
role:
headline: directed by Gerard De Thame, GDT Films
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: morgan-stanley-bar.01.jpg
    -   caption: ''
        filename: morgan-stanley-bar.02.jpg
    -   caption: ''
        filename: morgan-stanley-bar.03.jpg
    path: work/2001/morgan-stanley-bar/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2001/morgan-stanley-bar/videos/morgan-stanley-bar.jpg
    path: work/2001/morgan-stanley-bar/videos/morgan-stanley-bar
    slug: morgan-stanley-bar
    sources:
    -   filename: morgan-stanley-bar.1280x720.mp4
        format: mp4
        height: '720'
        size: 6547715
        width: '1280'
    -   filename: morgan-stanley-bar.960x540.mp4
        format: mp4
        height: '540'
        size: 4243637
        width: '960'
    -   filename: morgan-stanley-bar.640x360.ogv
        format: ogv
        height: '360'
        size: 2343034
        width: '640'
    -   filename: morgan-stanley-bar.640x360.mp4
        format: mp4
        height: '360'
        size: 2299618
        width: '640'
    -   filename: morgan-stanley-bar.640x360.webm
        format: webm
        height: '360'
        size: 1832617
        width: '640'
    -   filename: morgan-stanley-bar.480x270.mp4
        format: mp4
        height: '270'
        size: 1475067
        width: '480'
    title: Morgan Stanley Bar
credits:
-   companyName: Morgan Stanley
    companySlug: morgan-stanley
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: GDT Films
    companySlug: gdt-films
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Gerard de Thame
    personSlug: gerard-de-thame
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Smoke & Mirrors
    companySlug: smoke-mirrors
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: inferno-artist
    roleTitle: Inferno Artist
    visible: false
---
