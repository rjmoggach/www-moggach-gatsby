---
title: Morgan Stanley Race
subtitle:
slug: morgan-stanley-race
date: 2001-09-01
role:
headline: directed by Gerard De Thame, GDT Films
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: morgan-stanley-race.01.jpg
    -   caption: ''
        filename: morgan-stanley-race.02.jpg
    -   caption: ''
        filename: morgan-stanley-race.03.jpg
    -   caption: ''
        filename: morgan-stanley-race.04.jpg
    path: work/2001/morgan-stanley-race/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2001/morgan-stanley-race/videos/morgan-stanley-race.jpg
    path: work/2001/morgan-stanley-race/videos/morgan-stanley-race
    slug: morgan-stanley-race
    sources:
    -   filename: morgan-stanley-race.1280x720.mp4
        format: mp4
        height: '720'
        size: 6956404
        width: '1280'
    -   filename: morgan-stanley-race.960x540.mp4
        format: mp4
        height: '540'
        size: 6583590
        width: '960'
    -   filename: morgan-stanley-race.640x360.mp4
        format: mp4
        height: '360'
        size: 3571176
        width: '640'
    -   filename: morgan-stanley-race.640x360.ogv
        format: ogv
        height: '360'
        size: 3539042
        width: '640'
    -   filename: morgan-stanley-race.640x360.webm
        format: webm
        height: '360'
        size: 3260306
        width: '640'
    -   filename: morgan-stanley-race.480x270.mp4
        format: mp4
        height: '270'
        size: 2174500
        width: '480'
    title: Morgan Stanley Race
credits:
-   companyName: Morgan Stanley
    companySlug: morgan-stanley
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: GDT Films
    companySlug: gdt-films
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Gerard de Thame
    personSlug: gerard-de-thame
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Smoke & Mirrors
    companySlug: smoke-mirrors
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: inferno-artist
    roleTitle: Inferno Artist
    visible: false
---
