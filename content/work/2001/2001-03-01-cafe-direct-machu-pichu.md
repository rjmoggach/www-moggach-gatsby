---
title: Cafe Direct "Machu Pichu"
subtitle:
slug: cafe-direct-machu-pichu
date: 2001-03-01
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories:
- Advertising
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: cafe-direct-machu-pichu.01.jpg
    -   caption: ''
        filename: cafe-direct-machu-pichu.02.jpg
    -   caption: ''
        filename: cafe-direct-machu-pichu.03.jpg
    path: work/2001/cafe-direct-machu-pichu/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2001/cafe-direct-machu-pichu/videos/cafe-direct-machu-pichu.jpg
    path: work/2001/cafe-direct-machu-pichu/videos/cafe-direct-machu-pichu
    slug: cafe-direct-machu-pichu
    sources:
    -   filename: cafe-direct-machu-pichu.1280x720.mp4
        format: mp4
        height: '720'
        size: 8595304
        width: '1280'
    -   filename: cafe-direct-machu-pichu.960x540.mp4
        format: mp4
        height: '540'
        size: 5598951
        width: '960'
    -   filename: cafe-direct-machu-pichu.640x360.mp4
        format: mp4
        height: '360'
        size: 3179440
        width: '640'
    -   filename: cafe-direct-machu-pichu.640x360.ogv
        format: ogv
        height: '360'
        size: 2262399
        width: '640'
    -   filename: cafe-direct-machu-pichu.640x360.webm
        format: webm
        height: '360'
        size: 1777540
        width: '640'
    -   filename: cafe-direct-machu-pichu.480x270.mp4
        format: mp4
        height: '270'
        size: 1728728
        width: '480'
    title: Cafe Direct Machu Pichu
credits: []
---
