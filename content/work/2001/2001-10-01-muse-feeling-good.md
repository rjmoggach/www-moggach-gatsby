---
title: Muse Feeling Good
subtitle:
slug: muse-feeling-good
date: 2001-10-01
role:
headline: directed by David Slade, Bullett
summary:
excerpt:
published: false
featured: false
categories:
- Music Videos
tags:
- facial warping
- compositing
- tracking
albums:
-   cover:
    images:
    -   caption: ''
        filename: muse-feeling-good.01.jpg
    -   caption: ''
        filename: muse-feeling-good.02.jpg
    -   caption: ''
        filename: muse-feeling-good.03.jpg
    -   caption: ''
        filename: muse-feeling-good.04.jpg
    -   caption: ''
        filename: muse-feeling-good.05.jpg
    -   caption: ''
        filename: muse-feeling-good.06.jpg
    -   caption: ''
        filename: muse-feeling-good.07.jpg
    -   caption: ''
        filename: muse-feeling-good.08.jpg
    -   caption: ''
        filename: muse-feeling-good.09.jpg
    path: work/2001/muse-feeling-good/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2001/muse-feeling-good/videos/muse-feeling-good.jpg
    path: work/2001/muse-feeling-good/videos/muse-feeling-good
    slug: muse-feeling-good
    sources:
    -   filename: muse-feeling-good-640x480.mp4
        format: mp4
        height: '480'
        size: 33648770
        width: '640'
    title: Muse Feeling Good
credits:
-   companyName: Bullett
    companySlug: bullett
    roleSlug: production
    roleTitle: Production Company
    visible: true
-   personName: David Slade
    personSlug: david-slade
    roleSlug: director
    roleTitle: Director
    visible: true
-   companyName: Smoke & Mirrors
    companySlug: smoke-mirrors
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: true
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: true
---
