---
title: National Rail Time To Return
subtitle:
slug: national-rail-time-to-return
date: 2001-05-20
role:
headline: ''
summary: Beautiful images coax travellers back to train travel.
excerpt: Beautiful images coax travellers back to train travel.
published: false
featured: false
categories:
- Advertising
tags:
- compositing
- projection
- tracking
albums:
-   cover:
    images:
    -   caption: ''
        filename: national-rail-time-to-return.01.jpg
    -   caption: ''
        filename: national-rail-time-to-return.02.jpg
    -   caption: ''
        filename: national-rail-time-to-return.03.jpg
    -   caption: ''
        filename: national-rail-time-to-return.04.jpg
    -   caption: ''
        filename: national-rail-time-to-return.05.jpg
    -   caption: ''
        filename: national-rail-time-to-return.06.jpg
    -   caption: ''
        filename: national-rail-time-to-return.07.jpg
    -   caption: ''
        filename: national-rail-time-to-return.08.jpg
    -   caption: ''
        filename: national-rail-time-to-return.09.jpg
    -   caption: ''
        filename: national-rail-time-to-return.10.jpg
    path: work/2001/national-rail-time-to-return/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2001/national-rail-time-to-return/videos/national-rail-time-to-return.jpg
    path: work/2001/national-rail-time-to-return/videos/national-rail-time-to-return
    slug: national-rail-time-to-return
    sources:
    -   filename: national-rail-time-to-return.1280x720.mp4
        format: mp4
        height: '720'
        size: 13425933
        width: '1280'
    -   filename: national-rail-time-to-return.960x540.mp4
        format: mp4
        height: '540'
        size: 13391063
        width: '960'
    -   filename: national-rail-time-to-return.640x360.ogv
        format: ogv
        height: '360'
        size: 6548361
        width: '640'
    -   filename: national-rail-time-to-return.640x360.webm
        format: webm
        height: '360'
        size: 6525552
        width: '640'
    -   filename: national-rail-time-to-return.640x360.mp4
        format: mp4
        height: '360'
        size: 6505809
        width: '640'
    -   filename: national-rail-time-to-return.480x270.mp4
        format: mp4
        height: '270'
        size: 5254229
        width: '480'
    title: National Rail Time To Return
credits:
-   companyName: Smoke & Mirrors
    companySlug: smoke-mirrors
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: true
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: flame
    roleTitle: Flame Artist
    visible: true
---
Despite winning broadcast magazine's pig of the week award for the ad's
concept, this commercial was beautifully shot. The body of the commercial
involved primarily cosmetic fixes (some far more complex than others) and
intelligent conformimg of what were at times 10 or more mixing and dissolving
layers. Actors were also composited into real settings using footage shot
against green screen. The bulk of the post time was spent creating the end
sequence which again was a complicated conform more traditionally suited to
henry but completed in inferno with careful management of edit layers and more
powerful matting and compositing tools.