---
title: Gatorade Big Heads
subtitle:
slug: gatorade-big-heads
date: 2006-05-01
role:
headline: directed by Dante Ariola, MJZ
summary: Kids with pro-athlete heads taunt each other on.
excerpt: Kids with pro-athlete heads taunt each other on.
published: false
featured: false
categories:
- Advertising
tags:
- compositing
- drinks
- tracking
- facial warping
albums:
-   cover:
    images:
    -   caption: ''
        filename: gatorade-big-heads.01.jpg
    -   caption: ''
        filename: gatorade-big-heads.02.jpg
    -   caption: ''
        filename: gatorade-big-heads.03.jpg
    -   caption: ''
        filename: gatorade-big-heads.04.jpg
    -   caption: ''
        filename: gatorade-big-heads.05.jpg
    -   caption: ''
        filename: gatorade-big-heads.06.jpg
    -   caption: ''
        filename: gatorade-big-heads.07.jpg
    -   caption: ''
        filename: gatorade-big-heads.08.jpg
    -   caption: ''
        filename: gatorade-big-heads.09.jpg
    -   caption: ''
        filename: gatorade-big-heads.10.jpg
    path: work/2006/gatorade-big-heads/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2006/gatorade-big-heads/videos/gatorade-big-heads.jpg
    path: work/2006/gatorade-big-heads/videos/gatorade-big-heads
    slug: gatorade-big-heads
    sources:
    -   filename: gatorade-big-heads-1280x720.mp4
        format: mp4
        height: '720'
        size: 10367613
        width: '1280'
    -   filename: gatorade-big-heads-960x540.mp4
        format: mp4
        height: '540'
        size: 6987382
        width: '960'
    -   filename: gatorade-big-heads-640x360.ogv
        format: ogv
        height: '360'
        size: 4615233
        width: '640'
    -   filename: gatorade-big-heads-640x360.mp4
        format: mp4
        height: '360'
        size: 4477061
        width: '640'
    -   filename: gatorade-big-heads-640x360.webm
        format: webm
        height: '360'
        size: 3439243
        width: '640'
    -   filename: gatorade-big-heads-480x270.mp4
        format: mp4
        height: '270'
        size: 2955999
        width: '480'
    title: Gatorade Big Heads
credits:
-   companyName: Gatorade
    companySlug: gatorade
    roleSlug: client
    roleTitle: Client
    visible: true
-   companyName: MJZ
    companySlug: mjz
    roleSlug: production
    roleTitle: Production Company
    visible: true
-   personName: Dante Ariola
    personSlug: dante-ariol
    roleSlug: director
    roleTitle: Director
    visible: true
-   personName: Mitch Drain
    personSlug: mitch-drain
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: true
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: inferno-artist
    roleTitle: Inferno Artist
    visible: true
-   personName: Tim Davies
    personSlug: tim-davies
    roleSlug: inferno-artist
    roleTitle: Inferno Artist
    visible: true
---
Kids with pro-athlete heads taunt each other on.