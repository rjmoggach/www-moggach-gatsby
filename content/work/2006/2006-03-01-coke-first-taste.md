---
title: Coke First Taste
subtitle:
slug: coke-first-taste
date: 2006-03-01
role:
headline: directed by Dante Ariola, MJZ
summary: Elderly man has first taste of Coke and revisits his life's missedopportunities.
excerpt: Elderly man has first taste of Coke and revisits his life's missedopportunities.
published: false
featured: false
categories:
- Advertising
tags:
- compositing
- environment
- set extensions
- tracking
- stock
albums:
-   cover:
    images:
    -   caption: ''
        filename: coke-first-taste.01.jpg
    -   caption: ''
        filename: coke-first-taste.02.jpg
    -   caption: ''
        filename: coke-first-taste.03.jpg
    -   caption: ''
        filename: coke-first-taste.04.jpg
    -   caption: ''
        filename: coke-first-taste.05.jpg
    -   caption: ''
        filename: coke-first-taste.06.jpg
    -   caption: ''
        filename: coke-first-taste.07.jpg
    -   caption: ''
        filename: coke-first-taste.08.jpg
    path: work/2006/coke-first-taste/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2006/coke-first-taste/videos/coke-first-taste.jpg
    path: work/2006/coke-first-taste/videos/coke-first-taste
    slug: coke-first-taste
    sources:
    -   filename: coke-first-taste-1280x720.mp4
        format: mp4
        height: '720'
        size: 18120448
        width: '1280'
    -   filename: coke-first-taste-960x540.mp4
        format: mp4
        height: '540'
        size: 11809580
        width: '960'
    -   filename: coke-first-taste-640x360.ogv
        format: ogv
        height: '360'
        size: 8153117
        width: '640'
    -   filename: coke-first-taste-640x360.mp4
        format: mp4
        height: '360'
        size: 7754465
        width: '640'
    -   filename: coke-first-taste-640x360.webm
        format: webm
        height: '360'
        size: 5605047
        width: '640'
    -   filename: coke-first-taste-480x270.mp4
        format: mp4
        height: '270'
        size: 5352208
        width: '480'
    title: Coke First Taste
credits:
-   companyName: Coke
    companySlug: coke
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Wieden + Kennedy
    companySlug: wk
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: false
-   companyName: MJZ
    companySlug: mjz
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Dante Ariola
    personSlug: dante-ariol
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
Elderly man has first taste of Coke and revisits his life's missed
opportunities.