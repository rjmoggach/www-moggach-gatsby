---
title: Coke Parade
subtitle:
slug: coke-parade
date: 2006-04-02
role:
headline: directed by Dante Ariola, MJZ
summary:
excerpt:
published: false
featured: false
categories:
- Advertising
tags:
- VFX
- compositing
- photoreal
- drinks
albums:
-   cover:
    images:
    -   caption: ''
        filename: coke-parade.01.jpg
    -   caption: ''
        filename: coke-parade.02.jpg
    -   caption: ''
        filename: coke-parade.03.jpg
    -   caption: ''
        filename: coke-parade.04.jpg
    -   caption: ''
        filename: coke-parade.05.jpg
    -   caption: ''
        filename: coke-parade.06.jpg
    path: work/2006/coke-parade/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2006/coke-parade/videos/coke-parade.jpg
    path: work/2006/coke-parade/videos/coke-parade
    slug: coke-parade
    sources:
    -   filename: coke-parade-1280x720.mp4
        format: mp4
        height: '720'
        size: 19552890
        width: '1280'
    -   filename: coke-parade-960x540.mp4
        format: mp4
        height: '540'
        size: 13902342
        width: '960'
    -   filename: coke-parade-640x360.ogv
        format: ogv
        height: '360'
        size: 10382913
        width: '640'
    -   filename: coke-parade-640x360.mp4
        format: mp4
        height: '360'
        size: 9142062
        width: '640'
    -   filename: coke-parade-640x360.webm
        format: webm
        height: '360'
        size: 8057282
        width: '640'
    -   filename: coke-parade-480x270.mp4
        format: mp4
        height: '270'
        size: 6356678
        width: '480'
    title: Coke Parade
credits:
-   companyName: Coke
    companySlug: coke
    roleSlug: client
    roleTitle: Client
    visible: true
-   companyName: Wieden + Kennedy
    companySlug: wk
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: true
-   companyName: MJZ
    companySlug: mjz
    roleSlug: production
    roleTitle: Production Company
    visible: true
-   personName: Dante Ariola
    personSlug: dante-ariol
    roleSlug: director
    roleTitle: Director
    visible: true
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: true
-   personName: Mitch Drain
    personSlug: mitch-drain
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: true
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: inferno-artist
    roleTitle: Inferno Artist
    visible: true
---
