---
title: Pirates Of The Caribbean | "Dead Man's Chest"
subtitle: "Dead Man's Chest"
slug: pirates-of-the-caribbean-dead-mans-chest
date: 2006-07-07
role:
headline: directed by Gore Verbinski, Walt Disney Pictures
summary:
excerpt:
published: true
featured: false
categories:
    - Feature Films
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: pirates-of-the-caribbean-dead-mans-chest.01.jpg
          - caption: ''
            filename: pirates-of-the-caribbean-dead-mans-chest.02.jpg
          - caption: ''
            filename: pirates-of-the-caribbean-dead-mans-chest.03.jpg
      path: work/2006/pirates-of-the-caribbean-dead-mans-chest/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2006/pirates-of-the-caribbean-dead-mans-chest/videos/pirates-of-the-caribbean-dead-mans-chest.jpg
      path: work/2006/pirates-of-the-caribbean-dead-mans-chest/videos/pirates-of-the-caribbean-dead-mans-chest
      slug: pirates-of-the-caribbean-dead-mans-chest
      sources:
          - filename: pirates-of-the-caribbean-dead-mans-chest-1280x720.mp4
            format: mp4
            height: '720'
            size: 12087659
            width: '1280'
          - filename: pirates-of-the-caribbean-dead-mans-chest-960x540.mp4
            format: mp4
            height: '540'
            size: 10360431
            width: '960'
          - filename: pirates-of-the-caribbean-dead-mans-chest-640x360.mp4
            format: mp4
            height: '360'
            size: 6119342
            width: '640'
          - filename: pirates-of-the-caribbean-dead-mans-chest-640x360.ogv
            format: ogv
            height: '360'
            size: 6061065
            width: '640'
          - filename: pirates-of-the-caribbean-dead-mans-chest-640x360.webm
            format: webm
            height: '360'
            size: 6033276
            width: '640'
          - filename: pirates-of-the-caribbean-dead-mans-chest-480x270.mp4
            format: mp4
            height: '270'
            size: 4752648
            width: '480'
      title: Pirates Of The Caribbean Dead Mans Chest
credits:
    - companyName: Walt Disney Pictures
      companySlug: walt-disney-pictures
      roleSlug: production
      roleTitle: Production Company
      visible: false
    - personName: Gore Verbinski
      personSlug: gore-verbinski
      roleSlug: director
      roleTitle: Director
      visible: false
    - personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: lead flame
      roleTitle: Lead Flame
      visible: false
---
