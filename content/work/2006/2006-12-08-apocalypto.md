---
title: Apocalypto
subtitle:
slug: apocalypto
date: 2006-12-08
role:
headline: directed by Mel Gibson, Touchstone Pictures
summary:
excerpt:
published: false
featured: false
categories:
- Feature Films
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: apocalypto.01.jpg
    -   caption: ''
        filename: apocalypto.02.jpg
    -   caption: ''
        filename: apocalypto.03.jpg
    path: work/2006/apocalypto/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2006/apocalypto/videos/apocalypto.jpg
    path: work/2006/apocalypto/videos/apocalypto
    slug: apocalypto
    sources:
    -   filename: apocalypto-960x540.mp4
        format: mp4
        height: '540'
        size: 13545531
        width: '960'
    -   filename: apocalypto-1280x720.mp4
        format: mp4
        height: '720'
        size: 12796995
        width: '1280'
    -   filename: apocalypto-640x360.mp4
        format: mp4
        height: '360'
        size: 6459076
        width: '640'
    -   filename: apocalypto-640x360.ogv
        format: ogv
        height: '360'
        size: 6423622
        width: '640'
    -   filename: apocalypto-640x360.webm
        format: webm
        height: '360'
        size: 6361672
        width: '640'
    -   filename: apocalypto-480x270.mp4
        format: mp4
        height: '270'
        size: 5912812
        width: '480'
    title: Apocalypto
credits:
-   companyName: Touchstone Pictures
    companySlug: touchstone-pictures
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Mel Gibson
    personSlug: mel-gibson
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: lead flame
    roleTitle: Lead Flame
    visible: false
---
