---
title: Chick Fil A Parachute
subtitle:
slug: chick-fil-a-parachute
date: 2007-06-02
role:
headline: directed by Gerard De Thame, HSI
summary: Cows descend on the Rose Bowl by parachute to protest eating beef.
excerpt: Cows descend on the Rose Bowl by parachute to protest eating beef.
published: false
featured: false
categories:
- Advertising
tags:
- animation
- characters
- animals
- CG
- compositing
- tracking
- cloth
- crowds
albums:
-   cover:
    images:
    -   caption: ''
        filename: chick-fil-a-parachute.01.jpg
    -   caption: ''
        filename: chick-fil-a-parachute.02.jpg
    -   caption: ''
        filename: chick-fil-a-parachute.03.jpg
    -   caption: ''
        filename: chick-fil-a-parachute.04.jpg
    -   caption: ''
        filename: chick-fil-a-parachute.05.jpg
    path: work/2007/chick-fil-a-parachute/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2007/chick-fil-a-parachute/videos/chick-fil-a-parachute.jpg
    path: work/2007/chick-fil-a-parachute/videos/chick-fil-a-parachute
    slug: chick-fil-a-parachute
    sources:
    -   filename: chick-fil-a-parachute-1280x720.mp4
        format: mp4
        height: '720'
        size: 21032209
        width: '1280'
    -   filename: chick-fil-a-parachute-960x540.mp4
        format: mp4
        height: '540'
        size: 18468477
        width: '960'
    -   filename: chick-fil-a-parachute-640x360.mp4
        format: mp4
        height: '360'
        size: 10676653
        width: '640'
    -   filename: chick-fil-a-parachute-640x360.ogv
        format: ogv
        height: '360'
        size: 10464394
        width: '640'
    -   filename: chick-fil-a-parachute-640x360.webm
        format: webm
        height: '360'
        size: 10110045
        width: '640'
    -   filename: chick-fil-a-parachute-480x270.mp4
        format: mp4
        height: '270'
        size: 8914437
        width: '480'
    title: Chick Fil A Parachute
credits:
-   companyName: Chick-Fil-A
    companySlug: chick-fil-
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: HSI Productions
    companySlug: hsi-productions
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Gerard de Thame
    personSlug: gerard-de-thame
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Sean Faden
    personSlug: sean-faden
    roleSlug: cg-sup
    roleTitle: CG Supervisor
    visible: false
---
Cows descend on the Rose Bowl by parachute to protest eating beef.