---
title: Adidas Xlarge
subtitle:
slug: adidas-xlarge
date: 2000-12-15
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: adidas-xlarge.01.jpg
    -   caption: ''
        filename: adidas-xlarge.02.jpg
    -   caption: ''
        filename: adidas-xlarge.03.jpg
    -   caption: ''
        filename: adidas-xlarge.04.jpg
    -   caption: ''
        filename: adidas-xlarge.05.jpg
    -   caption: ''
        filename: adidas-xlarge.06.jpg
    -   caption: ''
        filename: adidas-xlarge.07.jpg
    -   caption: ''
        filename: adidas-xlarge.08.jpg
    -   caption: ''
        filename: adidas-xlarge.09.jpg
    path: work/2007/adidas-xlarge/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: ''
    path: work/2007/adidas-xlarge/videos/adidas-xlarge
    slug: adidas-xlarge
    sources: []
    title: Adidas Xlarge
credits: []
---
