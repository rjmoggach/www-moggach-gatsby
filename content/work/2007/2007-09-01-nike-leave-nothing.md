---
title: Nike Leave Nothing
subtitle:
slug: nike-leave-nothing
date: 2007-09-01
role:
headline: directed by Michael Mann, Alturas Films
summary:
excerpt:
published: false
featured: false
categories:
- Advertising
tags:
- sports
- compositing
- environment
- tracking
- photogrammetry
- set extensions
- rig removal
- weather
- transitions
- crowds
albums:
-   cover:
    images:
    -   caption: ''
        filename: nike-leave-nothing.01.jpg
    -   caption: ''
        filename: nike-leave-nothing.02.jpg
    -   caption: ''
        filename: nike-leave-nothing.03.jpg
    -   caption: ''
        filename: nike-leave-nothing.04.jpg
    -   caption: ''
        filename: nike-leave-nothing.05.jpg
    path: work/2007/nike-leave-nothing/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2007/nike-leave-nothing/videos/nike-leave-nothing.jpg
    path: work/2007/nike-leave-nothing/videos/nike-leave-nothing
    slug: nike-leave-nothing
    sources:
    -   filename: nike-leave-nothing-640x480.mp4
        format: mp4
        height: '480'
        size: 12901878
        width: '640'
    title: Nike Leave Nothing
credits:
-   companyName: Nike
    companySlug: nike
    roleSlug: client
    roleTitle: Client
    visible: true
-   companyName: Wieden + Kennedy
    companySlug: wk
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: true
-   companyName: Alturas Films
    companySlug: alturas-films
    roleSlug: production
    roleTitle: Production Company
    visible: true
-   personName: Michael Mann
    personSlug: michael-mann
    roleSlug: director
    roleTitle: Director
    visible: true
-   personName: Marshall Rawlings
    personSlug: marshall-rawlings
    roleSlug: prod-ep
    roleTitle: Executive Producer
    visible: true
-   personName: Jeff Rohrer
    personSlug: jeff-rohrer
    roleSlug: line-producer
    roleTitle: Line Producer
    visible: true
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: true
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: true
-   personName: Mark Kurtz
    personSlug: mark-kurtz
    roleSlug: vfx-producer
    roleTitle: VFX Producer
    visible: true
-   personName: Rob Trent
    personSlug: rob-trent
    roleSlug: flame
    roleTitle: Flame Artist
    visible: true
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: flame
    roleTitle: Flame Artist
    visible: true
---
