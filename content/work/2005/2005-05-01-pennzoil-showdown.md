---
title: Pennzoil Showdown
subtitle:
slug: pennzoil-showdown
date: 2005-05-01
role:
headline: directed by ACNE
summary: Pennzoil takes on the competition spaghetti western style.
excerpt: Pennzoil takes on the competition spaghetti western style.
published: false
featured: false
categories:
- Advertising
tags:
- photoreal
- animation
- compositing
- automotive
- characters
albums:
-   cover:
    images:
    -   caption: ''
        filename: pennzoil-showdown.01.jpg
    -   caption: ''
        filename: pennzoil-showdown.02.jpg
    -   caption: ''
        filename: pennzoil-showdown.03.jpg
    -   caption: ''
        filename: pennzoil-showdown.04.jpg
    -   caption: ''
        filename: pennzoil-showdown.05.jpg
    path: work/2005/pennzoil-showdown/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2005/pennzoil-showdown/videos/pennzoil-showdown.jpg
    path: work/2005/pennzoil-showdown/videos/pennzoil-showdown
    slug: pennzoil-showdown
    sources:
    -   filename: pennzoil-showdown-1280x720.mp4
        format: mp4
        height: '720'
        size: 7586276
        width: '1280'
    -   filename: pennzoil-showdown-960x540.mp4
        format: mp4
        height: '540'
        size: 4877378
        width: '960'
    -   filename: pennzoil-showdown-640x360.ogv
        format: ogv
        height: '360'
        size: 3633061
        width: '640'
    -   filename: pennzoil-showdown-640x360.mp4
        format: mp4
        height: '360'
        size: 3052701
        width: '640'
    -   filename: pennzoil-showdown-640x360.webm
        format: webm
        height: '360'
        size: 2400559
        width: '640'
    -   filename: pennzoil-showdown-480x270.mp4
        format: mp4
        height: '270'
        size: 2008940
        width: '480'
    title: Pennzoil Showdown
credits:
-   companyName: RSA Films
    companySlug: rsa-films
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: 'ACNE '
    personSlug: acne
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Digital Domain
    companySlug: digital-domain
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Cris Blyth
    personSlug: cris-blyth
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: dfx-supervisor
    roleTitle: DFX Supervisor
    visible: false
---
Pennzoil takes on the competition spaghetti western style.