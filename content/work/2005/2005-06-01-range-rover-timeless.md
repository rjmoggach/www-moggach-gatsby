---
title: Range Rover Timeless
subtitle:
slug: range-rover-timeless
date: 2005-06-01
role:
headline: directed by Alain Gourrier, Digital Domain
summary: Range Rover stands the test of time.
excerpt: Range Rover stands the test of time.
published: false
featured: false
categories:
- Advertising
tags:
- automotive
- VFX
- compositing
albums:
-   cover:
    images:
    -   caption: ''
        filename: range-rover-timeless.01.jpg
    -   caption: ''
        filename: range-rover-timeless.02.jpg
    -   caption: ''
        filename: range-rover-timeless.03.jpg
    -   caption: ''
        filename: range-rover-timeless.04.jpg
    -   caption: ''
        filename: range-rover-timeless.05.jpg
    -   caption: ''
        filename: range-rover-timeless.06.jpg
    -   caption: ''
        filename: range-rover-timeless.07.jpg
    path: work/2005/range-rover-timeless/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2005/range-rover-timeless/videos/range-rover-timeless.jpg
    path: work/2005/range-rover-timeless/videos/range-rover-timeless
    slug: range-rover-timeless
    sources:
    -   filename: range-rover-timeless-960x540.mp4
        format: mp4
        height: '540'
        size: 11615753
        width: '960'
    -   filename: range-rover-timeless-1280x720.mp4
        format: mp4
        height: '720'
        size: 10460105
        width: '1280'
    -   filename: range-rover-timeless-640x360.mp4
        format: mp4
        height: '360'
        size: 5257805
        width: '640'
    -   filename: range-rover-timeless-640x360.ogv
        format: ogv
        height: '360'
        size: 5241319
        width: '640'
    -   filename: range-rover-timeless-640x360.webm
        format: webm
        height: '360'
        size: 5216972
        width: '640'
    -   filename: range-rover-timeless-480x270.mp4
        format: mp4
        height: '270'
        size: 4668711
        width: '480'
    title: Range Rover Timeless
credits:
-   companyName: Tate USA
    companySlug: tate-usa
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Alain Gourrier
    personSlug: alain-gourrier
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Digital Domain
    companySlug: digital-domain
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: dfx-supervisor
    roleTitle: DFX Supervisor
    visible: false
---
Range Rover stands the test of time.