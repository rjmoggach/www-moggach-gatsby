---
title: Stealth
subtitle:
slug: stealth
date: 2005-07-29
role:
headline: directed by Rob Cohen, Columbia Pictures
summary:
excerpt:
published: true
featured: false
categories:
    - Feature Films
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: stealth.01.jpg
          - caption: ''
            filename: stealth.02.jpg
      path: work/2005/stealth/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2005/stealth/videos/stealth.jpg
      path: work/2005/stealth/videos/stealth
      slug: stealth
      sources:
          - filename: stealth-1280x720.mp4
            format: mp4
            height: '720'
            size: 8261026
            width: '1280'
          - filename: stealth-960x540.mp4
            format: mp4
            height: '540'
            size: 6315680
            width: '960'
          - filename: stealth-640x360.ogv
            format: ogv
            height: '360'
            size: 4184738
            width: '640'
          - filename: stealth-640x360.mp4
            format: mp4
            height: '360'
            size: 4093475
            width: '640'
          - filename: stealth-640x360.webm
            format: webm
            height: '360'
            size: 3163639
            width: '640'
          - filename: stealth-480x270.mp4
            format: mp4
            height: '270'
            size: 2768725
            width: '480'
      title: Stealth
credits:
    - companyName: Columbia Pictures
      companySlug: columbia-pictures
      roleSlug: production
      roleTitle: Production Company
      visible: false
    - personName: Rob Cohen
      personSlug: rob-cohen
      roleSlug: director
      roleTitle: Director
      visible: false
    - personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: lead flame
      roleTitle: Lead Flame
      visible: false
---
