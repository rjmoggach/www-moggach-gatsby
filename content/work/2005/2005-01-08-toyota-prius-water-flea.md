---
title: Toyota Prius Water Flea
subtitle:
slug: toyota-prius-water-flea
date: 2005-01-08
role:
headline: directed by Andrew Douglas, Anonymous Content
summary: Water Fleas create gasoline over the course of thousands of years.
excerpt: Water Fleas create gasoline over the course of thousands of years.
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: toyota-prius-water-flea.01.jpg
    -   caption: ''
        filename: toyota-prius-water-flea.02.jpg
    -   caption: ''
        filename: toyota-prius-water-flea.03.jpg
    -   caption: ''
        filename: toyota-prius-water-flea.04.jpg
    -   caption: ''
        filename: toyota-prius-water-flea.05.jpg
    -   caption: ''
        filename: toyota-prius-water-flea.06.jpg
    path: work/2005/toyota-prius-water-flea/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2005/toyota-prius-water-flea/videos/toyota-prius-water-flea.jpg
    path: work/2005/toyota-prius-water-flea/videos/toyota-prius-water-flea
    slug: toyota-prius-water-flea
    sources:
    -   filename: toyota-prius-water-flea-1280x720.mp4
        format: mp4
        height: '720'
        size: 11696536
        width: '1280'
    -   filename: toyota-prius-water-flea-960x540.mp4
        format: mp4
        height: '540'
        size: 7377247
        width: '960'
    -   filename: toyota-prius-water-flea-640x360.mp4
        format: mp4
        height: '360'
        size: 5020197
        width: '640'
    -   filename: toyota-prius-water-flea-640x360.ogv
        format: ogv
        height: '360'
        size: 3377084
        width: '640'
    -   filename: toyota-prius-water-flea-480x270.mp4
        format: mp4
        height: '270'
        size: 2983677
        width: '480'
    -   filename: toyota-prius-water-flea-640x360.webm
        format: webm
        height: '360'
        size: 2514600
        width: '640'
    title: Toyota Prius Water Flea
credits:
-   companyName: Toyota
    companySlug: toyota
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Anonymous Content
    companySlug: anonymous-content
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Andrew Douglas
    personSlug: andrew-douglas
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
Water Fleas create gasoline over the course of thousands of years.