---
title: Sony Wega Arrival
subtitle:
slug: sony-wega-arrival
date: 2005-08-01
role:
headline: directed by Antony Hoffman, @radical media
summary:
excerpt:
published: false
featured: false
categories:
- Advertising
tags:
- projection
- compositing
- CG
- VFX
albums:
-   cover:
    images:
    -   caption: ''
        filename: sony-wega-arrival.01.jpg
    -   caption: ''
        filename: sony-wega-arrival.02.jpg
    -   caption: ''
        filename: sony-wega-arrival.03.jpg
    -   caption: ''
        filename: sony-wega-arrival.04.jpg
    -   caption: ''
        filename: sony-wega-arrival.05.jpg
    -   caption: ''
        filename: sony-wega-arrival.06.jpg
    path: work/2005/sony-wega-arrival/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2005/sony-wega-arrival/videos/sony-wega-arrival.jpg
    path: work/2005/sony-wega-arrival/videos/sony-wega-arrival
    slug: sony-wega-arrival
    sources:
    -   filename: sony-wega-arrival-1280x720.mp4
        format: mp4
        height: '720'
        size: 15051207
        width: '1280'
    -   filename: sony-wega-arrival-960x540.mp4
        format: mp4
        height: '540'
        size: 9955427
        width: '960'
    -   filename: sony-wega-arrival-640x360.mp4
        format: mp4
        height: '360'
        size: 6183038
        width: '640'
    -   filename: sony-wega-arrival-640x360.ogv
        format: ogv
        height: '360'
        size: 5737393
        width: '640'
    -   filename: sony-wega-arrival-640x360.webm
        format: webm
        height: '360'
        size: 4518454
        width: '640'
    -   filename: sony-wega-arrival-480x270.mp4
        format: mp4
        height: '270'
        size: 3690065
        width: '480'
    title: Sony Wega Arrival
credits:
-   companyName: Sony
    companySlug: sony
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Wieden + Kennedy
    companySlug: wk
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: false
-   companyName: '@Radical Media'
    companySlug: radical-media
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Antony Hoffman
    personSlug: antony-hoffman
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
glowing gelatinous TVs