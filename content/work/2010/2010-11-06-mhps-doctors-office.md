---
title: Mhps Doctors Office
subtitle:
slug: mhps-doctors-office
date: 2010-11-06
role:
headline: directed by Woods + Low, OPC for Bensimon Byrne
summary: When a viewer sees the face of a 12-year-old boy, sweetly warning that thedecisions
    he makes now will affect his health when he's much older - theyexpect a 12-year-old
    body to be attached. As the camera dollies out from justthis subject in "Doctor's
    Office, that prepubescent face is attached to atitanic, obese body.
excerpt: When a viewer sees the face of a 12-year-old boy, sweetly warning that thedecisions
    he makes now will affect his health when he's much older - theyexpect a 12-year-old
    body to be attached. As the camera dollies out from justthis subject in "Doctor's
    Office, that prepubescent face is attached to atitanic, obese body.
published: false
featured: false
categories:
    - Advertising
tags:
    - health
    - VFX
    - photoreal
    - compositing
albums:
    - cover:
      images:
          - caption: ''
            filename: mhps-doctors-office-en.01.jpg
          - caption: ''
            filename: mhps-doctors-office-en.02.jpg
      path: work/2010/mhps-doctors-office/images/mhps-doctors-office-en-stills
      slug: mhps-doctors-office-en-stills
      title: Mhps Doctors Office En Stills
    - cover:
      images:
          - caption: ''
            filename: mhps-doctors-office-fr.01.jpg
          - caption: ''
            filename: mhps-doctors-office-fr.02.jpg
      path: work/2010/mhps-doctors-office/images/mhps-doctors-office-fr-stills
      slug: mhps-doctors-office-fr-stills
      title: Mhps Doctors Office Fr Stills
videos:
    - caption: ''
      cover: work/2010/mhps-doctors-office/videos/mhps-doctors-office-en.jpg
      path: work/2010/mhps-doctors-office/videos/mhps-doctors-office-en
      slug: mhps-doctors-office-en
      sources:
          - filename: mhps-doctors-office-en-1280x720.mp4
            format: mp4
            height: '720'
            size: 18714688
            width: '1280'
          - filename: mhps-doctors-office-en-960x540.mp4
            format: mp4
            height: '540'
            size: 1925978
            width: '960'
          - filename: mhps-doctors-office-en-640x360.ogv
            format: ogv
            height: '360'
            size: 1193773
            width: '640'
          - filename: mhps-doctors-office-en-640x360.mp4
            format: mp4
            height: '360'
            size: 1087127
            width: '640'
          - filename: mhps-doctors-office-en-640x360.webm
            format: webm
            height: '360'
            size: 1075432
            width: '640'
          - filename: mhps-doctors-office-en-480x270.mp4
            format: mp4
            height: '270'
            size: 870722
            width: '480'
      title: Mhps Doctors Office En
    - caption: ''
      cover: work/2010/mhps-doctors-office/videos/mhps-doctors-office-fr.jpg
      path: work/2010/mhps-doctors-office/videos/mhps-doctors-office-fr
      slug: mhps-doctors-office-fr
      sources:
          - filename: mhps-doctors-office-fr-1280x720.mp4
            format: mp4
            height: '720'
            size: 18964159
            width: '1280'
          - filename: mhps-doctors-office-fr-960x540.mp4
            format: mp4
            height: '540'
            size: 1462810
            width: '960'
          - filename: mhps-doctors-office-fr-640x360.ogv
            format: ogv
            height: '360'
            size: 1160956
            width: '640'
          - filename: mhps-doctors-office-fr-640x360.webm
            format: webm
            height: '360'
            size: 1071517
            width: '640'
          - filename: mhps-doctors-office-fr-640x360.mp4
            format: mp4
            height: '360'
            size: 1065383
            width: '640'
          - filename: mhps-doctors-office-fr-480x270.mp4
            format: mp4
            height: '270'
            size: 867911
            width: '480'
      title: Mhps Doctors Office Fr
credits:
    - companyName: Ministry of Health
      companySlug: ministry-health
      roleSlug: client
      roleTitle: Client
      visible: false
    - companyName: Bensimon Byrne
      companySlug: bensimon-byrne
      roleSlug: agency
      roleTitle: Advertising Agency
      visible: false
    - personName: David Rosenberg
      personSlug: david-rosenberg
      roleSlug: agency-cd
      roleTitle: Creative Director
      visible: false
    - personName: Michael Lee
      personSlug: michael-lee
      roleSlug: agency-art-director
      roleTitle: Art Director
      visible: false
    - personName: Troy Palmer
      personSlug: troy-palmer
      roleSlug: agency-copywriter
      roleTitle: Copywriter
      visible: false
    - personName: Ken Rodger
      personSlug: ken-rodger
      roleSlug: agency-producer
      roleTitle: Agency Producer
      visible: false
    - companyName: OPC
      companySlug: opc
      roleSlug: production
      roleTitle: Production Company
      visible: false
    - personName: 'Woods + Low '
      personSlug: woods-low
      roleSlug: director
      roleTitle: Director
      visible: false
    - personName: Adam Marsden
      personSlug: adam-marsden
      roleSlug: dp
      roleTitle: Director of Photography
      visible: false
    - personName: Harland Weiss
      personSlug: harland-weiss
      roleSlug: prod-ep
      roleTitle: Executive Producer
      visible: false
    - personName: Tara Handley
      personSlug: tara-handley
      roleSlug: line-producer
      roleTitle: Line Producer
      visible: false
    - companyName: Panic & Bob
      companySlug: panic-bob
      roleSlug: edit
      roleTitle: Editorial
      visible: false
    - personName: David Baxter
      personSlug: david-baxter
      roleSlug: editor
      roleTitle: Editor
      visible: false
    - personName: Sam McLaren
      personSlug: sam-mclaren
      roleSlug: edit-ep
      roleTitle: Executive Producer
      visible: false
    - companyName: Dashing Collective
      companySlug: dshng
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: false
    - personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: vfx-supervisor
      roleTitle: VFX Supervisor
      visible: false
    - personName: Danielle Lyons
      personSlug: danielle-lyons
      roleSlug: vfx-ep
      roleTitle: Executive Producer
      visible: false
    - personName: Debbie Cooke
      personSlug: debbie-cooke
      roleSlug: vfx-producer
      roleTitle: VFX Producer
      visible: false
---

When a viewer sees the face of a 12-year-old boy, sweetly warning that the
decisions he makes now will affect his health when he's much older - they
expect a 12-year-old body to be attached. As the camera dollies out from just
this subject in "Doctor's Office", that prepubescent face is attached to a
titanic, obese body.

This is just the latest bit of magic to emerge from Dashing. The studio teamed
up with OPC Directors Woods + Low and Bensimon Byrne to craft the attention-
grabbing spot &mdash; a warning from the Ontario Ministry of Health Promotion and
Sport about the dangers of not establishing a healthy lifestyle.

Dashing was involved early on in the process to ensure a believable spot. One
of the principal challenges was fulfilling an agency mandate that the spot be
seamless and real, and also not so disturbing that it distracted from the
central message.

While the concept of accurately attaching a 12-year-old boy's head to a 40
-year-old overweight man's body is simple in concept, it required complex
execution. To seamlessly track the boy to the man, tracking markers were
placed in positions of equal proportions all over the face and upper body on
both characters. They then shot multiple passes of the head in different
positions to cover any parts of the performance that didn't allow for clean
integration.

The biggest VFX challenge was getting an accurate track across the neck and
chin of both the boy and man. The fat rolls on the man's chin moved
inconsistently and differently than the thin neck of the boy, making tracking
markers appear and disappear with small gestures and head movements.

"It became clear that this was more than a 2D tracking challenge, and would
have to be solved using a combination of 2D and 3D tools, said Dashing
Founder/VFX Supervisor Rob Moggach. "We tracked the heads in 3D to accurately
capture the subtle variations in perspective as the man or boy spoke, then
applied it to the head of the boy and body of the man so both moved in unison.
With some adjustment to shadowing and careful attention to tracking accurate
mattes along the jawline, we got it right.

Using Linux workstations and working in Maya and Nuke for 3D tracking,
Silhouette for rotoscoping, and Autodesk Flame for the final compositing and
online, Dashing spent three weeks completing both this spot and a companion
French version that used entirely different talent and dialogue.

"We're happy we can take the time to focus on the details with jobs like this.
The subtle details are what we hope will set the quality of our work apart,
noted Moggach. "We were fortunate to be a part of a great creative team that
supported our approach with a truly collaborative spirit. Everyone had a
common goal and engaged to create a great spot.

Anchored by Founder/Creative Director Robert Moggach, Executive Producer
Danielle Lyons and a select team of top digital artists, Dashing is tailor-
made for handling projects with demanding and unconventional, creative and
technical challenges.
