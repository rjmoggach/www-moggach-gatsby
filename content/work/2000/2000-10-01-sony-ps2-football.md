---
title: Sony PS2 | Launch "Football"
subtitle:
slug: sony-ps2-football
date: 2000-10-01
role:
headline: directed by Stuart Douglas
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
tags:
    - Compositing
    - Flame
albums:
    - cover:
      images:
          - caption: ''
            filename: sony-ps2-football.01.jpg
          - caption: ''
            filename: sony-ps2-football.02.jpg
      path: work/2000/sony-ps2-football/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2000/sony-ps2-football/videos/sony-ps2-football.jpg
      path: work/2000/sony-ps2-football/videos/sony-ps2-football
      slug: sony-ps2-football
      sources:
          - filename: sony-ps2-football.1280x720.mp4
            format: mp4
            height: '720'
            size: 3670164
            width: '1280'
          - filename: sony-ps2-football.960x540.mp4
            format: mp4
            height: '540'
            size: 3592099
            width: '960'
          - filename: sony-ps2-football.640x360.mp4
            format: mp4
            height: '360'
            size: 1927845
            width: '640'
          - filename: sony-ps2-football.640x360.ogv
            format: ogv
            height: '360'
            size: 1759831
            width: '640'
          - filename: sony-ps2-football.640x360.webm
            format: webm
            height: '360'
            size: 1603274
            width: '640'
          - filename: sony-ps2-football.480x270.mp4
            format: mp4
            height: '270'
            size: 1267961
            width: '480'
      title: Sony Ps2 Football
credits: []
---

This project was actually sixty-four separate commercials, comprised of eight
separate films with eight different durations. This created an organizational
nightmare, given the task of ensuring that the correct footage was used in
each final master and that the final approved effects sequences were correctly
in place. I had the job of building all the final commercials and creating any
effects that were needed. The effects work in question was primarily cosmetic.
Some shots required compositional changes while others required removal of
elements that detracted from the beauty of the image. In all shots where the
field / pitch was visible, a considerable amount of work was done to enhance
the blue colour of the grass and increase the intensity of the white lines.
The result was eight beautiful films that were used for the launch of Sonyâ€šÃ„Ã´s
new console gaming system in the United Kingdom.
