---
title: Guinness | "Bi-Plane"
subtitle:
slug: guinness-biplane
date: 2000-10-10
role:
headline: ''
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
tags:
    - Compositing
    - Flame
albums:
    - cover:
      images:
          - caption: ''
            filename: guinness-biplane.01.jpg
          - caption: ''
            filename: guinness-biplane.02.jpg
          - caption: ''
            filename: guinness-biplane.03.jpg
          - caption: ''
            filename: guinness-biplane.04.jpg
      path: work/2000/guinness-biplane/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2000/guinness-biplane/videos/guinness-biplane.jpg
      path: work/2000/guinness-biplane/videos/guinness-biplane
      slug: guinness-biplane
      sources:
          - filename: guinness-biplane.1280x720.mp4
            format: mp4
            height: '720'
            size: 10483817
            width: '1280'
          - filename: guinness-biplane.960x540.mp4
            format: mp4
            height: '540'
            size: 7389113
            width: '960'
          - filename: guinness-biplane.640x360.mp4
            format: mp4
            height: '360'
            size: 4621536
            width: '640'
          - filename: guinness-biplane.640x360.ogv
            format: ogv
            height: '360'
            size: 4012135
            width: '640'
          - filename: guinness-biplane.640x360.webm
            format: webm
            height: '360'
            size: 2976113
            width: '640'
          - filename: guinness-biplane.480x270.mp4
            format: mp4
            height: '270'
            size: 2818305
            width: '480'
      title: Guinness Biplane
credits: []
---

I worked on two shots for this commercial, assisting the lead on the project:
Sean Broughton. The first was a transition from a rockslide to tumbling
guinness foam, pulling back to a pint of guinness pulling back further to a
cover girl holding the pint pulling back further to reveal the girl on the
side of a plane banking away. The second was combining live action plane
footage with the fictitious desert landscape.
