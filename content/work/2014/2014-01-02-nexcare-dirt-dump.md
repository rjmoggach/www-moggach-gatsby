---
title: Nexcare "Dirt Dump"
subtitle:
slug: nexcare-dirt-dump
date: 2014-01-01
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories:
- Advertising
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: nexcare-dirt-dump.01.jpg
    -   caption: ''
        filename: nexcare-dirt-dump.02.jpg
    -   caption: ''
        filename: nexcare-dirt-dump.03.jpg
    -   caption: ''
        filename: nexcare-dirt-dump.04.jpg
    -   caption: ''
        filename: nexcare-dirt-dump.05.jpg
    -   caption: ''
        filename: nexcare-dirt-dump.06.jpg
    -   caption: ''
        filename: nexcare-dirt-dump.07.jpg
    path: work/2014/nexcare-dirt-dump/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2014/nexcare-dirt-dump/videos/nexcare-dirt-dump.jpg
    path: work/2014/nexcare-dirt-dump/videos/nexcare-dirt-dump
    slug: nexcare-dirt-dump
    sources:
    -   filename: nexcare-dirtdump-dirTitled-1280x720.mp4
        format: mp4
        height: '720'
        size: 23379992
        width: '1280'
    -   filename: nexcare-dirtdump-dirTitled-960x540.mp4
        format: mp4
        height: '540'
        size: 16574044
        width: '960'
    -   filename: nexcare-dirtdump-dirTitled-640x360.ogv
        format: ogv
        height: '360'
        size: 11304840
        width: '640'
    -   filename: nexcare-dirtdump-dirTitled-640x360.webm
        format: webm
        height: '360'
        size: 9986226
        width: '640'
    -   filename: nexcare-dirtdump-dirTitled-640x360.mp4
        format: mp4
        height: '360'
        size: 9876480
        width: '640'
    -   filename: nexcare-dirtdump-dirTitled-480x270.mp4
        format: mp4
        height: '270'
        size: 6322491
        width: '480'
    title: Nexcare Dirt Dump
-   caption: ''
    cover: ''
    path: work/2014/nexcare-dirt-dump/videos/prores
    slug: prores
    sources: []
    title: Prores
credits: []
---
