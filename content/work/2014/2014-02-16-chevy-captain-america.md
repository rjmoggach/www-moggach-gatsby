---
title: Chevy Captain America
subtitle:
slug: chevy-captain-america
date: 2014-02-01
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories:
- Advertising
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: chevy-captain-america.01.jpg
    -   caption: ''
        filename: chevy-captain-america.02.jpg
    -   caption: ''
        filename: chevy-captain-america.03.jpg
    -   caption: ''
        filename: chevy-captain-america.04.jpg
    -   caption: ''
        filename: chevy-captain-america.05.jpg
    -   caption: ''
        filename: chevy-captain-america.06.jpg
    -   caption: ''
        filename: chevy-captain-america.07.jpg
    -   caption: ''
        filename: chevy-captain-america.08.jpg
    -   caption: ''
        filename: chevy-captain-america.09.jpg
    -   caption: ''
        filename: chevy-captain-america.10.jpg
    -   caption: ''
        filename: chevy-captain-america.11.jpg
    -   caption: ''
        filename: chevy-captain-america.12.jpg
    -   caption: ''
        filename: chevy-captain-america.13.jpg
    -   caption: ''
        filename: chevy-captain-america.14.jpg
    -   caption: ''
        filename: chevy-captain-america.15.jpg
    -   caption: ''
        filename: chevy-captain-america.16.jpg
    -   caption: ''
        filename: chevy-captain-america.17.jpg
    -   caption: ''
        filename: chevy-captain-america.18.jpg
    -   caption: ''
        filename: chevy-captain-america.19.jpg
    -   caption: ''
        filename: chevy-captain-america.20.jpg
    -   caption: ''
        filename: chevy-captain-america.21.jpg
    -   caption: ''
        filename: chevy-captain-america.22.jpg
    -   caption: ''
        filename: chevy-captain-america.23.jpg
    -   caption: ''
        filename: chevy-captain-america.24.jpg
    path: work/2014/chevy-captain-america/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2014/chevy-captain-america/videos/chevy-captain-america.jpg
    path: work/2014/chevy-captain-america/videos/chevy-captain-america
    slug: chevy-captain-america
    sources:
    -   filename: chevy-capt-america-1280x720.mp4
        format: mp4
        height: '720'
        size: 14712649
        width: '1280'
    -   filename: chevy-capt-america-960x540.mp4
        format: mp4
        height: '540'
        size: 11301580
        width: '960'
    -   filename: chevy-capt-america-640x360.ogv
        format: ogv
        height: '360'
        size: 8115193
        width: '640'
    -   filename: chevy-capt-america-640x360.mp4
        format: mp4
        height: '360'
        size: 7930344
        width: '640'
    -   filename: chevy-capt-america-640x360.webm
        format: webm
        height: '360'
        size: 7896566
        width: '640'
    -   filename: chevy-capt-america-480x270.mp4
        format: mp4
        height: '270'
        size: 5672497
        width: '480'
    title: Chevy Captain America
credits:
-   companyName: Zoic Studios
    companySlug: zoic
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: true
-   companyName: Zoic Studios
    companySlug: zoic
    personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: creative-director
    roleTitle: Creative Director
    visible: true
-   companyName: Zoic Studios
    companySlug: zoic
    personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: dfx-supervisor
    roleTitle: DFX Supervisor
    visible: true
-   companyName: Zoic Studios
    companySlug: zoic
    personName: Ryan McDougall
    personSlug: ryan-mcdougall
    roleSlug: cg-sup
    roleTitle: CG Supervisor
    visible: true
---
