---
title: HP | "Connect, Print"
subtitle: 'Connect, Print'
slug: hp-connect-print
date: 2014-02-01
role:
headline: ''
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
tags:
    - colour
albums:
    - cover:
      images:
          - caption: ''
            filename: hp-connect-print.01.jpg
          - caption: ''
            filename: hp-connect-print.02.jpg
          - caption: ''
            filename: hp-connect-print.03.jpg
          - caption: ''
            filename: hp-connect-print.04.jpg
          - caption: ''
            filename: hp-connect-print.05.jpg
          - caption: ''
            filename: hp-connect-print.06.jpg
      path: work/2014/hp-connect-print/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2014/hp-connect-print/videos/hp-connect-print.jpg
      path: work/2014/hp-connect-print/videos/hp-connect-print
      slug: hp-connect-print
      sources:
          - filename: hp-connect-print-1280x720.mp4
            format: mp4
            height: '720'
            size: 6044705
            width: '1280'
          - filename: hp-connect-print-960x540.mp4
            format: mp4
            height: '540'
            size: 4606245
            width: '960'
          - filename: hp-connect-print-640x360.ogv
            format: ogv
            height: '360'
            size: 3756274
            width: '640'
          - filename: hp-connect-print-640x360.webm
            format: webm
            height: '360'
            size: 3361481
            width: '640'
          - filename: hp-connect-print-640x360.mp4
            format: mp4
            height: '360'
            size: 3250620
            width: '640'
          - filename: hp-connect-print-480x270.mp4
            format: mp4
            height: '270'
            size: 2406711
            width: '480'
      title: Hp Connect Print
credits:
    - companyName: Zoic Studios
      companySlug: zoic
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: true
    - companyName: Zoic Studios
      companySlug: zoic
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: agency-cd
      roleTitle: Creative Director
      visible: true
    - companyName: Zoic Studios
      companySlug: zoic
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: colorist
      roleTitle: Colorist
      visible: true
    - companyName: Zoic Studios
      companySlug: zoic
      personName: RenÃ©e Tymn
      personSlug: renee-tymn
      roleSlug: flame
      roleTitle: Flame Artist
      visible: true
    - personName: Bo Platt
      personSlug: bo-platt
      roleSlug: director
      roleTitle: Director
      visible: false
---
