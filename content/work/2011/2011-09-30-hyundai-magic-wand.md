---
title: Hyundai Magic Wand
subtitle:
slug: hyundai-magic-wand
date: 2011-06-06
role:
headline: directed by Clay Weiner, OPC for Innocean Canada
summary: 'Dashing worked with OPC Director Clay Weiner to fulfill Innocean''s vision
    forthe project, creating magic effects that were succinct, even believable intheir
    simplicity of execution. "Our main challenge was creating magicalappearing/disappearing
    effects that were simple and accessible," notedCreative Lead/Flame: Rob Moggach.'
excerpt: 'Dashing worked with OPC Director Clay Weiner to fulfill Innocean''s vision
    forthe project, creating magic effects that were succinct, even believable intheir
    simplicity of execution. "Our main challenge was creating magicalappearing/disappearing
    effects that were simple and accessible," notedCreative Lead/Flame: Rob Moggach.'
published: false
featured: false
categories:
- Advertising
tags:
- VFX
- compositing
- automotive
- comedy
- effects
- photoreal
albums:
-   cover:
    images:
    -   caption: ''
        filename: hyundai-magic-wand.01.jpg
    -   caption: ''
        filename: hyundai-magic-wand.02.jpg
    -   caption: ''
        filename: hyundai-magic-wand.03.jpg
    -   caption: ''
        filename: hyundai-magic-wand.04.jpg
    -   caption: ''
        filename: hyundai-magic-wand.05.jpg
    -   caption: ''
        filename: hyundai-magic-wand.06.jpg
    -   caption: ''
        filename: hyundai-magic-wand.07.jpg
    -   caption: ''
        filename: hyundai-magic-wand.08.jpg
    -   caption: ''
        filename: hyundai-magic-wand.09.jpg
    path: work/2011/hyundai-magic-wand/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2011/hyundai-magic-wand/videos/hyundai-magic-wand.jpg
    path: work/2011/hyundai-magic-wand/videos/hyundai-magic-wand
    slug: hyundai-magic-wand
    sources:
    -   filename: hyundai-magic-wand-1280x720.mp4
        format: mp4
        height: '720'
        size: 37342769
        width: '1280'
    -   filename: hyundai-magic-wand-960x540.mp4
        format: mp4
        height: '540'
        size: 17680767
        width: '960'
    -   filename: hyundai-magic-wand-640x360.ogv
        format: ogv
        height: '360'
        size: 10553261
        width: '640'
    -   filename: hyundai-magic-wand-640x360.mp4
        format: mp4
        height: '360'
        size: 10179310
        width: '640'
    -   filename: hyundai-magic-wand-640x360.webm
        format: webm
        height: '360'
        size: 9162596
        width: '640'
    -   filename: hyundai-magic-wand-480x270.mp4
        format: mp4
        height: '270'
        size: 6614596
        width: '480'
    title: Hyundai Magic Wand
credits:
-   companyName: Hyundai Canada
    companySlug: hyundai-canada
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Innocean Worldwide Canada
    companySlug: innocean-worldwide-canada
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: false
-   personName: Gary Westgate
    personSlug: gary-westgate
    roleSlug: agency-cd
    roleTitle: Creative Director
    visible: false
-   personName: Damon Crate
    personSlug: damon-crate
    roleSlug: agency-art-director
    roleTitle: Art Director
    visible: false
-   personName: Nelson Quintal
    personSlug: nelson-quintal
    roleSlug: agency-copywriter
    roleTitle: Copywriter
    visible: false
-   personName: Alina Prussky
    personSlug: alina-prussky
    roleSlug: agency-producer
    roleTitle: Agency Producer
    visible: false
-   companyName: OPC
    companySlug: opc
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Clay Weiner
    personSlug: clay-weiner
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Harland Weiss
    personSlug: harland-weiss
    roleSlug: prod-ep
    roleTitle: Executive Producer
    visible: false
-   personName: Tony DiMarco
    personSlug: tony-dimarco
    roleSlug: line-producer
    roleTitle: Line Producer
    visible: false
-   companyName: School Editing
    companySlug: school-editing
    roleSlug: edit
    roleTitle: Editorial
    visible: false
-   personName: Brian Wells
    personSlug: brian-wells
    roleSlug: editor
    roleTitle: Editor
    visible: false
-   personName: Sarah Brooks
    personSlug: sarah-brooks
    roleSlug: edit-ep
    roleTitle: Executive Producer
    visible: false
-   companyName: Dashing Collective
    companySlug: dshng
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Danielle Lyons
    personSlug: danielle-lyons
    roleSlug: vfx-ep
    roleTitle: Executive Producer
    visible: false
-   personName: Mary Anne Ledesma
    personSlug: mary-anne-ledesma
    roleSlug: vfx-producer
    roleTitle: VFX Producer
    visible: false
---
For the artists at Dashing, magic doesn't seem so mysterious. This is proven
True in a :60 spot for Hyundai, produced in collaboration with past live
action partner, OPC, for Innocean Worldwide.

_Magic Wand_ stars a band of four buddies who discover a magic wand on the
side of the road. They immediately conjure up a Hyundai Accent and the crew
launches into an urban adventure. The wand is used to change red stoplights to
green, transport food-bearing waiters into their car, make the guitar-
strumming hula girl on their dashboard come to life, and transport one lucky
bachelor into another Hyundai full of attractive females.

Dashing worked with OPC Director Clay Weiner to fulfill Innocean's vision for
the project, creating magic effects that were succinct, even believable in
their simplicity of execution. "Our main challenge was creating magical
appearing/disappearing effects that were simple and accessible," noted
Creative Lead/Flame: Rob Moggach. "It was imperative that we create a magical
effect that didn't suffer from the usual stereotypes of glowing stardust and
overly processed digital work. It had to feel real." After painting clean
plates, Moggach used subtle warping of the image combined with interactive
lighting effects to create smooth transitions and transformations that feel
more like sleight of hand than digital effects.

With a demanding schedule, Dashing completed the magic effects and extensive
car cleanup work throughout the :60 spot in just two days, using a streamlined
workflow that relied on Autodesk Flame instead of more time consuming CGI or
even rotoscoping. "Innocean was nervous about their tight schedule, but we
pushed ourselves to deliver the promised effects on eight shots and Rob was
determined to also balance the car's aesthetic throughout the spot," said
Dashing EP: Danielle Lyons. "It was refreshing to work with Clay, who brought
a more narrative and comedy-focused experience to a traditional car spot. He
trusted Rob's instinct for the effects work and had a good intuition for how
the VFX would play out and what to shoot, despite a demanding two-day shoot
schedule."

Anchored by Creative Director Rob Moggach, Executive Producer Danielle Lyons
and a team of top digital artists, Dashing thrives on handling unconventional
projects with demanding creative and technical challenges.