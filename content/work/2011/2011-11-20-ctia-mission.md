---
title: Ctia Mission
subtitle:
slug: ctia-mission
date: 2000-12-15
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: ctia-mission.01.jpg
    -   caption: ''
        filename: ctia-mission.02.jpg
    -   caption: ''
        filename: ctia-mission.03.jpg
    -   caption: ''
        filename: ctia-mission.04.jpg
    path: work/2011/ctia-mission/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2011/ctia-mission/videos/ctia-mission.jpg
    path: work/2011/ctia-mission/videos/ctia-mission
    slug: ctia-mission
    sources:
    -   filename: ctia-mission-1280x720.mp4
        format: mp4
        height: '720'
        size: 7324511
        width: '1280'
    title: Ctia Mission
credits: []
---
