---
title: Crea Old Woman
subtitle:
slug: crea-old-woman
date: 2011-03-11
role:
headline: directed by Blue Source, Radke Films/MJZ for CP+B Toronto
summary: When the Canadian Real Estate Association decided to craft a spot portrayingan
    old woman and her dozens of children living in a shoe-shaped home, theyturned
    to Dashing to craft the colossal structure, inspired by the classicnursery rhyme
    about a matriarch raising her brood in a shoe. Working withMJZ/Radke Films Director
    Blue Source via Crispin Porter + Bogusky Toronto,Dashing created an unlikely and
    impressive building at the nexus of the realand fantastic.
excerpt: When the Canadian Real Estate Association decided to craft a spot portrayingan
    old woman and her dozens of children living in a shoe-shaped home, theyturned
    to Dashing to craft the colossal structure, inspired by the classicnursery rhyme
    about a matriarch raising her brood in a shoe. Working withMJZ/Radke Films Director
    Blue Source via Crispin Porter + Bogusky Toronto,Dashing created an unlikely and
    impressive building at the nexus of the realand fantastic.
published: false
featured: false
categories:
- Advertising
tags:
- VFX
- photoreal
- CG
- compositing
- tracking
- comedy
- design
albums:
-   cover:
    images:
    -   caption: ''
        filename: crea-old-woman.01.jpg
    -   caption: ''
        filename: crea-old-woman.02.jpg
    -   caption: ''
        filename: crea-old-woman.03.jpg
    -   caption: ''
        filename: crea-old-woman.04.jpg
    -   caption: ''
        filename: crea-old-woman.05.jpg
    path: work/2011/crea-old-woman/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2011/crea-old-woman/videos/crea-old-woman.jpg
    path: work/2011/crea-old-woman/videos/crea-old-woman
    slug: crea-old-woman
    sources:
    -   filename: crea-old-woman-960x540.mp4
        format: mp4
        height: '540'
        size: 49553054
        width: '960'
    -   filename: crea-old-woman-640x360.mp4
        format: mp4
        height: '360'
        size: 22317540
        width: '640'
    -   filename: crea-old-woman-640x360.ogv
        format: ogv
        height: '360'
        size: 21844964
        width: '640'
    -   filename: crea-old-woman-640x360.webm
        format: webm
        height: '360'
        size: 21788481
        width: '640'
    -   filename: crea-old-woman-480x270.mp4
        format: mp4
        height: '270'
        size: 14057166
        width: '480'
    title: Crea Old Woman
-   caption: ''
    cover: work/2011/crea-old-woman/videos/crea-old-woman-director.jpg
    path: work/2011/crea-old-woman/videos/crea-old-woman-director
    slug: crea-old-woman-director
    sources:
    -   filename: crea-old-woman-director-1280x720.mp4
        format: mp4
        height: '720'
        size: 55802482
        width: '1280'
    -   filename: crea-old-woman-director-960x540.mp4
        format: mp4
        height: '540'
        size: 34774505
        width: '960'
    -   filename: crea-old-woman-director-640x360.ogv
        format: ogv
        height: '360'
        size: 18337030
        width: '640'
    -   filename: crea-old-woman-director-640x360.mp4
        format: mp4
        height: '360'
        size: 17120726
        width: '640'
    -   filename: crea-old-woman-director-640x360.webm
        format: webm
        height: '360'
        size: 16458709
        width: '640'
    -   filename: crea-old-woman-director-480x270.mp4
        format: mp4
        height: '270'
        size: 10661702
        width: '480'
    title: Crea Old Woman Director
credits:
-   companyName: CREA
    companySlug: crea
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: CP+B
    companySlug: cpb
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: false
-   personName: Mark Puchala
    personSlug: mark-puchala
    roleSlug: agency-art-director
    roleTitle: Art Director
    visible: false
-   personName: Gerald Kugler
    personSlug: gerald-kugler
    roleSlug: agency-copywriter
    roleTitle: Copywriter
    visible: false
-   personName: Alina Prussky
    personSlug: alina-prussky
    roleSlug: agency-producer
    roleTitle: Agency Producer
    visible: false
-   companyName: Radke / MJZ
    companySlug: radke-mjz
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: 'Blue Source '
    personSlug: blue-source
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Eduardo Martinez
    personSlug: eduardo-martinez
    roleSlug: dp
    roleTitle: Director of Photography
    visible: false
-   personName: Scott Mackenzie
    personSlug: scott-mackenzie
    roleSlug: prod-ep
    roleTitle: Executive Producer
    visible: false
-   personName: Eric Stern
    personSlug: eric-stern
    roleSlug: edit-ep
    roleTitle: Executive Producer
    visible: false
-   personName: Mark Hall
    personSlug: mark-hall
    roleSlug: line-producer
    roleTitle: Line Producer
    visible: false
-   companyName: School Editing
    companySlug: school-editing
    roleSlug: edit
    roleTitle: Editorial
    visible: false
-   personName: Chris Van Dyke
    personSlug: chris-van-dyke
    roleSlug: editor
    roleTitle: Editor
    visible: false
-   personName: Sarah Brooks
    personSlug: sarah-brooks
    roleSlug: edit-ep
    roleTitle: Executive Producer
    visible: false
-   companyName: Dashing Collective
    companySlug: dshng
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Doug Law
    personSlug: doug-law
    roleSlug: cg-sup
    roleTitle: CG Supervisor
    visible: false
-   personName: Danielle Lyons
    personSlug: danielle-lyons
    roleSlug: vfx-ep
    roleTitle: Executive Producer
    visible: false
-   personName: Mary Anne Ledesma
    personSlug: mary-anne-ledesma
    roleSlug: vfx-producer
    roleTitle: VFX Producer
    visible: false
-   companyName: Notch
    companySlug: notch
    roleSlug: telecine
    roleTitle: DI / Color
    visible: false
-   personName: Bill Ferwerda
    personSlug: bill-ferwerda
    roleSlug: colorist
    roleTitle: Colorist
    visible: false
-   personName: Bill Ferwerda
    personSlug: bill-ferwerda
    roleSlug: colorist
    roleTitle: Colorist
    visible: false
---
When the Canadian Real Estate Association decided to craft a spot portraying
an old woman and her dozens of children living in a shoe-shaped home, they
turned to Dashing to craft the colossal structure, inspired by the classic
nursery rhyme about a matriarch raising her brood in a shoe. Working with
MJZ/Radke Films Director Blue Source via Crispin Porter + Bogusky Toronto,
Dashing created an unlikely and impressive building at the nexus of the real
and fantastic.

Old Woman is narrated by its star, Carmela, a determined mother who rises at 6
a.m. each day to care for her enormous brood. As she dishes out breakfast,
works the endless lines of laundry, leads calisthenics and band practice, and
corrals her children onto the school bus, she boasts about how much easier her
life has been since moving to her new home and how helpful it was that her
REALTORÂ® understood her needs. The spot ends with a slow-moving, panoramic
shot of the Dashing-created, entirely CG, shoe-shaped house, a tremendous
aging beauty with three floors rising off the "heel," an ivy-covered porch,
and a large multi-windowed room in the "toe," all set on a sprawling lawn.

"Blue Source wanted to build a house shaped like a shoe instead of an
oversized shoe, so we set out to create a house that felt like it was really
built that way and had evolved over time," noted Rob Moggach, who acted as VFX
supervisor and Flame artist on the spot. "Building the house with CG gave us
the flexibility to build a photoreal house that could be art directed to meet
changing creative needs and be incorporated into an undetermined selection of
wide and medium shots. We worked closely with Blue Source's production design
team throughout the design and development to get the look exactly right."

Dashing built the house in Maya and Mental Ray over a two-week period,
experimenting with different window shapes, floor scales, stucco and brick
exteriors, and clay or wood roof tiles. They then aged the house, making the
different floors settle into place so that - much as in many actual old homes

-   nothing was perfectly flat. Ivy was added on the walls and water damage
    textures and cracks were the final touches.

"To maintain the realism of our integration we completed all the visual
effects in a flat film color space and the grade was applied to our fully
integrated shots once they were finished," said Moggach. "This meant our work
was graded in the same way the rest of the film was graded, helping the
cohesiveness of the shots within the film immensely. We needed to cover
ourselves for different scenarios ahead of seeing any footage, so we were
careful to add a lot of detail to our model and texture elements."

Once Dashing received the final plates, they had less than two weeks to track
the shots using SynthEyes and Nuke, finish texturing, then light (Boxx
workstations with Renderpro satellite solution), render and composite (using
Nuke and Flame) the shoe into the plates. "One of the big challenges was
tracking the documentary style, handheld, and very grainy super16 footage,
which gives the film it's very real and intimate personality," said Moggach.
"Lock-offs or dolly shots were not part of this piece, and many shots featured
energized kids running through frame, adding to the challenge."

This project marked the beginning of a new relationship for Dashing with both
CP+B Toronto and directing team Blue Source, though they had worked with both
MJZ and Radke in the past. "They entrusted us with the CG/VFX based on Rob's
reputation for producing great work locally and abroad," stated EP Danielle
Lyons. "There was a collaborative synergy in the whole team that enabled the
great result. The film tells the story in quite a clever, revealing way, and I
believe both the film and the VFX maintain the integrity of the original
vision. Our VFX work serves as a backdrop to an already great narrative."