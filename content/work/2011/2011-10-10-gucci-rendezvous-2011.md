---
title: Gucci Rendezvous 2011
subtitle:
slug: gucci-rendezvous-2011
date: 2011-05-16
role:
headline: directed by Nicholas Berglund for Gucci Paris
summary: Two beautiful people, a chance meeting & Paris. Sounds like a recipe forl'amour
    and some gorgeous glasses that will add colour to the world aroundyou. Nicholas
    Berglund requested Rob Moggach's eye for colour on this"Rendezvous" spot for Gucci.
excerpt: Two beautiful people, a chance meeting & Paris. Sounds like a recipe forl'amour
    and some gorgeous glasses that will add colour to the world aroundyou. Nicholas
    Berglund requested Rob Moggach's eye for colour on this"Rendezvous" spot for Gucci.
published: false
featured: false
categories:
- Advertising
tags:
- compositing
- colour
albums:
-   cover:
    images:
    -   caption: ''
        filename: gucci-rendezvous-2011.01.jpg
    -   caption: ''
        filename: gucci-rendezvous-2011.02.jpg
    -   caption: ''
        filename: gucci-rendezvous-2011.03.jpg
    -   caption: ''
        filename: gucci-rendezvous-2011.04.jpg
    -   caption: ''
        filename: gucci-rendezvous-2011.05.jpg
    -   caption: ''
        filename: gucci-rendezvous-2011.06.jpg
    -   caption: ''
        filename: gucci-rendezvous-2011.07.jpg
    -   caption: ''
        filename: gucci-rendezvous-2011.08.jpg
    -   caption: ''
        filename: gucci-rendezvous-2011.09.jpg
    -   caption: ''
        filename: gucci-rendezvous-2011.10.jpg
    path: work/2011/gucci-rendezvous-2011/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2011/gucci-rendezvous-2011/videos/gucci-rendezvous-2011.jpg
    path: work/2011/gucci-rendezvous-2011/videos/gucci-rendezvous-2011
    slug: gucci-rendezvous-2011
    sources:
    -   filename: gucci-rendezvous-2011-960x540.mp4
        format: mp4
        height: '540'
        size: 15696648
        width: '960'
    -   filename: gucci-rendezvous-2011-640x360.ogv
        format: ogv
        height: '360'
        size: 12283401
        width: '640'
    -   filename: gucci-rendezvous-2011-640x360.mp4
        format: mp4
        height: '360'
        size: 10376969
        width: '640'
    -   filename: gucci-rendezvous-2011-640x360.webm
        format: webm
        height: '360'
        size: 9648509
        width: '640'
    -   filename: gucci-rendezvous-2011-480x270.mp4
        format: mp4
        height: '270'
        size: 7309000
        width: '480'
    title: Gucci Rendezvous 2011
credits:
-   companyName: Gucci
    companySlug: gucci
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Nicholas Berglund
    companySlug: nicholas-berglund
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Nicholas Berglund
    personSlug: nicholas-berglund
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Sebastian Pfaffenbichler
    personSlug: sebastian-pfaffenbichler
    roleSlug: dp
    roleTitle: Director of Photography
    visible: false
-   personName: Agatha Gorazdowski
    personSlug: agatha-gorazdowski
    roleSlug: agency-producer
    roleTitle: Agency Producer
    visible: false
-   companyName: Dashing Collective
    companySlug: dshng
    roleSlug: telecine
    roleTitle: DI / Color
    visible: false
-   companyName: Dashing Collective
    companySlug: dshng
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Danielle Lyons
    personSlug: danielle-lyons
    roleSlug: vfx-ep
    roleTitle: Executive Producer
    visible: false
-   personName: Mary Anne Ledesma
    personSlug: mary-anne-ledesma
    roleSlug: vfx-producer
    roleTitle: VFX Producer
    visible: false
---
Two beautiful people, a chance meeting & Paris. Sounds like a recipe for
l'amour and some gorgeous glasses that will add colour to the world around
you. Nicholas Berglund requested Rob Moggach's eye for colour on this
"Rendezvous" spot for Gucci.