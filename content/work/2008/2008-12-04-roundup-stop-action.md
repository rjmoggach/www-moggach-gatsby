---
title: Roundup Stop Action
subtitle:
slug: roundup-stop-action
date: 2000-12-15
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: roundup-stop-action.01.jpg
    -   caption: ''
        filename: roundup-stop-action.02.jpg
    -   caption: ''
        filename: roundup-stop-action.03.jpg
    -   caption: ''
        filename: roundup-stop-action.04.jpg
    -   caption: ''
        filename: roundup-stop-action.05.jpg
    path: work/2008/roundup-stop-action/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2008/roundup-stop-action/videos/roundup-stop-action.jpg
    path: work/2008/roundup-stop-action/videos/roundup-stop-action
    slug: roundup-stop-action
    sources:
    -   filename: roundup-stop-action-1280x720.mp4
        format: mp4
        height: '720'
        size: 10377996
        width: '1280'
    -   filename: roundup-stop-action-960x540.mp4
        format: mp4
        height: '540'
        size: 8051534
        width: '960'
    -   filename: roundup-stop-action-640x360.mp4
        format: mp4
        height: '360'
        size: 4410771
        width: '640'
    -   filename: roundup-stop-action-640x360.ogv
        format: ogv
        height: '360'
        size: 4398396
        width: '640'
    -   filename: roundup-stop-action-640x360.webm
        format: webm
        height: '360'
        size: 3175193
        width: '640'
    -   filename: roundup-stop-action-480x270.mp4
        format: mp4
        height: '270'
        size: 2692873
        width: '480'
    title: Roundup Stop Action
credits: []
---
