---
title: Land Rover Kidnap
subtitle:
slug: land-rover-kidnap
date: 2008-10-01
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags:
- compositing
- automotive
- Live Action
albums:
-   cover:
    images:
    -   caption: ''
        filename: land-rover-kidnap.01.jpg
    -   caption: ''
        filename: land-rover-kidnap.02.jpg
    -   caption: ''
        filename: land-rover-kidnap.03.jpg
    -   caption: ''
        filename: land-rover-kidnap.04.jpg
    -   caption: ''
        filename: land-rover-kidnap.05.jpg
    path: work/2008/land-rover-kidnap/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2008/land-rover-kidnap/videos/land-rover-kidnap-30.jpg
    path: work/2008/land-rover-kidnap/videos/land-rover-kidnap-30
    slug: land-rover-kidnap-30
    sources:
    -   filename: land-rover-kidnap-30-1280x720.mp4
        format: mp4
        height: '720'
        size: 9638468
        width: '1280'
    -   filename: land-rover-kidnap-30-960x540.mp4
        format: mp4
        height: '540'
        size: 7253813
        width: '960'
    -   filename: land-rover-kidnap-30-640x360.ogv
        format: ogv
        height: '360'
        size: 5325059
        width: '640'
    -   filename: land-rover-kidnap-30-640x360.mp4
        format: mp4
        height: '360'
        size: 4757242
        width: '640'
    -   filename: land-rover-kidnap-30-640x360.webm
        format: webm
        height: '360'
        size: 4140971
        width: '640'
    -   filename: land-rover-kidnap-30-480x270.mp4
        format: mp4
        height: '270'
        size: 3254946
        width: '480'
    title: Land Rover Kidnap 30
-   caption: ''
    cover: work/2008/land-rover-kidnap/videos/land-rover-kidnap-45.jpg
    path: work/2008/land-rover-kidnap/videos/land-rover-kidnap-45
    slug: land-rover-kidnap-45
    sources:
    -   filename: land-rover-kidnap-45-1280x720.mp4
        format: mp4
        height: '720'
        size: 14604947
        width: '1280'
    -   filename: land-rover-kidnap-45-960x540.mp4
        format: mp4
        height: '540'
        size: 10998154
        width: '960'
    -   filename: land-rover-kidnap-45-640x360.ogv
        format: ogv
        height: '360'
        size: 7988434
        width: '640'
    -   filename: land-rover-kidnap-45-640x360.mp4
        format: mp4
        height: '360'
        size: 7328137
        width: '640'
    -   filename: land-rover-kidnap-45-640x360.webm
        format: webm
        height: '360'
        size: 6338435
        width: '640'
    -   filename: land-rover-kidnap-45-480x270.mp4
        format: mp4
        height: '270'
        size: 5029685
        width: '480'
    title: Land Rover Kidnap 45
credits:
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: director
    roleTitle: Director
    visible: true
-   personName: Michael Pescasio
    personSlug: michael-pescasio
    roleSlug: dp
    roleTitle: Director of Photography
    visible: true
-   personName: Theresa Marth
    personSlug: theresa-marth
    roleSlug: line-producer
    roleTitle: Line Producer
    visible: true
---
