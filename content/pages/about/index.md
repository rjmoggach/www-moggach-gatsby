---
title: About
slug: about-index
date: '2019-07-15'
headline: I Collaborate. | I Craft. | I Create.
cover: images/rob-hand-shadow.jpg
---

I am grateful for my success being a result of collaborating with an incredible collection of peers, dedicated to our individual crafts, and focused on how to be creative in our approach to any challenge. My focus is always working together, learning, improving and honing our craft, and looking past the button pushing to how we can create something elevated and special. My work here is hopefully a reflection of that shared passion and some amazing collaborations. Thank you.

Here's some self-indulgent espousing of rhetoric in the form of quotes I'm heard saying...

**_"A visual design foundation reveals the destination. A process design foundation draws the map. Both are essential."_**

**_"There are no problems, only situations to manage."_**

**_"Worry about making a great film first and the rest will come together."_**

**_"If it's worth doing, do it right."_**
