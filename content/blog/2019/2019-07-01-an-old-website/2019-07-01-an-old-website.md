---
postType: blog
published: false
featured: false
title: An Old Website...
date: '2019-07-01'
section: blog
cover: 'placeimg_1000_480_tech2.jpg'
tags: ['news', 'development', 'react', 'github', 'gatsbyjs', 'ssg', 'static site generator']
draft: false
categories: ['Creative', 'Design', 'Visual Effects']
---

My old website was efficient and cutting edge at the time but it needed a refresh and I've been enamored by the JAM stack and then the MERN stack and all the latest javascript based stuff going on. It's a different learning curve but I've done it.
